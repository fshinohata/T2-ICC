
N: 32
# Tempo LU: 0s
# Tempo iter: 0.001s
# Tempo Residuo: 0.001s
|    L3 bandwidth [MBytes/s]    | 18815.4229 |
|    L3 bandwidth [MBytes/s]    | 32726.6837 |

N: 33
# Tempo LU: 0s
# Tempo iter: 0.003s
# Tempo Residuo: 0.002s
|    L3 bandwidth [MBytes/s]    | 9215.4118 |
|    L3 bandwidth [MBytes/s]    | 26489.5627 |

N: 64
# Tempo LU: 0s
# Tempo iter: 0.003s
# Tempo Residuo: 0.004s
|    L3 bandwidth [MBytes/s]    | 13328.7948 |
|    L3 bandwidth [MBytes/s]    | 24533.2497 |

N: 65
# Tempo LU: 0s
# Tempo iter: 0.004s
# Tempo Residuo: 0.005s
|    L3 bandwidth [MBytes/s]    | 13428.9905 |
|    L3 bandwidth [MBytes/s]    | 23111.2003 |

N: 128
# Tempo LU: 0.001s
# Tempo iter: 0.007s
# Tempo Residuo: 0.007s
|    L3 bandwidth [MBytes/s]    | 31061.6753 |
|    L3 bandwidth [MBytes/s]    | 46108.1372 |

N: 129
# Tempo LU: 0.001s
# Tempo iter: 0.009s
# Tempo Residuo: 0.007s
|    L3 bandwidth [MBytes/s]    | 25537.0534 |
|    L3 bandwidth [MBytes/s]    | 43429.4810 |

N: 256
# Tempo LU: 0.005s
# Tempo iter: 0.017s
# Tempo Residuo: 0.016s
|    L3 bandwidth [MBytes/s]    | 42887.1561 |
|    L3 bandwidth [MBytes/s]    | 50725.2298 |

N: 257
# Tempo LU: 0.009s
# Tempo iter: 0.016s
# Tempo Residuo: 0.017s
|    L3 bandwidth [MBytes/s]    | 38430.2421 |
|    L3 bandwidth [MBytes/s]    | 51069.6533 |

N: 512
# Tempo LU: 0.048s
# Tempo iter: 0.048s
# Tempo Residuo: 0.046s
|    L3 bandwidth [MBytes/s]    | 36982.6754 |
|    L3 bandwidth [MBytes/s]    | 45021.9366 |

N: 1000
# Tempo LU: 0.24s
# Tempo iter: 0.5s
# Tempo Residuo: 0.488s
|    L3 bandwidth [MBytes/s]    | 19371.3597 |
|    L3 bandwidth [MBytes/s]    | 19456.2376 |

N: 2000
# Tempo LU: 2.2s
# Tempo iter: 3.98s
# Tempo Residuo: 3.9s
|    L3 bandwidth [MBytes/s]    | 17263.6703 |
|    L3 bandwidth [MBytes/s]    | 17546.2454 |
