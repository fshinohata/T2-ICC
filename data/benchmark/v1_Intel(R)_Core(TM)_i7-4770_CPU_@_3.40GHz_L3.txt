
N: 32
# Tempo LU: 0.001s
# Tempo iter: 0.002s
# Tempo Residuo: 0.001s
|    L3 bandwidth [MBytes/s]    | 2075.1284 |
|    L3 bandwidth [MBytes/s]    | 1451.7505 |

N: 33
# Tempo LU: 0s
# Tempo iter: 0.001s
# Tempo Residuo: 0.001s
|    L3 bandwidth [MBytes/s]    | 2341.7530 |
|    L3 bandwidth [MBytes/s]    | 2022.5912 |

N: 64
# Tempo LU: 0s
# Tempo iter: 0.003s
# Tempo Residuo: 0.002s
|    L3 bandwidth [MBytes/s]    | 2035.5880 |
|    L3 bandwidth [MBytes/s]    | 1707.7085 |

N: 65
# Tempo LU: 0.001s
# Tempo iter: 0.003s
# Tempo Residuo: 0.003s
|    L3 bandwidth [MBytes/s]    | 1877.2641 |
|    L3 bandwidth [MBytes/s]    | 1572.4266 |

N: 128
# Tempo LU: 0.001s
# Tempo iter: 0.01s
# Tempo Residuo: 0.01s
|    L3 bandwidth [MBytes/s]    | 2761.8249 |
|    L3 bandwidth [MBytes/s]    | 2608.0918 |

N: 129
# Tempo LU: 0.003s
# Tempo iter: 0.01s
# Tempo Residuo: 0.011s
|    L3 bandwidth [MBytes/s]    | 2546.9661 |
|    L3 bandwidth [MBytes/s]    | 2480.2944 |

N: 256
# Tempo LU: 0.015s
# Tempo iter: 0.07s
# Tempo Residuo: 0.071s
|    L3 bandwidth [MBytes/s]    | 2387.0105 |
|    L3 bandwidth [MBytes/s]    | 2242.5839 |

N: 257
# Tempo LU: 0.016s
# Tempo iter: 0.07s
# Tempo Residuo: 0.073s
|    L3 bandwidth [MBytes/s]    | 2398.9483 |
|    L3 bandwidth [MBytes/s]    | 2268.8328 |

N: 512
# Tempo LU: 0.083s
# Tempo iter: 0.484s
# Tempo Residuo: 0.541s
|    L3 bandwidth [MBytes/s]    | 2448.8205 |
|    L3 bandwidth [MBytes/s]    | 2099.9423 |

N: 1000
# Tempo LU: 0.316s
# Tempo iter: 3.65s
# Tempo Residuo: 3.72s
|    L3 bandwidth [MBytes/s]    | 2343.1805 |
|    L3 bandwidth [MBytes/s]    | 2198.6337 |

N: 2000
# Tempo LU: 2.43s
# Tempo iter: 29s
# Tempo Residuo: 29.1s
|    L3 bandwidth [MBytes/s]    | 2333.9207 |
|    L3 bandwidth [MBytes/s]    | 2274.7874 |
