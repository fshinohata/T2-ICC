	.file	"matrix.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"ERROR: in lu_decomposition(): Could not allocate memory for copy matrix\n"
	.align 8
.LC5:
	.string	"ERROR: lu_decomposition(): Received matrix is not inversible.\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4,,15
	.globl	lu_decomposition
	.type	lu_decomposition, @function
lu_decomposition:
.LFB4642:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movslq	36(%rdi), %rbx
	movq	%rdi, -96(%rbp)
	movl	32(%rdi), %r13d
	movq	40(%rdi), %rdx
	movl	%ebx, %edi
	imull	%r13d, %edi
	movq	%rdx, -152(%rbp)
	movslq	%edi, %rdi
	salq	$3, %rdi
	call	malloc
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L2
	movq	%rax, %rdi
	testl	%r13d, %r13d
	jle	.L4
	movq	%rax, %rcx
	movq	-96(%rbp), %rax
	movslq	%r13d, %r12
	salq	$3, %rbx
	salq	$3, %r12
	xorl	%r15d, %r15d
	movq	(%rax), %r14
.L6:
	movq	%r14, %rsi
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memcpy
	addl	$1, %r15d
	addq	%rbx, %r14
	movq	%rax, %rcx
	addq	%rbx, %rcx
	cmpl	%r15d, %r13d
	jne	.L6
	movq	-152(%rbp), %rax
	movl	$0, -72(%rbp)
	vxorpd	%xmm4, %xmm4, %xmm4
	vmovsd	.LC1(%rip), %xmm5
	movq	$0, -144(%rbp)
	vmovsd	.LC4(%rip), %xmm7
	vmovsd	.LC3(%rip), %xmm8
	vmovapd	%xmm5, %xmm6
	movq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	movl	36(%rax), %r14d
	movl	-72(%rbp), %eax
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L62:
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%r13d, %eax
	jge	.L139
	movl	%eax, %ecx
	movq	-136(%rbp), %rax
	movl	-72(%rbp), %r10d
	movq	-80(%rbp), %r9
	movq	-152(%rbp), %r8
	movl	(%rax), %edi
	movl	%r15d, %eax
	imull	%edi, %eax
	addl	%r10d, %eax
	cltq
	vmovsd	(%r9,%rax,8), %xmm2
	movslq	%ecx, %rax
	leaq	(%r8,%rax,4), %rdx
	movl	%ecx, %eax
	movl	%r10d, %ecx
	.p2align 4,,10
	.p2align 3
.L27:
	vucomisd	%xmm2, %xmm4
	vmovapd	%xmm2, %xmm1
	jbe	.L10
	vxorpd	%xmm5, %xmm1, %xmm1
.L10:
	movl	(%rdx), %r11d
	movl	%r11d, %esi
	imull	%r15d, %esi
	addl	%r10d, %esi
	movslq	%esi, %rsi
	vmovsd	(%r9,%rsi,8), %xmm3
	vucomisd	%xmm3, %xmm4
	vmovapd	%xmm3, %xmm0
	jbe	.L13
	vxorpd	%xmm6, %xmm0, %xmm0
.L13:
	vucomisd	%xmm1, %xmm0
	movslq	%ecx, %rsi
	vcmpltsd	%xmm0, %xmm1, %xmm0
	leaq	(%r8,%rsi,4), %rsi
	cmova	%eax, %ecx
	cmova	%rdx, %rsi
	vandpd	%xmm0, %xmm3, %xmm3
	vandnpd	%xmm2, %xmm0, %xmm2
	vorpd	%xmm3, %xmm2, %xmm2
	cmova	%r11d, %edi
	addl	$1, %eax
	addq	$4, %rdx
	cmpl	%r13d, %eax
	jne	.L27
.L8:
	vucomisd	%xmm4, %xmm2
	jp	.L83
	je	.L28
.L83:
	vucomisd	%xmm2, %xmm4
	ja	.L140
	vucomisd	%xmm2, %xmm7
	jnb	.L28
.L32:
	movl	-72(%rbp), %eax
	cmpl	%eax, %ecx
	je	.L35
	movq	-96(%rbp), %rdx
	movl	%eax, %r10d
	imull	%r15d, %ecx
	imull	%r15d, %r10d
	movl	-144(%rbp), %r9d
	movslq	%ecx, %r11
	movq	8(%rdx), %rbx
	leaq	0(,%r11,8), %rcx
	movslq	%r10d, %r10
	leaq	0(,%r10,8), %r8
	movq	%rbx, -88(%rbp)
	leaq	(%rbx,%r8), %rax
	leaq	(%rbx,%rcx), %rdx
	testl	%r9d, %r9d
	je	.L44
	leaq	32(,%r10,8), %r9
	movq	-144(%rbp), %r14
	cmpq	%rcx, %r9
	leaq	32(,%r11,8), %r9
	setle	%cl
	cmpq	%r8, %r9
	setle	%r8b
	movl	%r14d, %ebx
	orb	%r8b, %cl
	je	.L37
	cmpl	$8, %r14d
	jbe	.L37
	movq	%rax, %rcx
	andl	$31, %ecx
	shrq	$3, %rcx
	negq	%rcx
	andl	$3, %ecx
	cmpl	%r14d, %ecx
	cmova	%r14d, %ecx
	testl	%ecx, %ecx
	je	.L76
	vmovsd	(%rax), %xmm0
	leaq	8(%rax), %r12
	movl	$1, %r14d
	vmovsd	(%rdx), %xmm1
	leaq	8(%rdx), %r13
	vmovsd	%xmm1, (%rax)
	vmovsd	%xmm0, (%rdx)
	cmpl	$1, %ecx
	je	.L38
	vmovsd	8(%rax), %xmm0
	leaq	16(%rax), %r12
	movl	$2, %r14d
	vmovsd	8(%rdx), %xmm1
	leaq	16(%rdx), %r13
	vmovsd	%xmm1, 8(%rax)
	vmovsd	%xmm0, 8(%rdx)
	cmpl	$3, %ecx
	jne	.L38
	vmovsd	16(%rax), %xmm0
	leaq	24(%rax), %r12
	movl	$3, %r14d
	vmovsd	16(%rdx), %xmm1
	leaq	24(%rdx), %r13
	vmovsd	%xmm1, 16(%rax)
	vmovsd	%xmm0, 16(%rdx)
.L38:
	subl	%ecx, %ebx
	movq	-88(%rbp), %r9
	movl	%ecx, %eax
	xorl	%r8d, %r8d
	leal	-4(%rbx), %edx
	addq	%rax, %r10
	addq	%r11, %rax
	shrl	$2, %edx
	movl	%ebx, %r15d
	addl	$1, %edx
	leaq	(%r9,%r10,8), %rcx
	leal	0(,%rdx,4), %ebx
	leaq	(%r9,%rax,8), %r9
	xorl	%eax, %eax
.L40:
	vmovapd	(%rcx,%rax), %ymm0
	addl	$1, %r8d
	vmovupd	(%r9,%rax), %ymm1
	vmovapd	%ymm1, (%rcx,%rax)
	vmovupd	%ymm0, (%r9,%rax)
	addq	$32, %rax
	cmpl	%edx, %r8d
	jb	.L40
	movl	%ebx, %eax
	addl	%ebx, %r14d
	salq	$3, %rax
	addq	%rax, %r12
	addq	%r13, %rax
	cmpl	%ebx, %r15d
	je	.L44
	movl	-72(%rbp), %ebx
	leal	1(%r14), %edx
	vmovsd	(%r12), %xmm0
	vmovsd	(%rax), %xmm1
	vmovsd	%xmm1, (%r12)
	vmovsd	%xmm0, (%rax)
	cmpl	%ebx, %edx
	jge	.L44
	vmovsd	8(%r12), %xmm0
	addl	$2, %r14d
	vmovsd	8(%rax), %xmm1
	vmovsd	%xmm1, 8(%r12)
	vmovsd	%xmm0, 8(%rax)
	cmpl	%ebx, %r14d
	jge	.L44
	vmovsd	16(%r12), %xmm0
	vmovsd	16(%rax), %xmm1
	vmovsd	%xmm1, 16(%r12)
	vmovsd	%xmm0, 16(%rax)
.L44:
	movq	-136(%rbp), %rax
	xorl	(%rax), %edi
	movl	%edi, (%rsi)
	xorl	(%rax), %edi
	movl	%edi, (%rax)
	movq	-96(%rbp), %rax
	xorl	%edi, (%rsi)
	movl	32(%rax), %r13d
	movl	36(%rax), %r15d
.L35:
	movq	-136(%rbp), %rbx
	movl	-60(%rbp), %r9d
	movl	(%rbx), %eax
	movq	-80(%rbp), %rbx
	imull	%r15d, %eax
	cltq
	leaq	(%rbx,%rax,8), %rsi
	movslq	%r9d, %rax
	cmpl	%r13d, %eax
	jge	.L47
	movq	-144(%rbp), %rdx
	movl	%r15d, -56(%rbp)
	salq	$3, %rdx
	leaq	(%rsi,%rdx), %rbx
	movq	%rdx, -112(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rbx, -104(%rbp)
	leaq	(%rdx,%rax,4), %rbx
	salq	$3, %rax
	movq	%rax, -128(%rbp)
	leal	-4(%r13), %edx
	addq	%rsi, %rax
	movq	%rbx, %r15
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-56(%rbp), %eax
	movq	-80(%rbp), %rbx
	imull	(%r15), %eax
	cltq
	leaq	(%rbx,%rax,8), %rcx
	movq	-112(%rbp), %rax
	vmovsd	(%rcx,%rax), %xmm2
	movq	-104(%rbp), %rax
	vdivsd	(%rax), %xmm2, %xmm2
	movl	-60(%rbp), %eax
	cmpl	%edx, %eax
	jge	.L48
	movq	-128(%rbp), %rbx
	vbroadcastsd	%xmm2, %ymm1
	movq	-120(%rbp), %r8
	leaq	(%rcx,%rbx), %rdi
	movq	-96(%rbp), %rbx
	movl	32(%rbx), %r11d
	leal	-4(%r11), %r10d
	.p2align 4,,10
	.p2align 3
.L49:
#APP
# 106 "src/matrix.c" 1
	# [START] AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%r8), %ymm0
	vfnmadd213pd	(%rdi), %ymm1, %ymm0
	vmovupd	%ymm0, (%rdi)
#APP
# 108 "src/matrix.c" 1
	# [END] AVX CHECK
# 0 "" 2
#NO_APP
	addl	$4, %eax
	movl	%r11d, %r13d
	addq	$32, %r8
	addq	$32, %rdi
	movl	%r10d, %edx
	cmpl	%eax, %r10d
	jg	.L49
.L48:
	cmpl	%r13d, %eax
	jge	.L59
	movslq	%eax, %rbx
	movl	%r13d, %edi
	leaq	32(,%rbx,8), %r10
	subl	%eax, %edi
	leaq	-32(%r10), %r11
	leaq	(%rsi,%r10), %r12
	leaq	(%rcx,%r11), %r8
	cmpq	%r12, %r8
	leaq	(%rsi,%r11), %r12
	setnb	%r14b
	addq	%rcx, %r10
	cmpq	%r10, %r12
	setnb	%r10b
	orb	%r14b, %r10b
	je	.L52
	cmpl	$7, %edi
	jbe	.L52
	andl	$31, %r8d
	movq	%r8, %r10
	shrq	$3, %r10
	negq	%r10
	andl	$3, %r10d
	cmpl	%edi, %r10d
	cmova	%edi, %r10d
	testl	%r10d, %r10d
	je	.L53
	leaq	(%rcx,%rbx,8), %r8
	vmovsd	(%rsi,%rbx,8), %xmm0
	vfnmadd213sd	(%r8), %xmm2, %xmm0
	vmovsd	%xmm0, (%r8)
	leal	1(%rax), %r8d
	cmpl	$1, %r10d
	je	.L81
	movslq	%r8d, %r8
	leaq	(%rcx,%r8,8), %r12
	vmovsd	(%rsi,%r8,8), %xmm0
	leal	2(%rax), %r8d
	vfnmadd213sd	(%r12), %xmm2, %xmm0
	vmovsd	%xmm0, (%r12)
	cmpl	$3, %r10d
	jne	.L81
	movslq	%r8d, %r8
	addl	$3, %eax
	leaq	(%rcx,%r8,8), %r12
	vmovsd	(%rsi,%r8,8), %xmm0
	vfnmadd213sd	(%r12), %xmm2, %xmm0
	vmovsd	%xmm0, (%r12)
.L53:
	subl	%r10d, %edi
	movl	%r10d, %r8d
	vbroadcastsd	%xmm2, %ymm1
	leal	-4(%rdi), %r10d
	addq	%r8, %rbx
	leaq	(%r11,%r8,8), %r11
	shrl	$2, %r10d
	xorl	%r8d, %r8d
	leaq	(%rcx,%rbx,8), %r12
	addl	$1, %r10d
	xorl	%ebx, %ebx
	leal	0(,%r10,4), %r14d
	movl	%r14d, -64(%rbp)
	leaq	(%rsi,%r11), %r14
	addq	%rcx, %r11
.L55:
	addl	$1, %ebx
	vmovupd	(%r14,%r8), %ymm0
	vfnmadd213pd	(%r12,%r8), %ymm1, %ymm0
	vmovapd	%ymm0, (%r11,%r8)
	addq	$32, %r8
	cmpl	%r10d, %ebx
	jb	.L55
	movl	-64(%rbp), %ebx
	addl	%ebx, %eax
	cmpl	%edi, %ebx
	je	.L59
	movslq	%eax, %r8
	leaq	(%rcx,%r8,8), %rdi
	vmovsd	(%rsi,%r8,8), %xmm0
	vfnmadd213sd	(%rdi), %xmm2, %xmm0
	vmovsd	%xmm0, (%rdi)
	leal	1(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L59
	movslq	%edi, %rdi
	addl	$2, %eax
	leaq	(%rcx,%rdi,8), %r8
	vmovsd	(%rsi,%rdi,8), %xmm0
	vfnmadd213sd	(%r8), %xmm2, %xmm0
	vmovsd	%xmm0, (%r8)
	cmpl	%eax, %r13d
	jle	.L59
	cltq
	leaq	(%rcx,%rax,8), %rcx
	vmovsd	(%rsi,%rax,8), %xmm0
	vfnmadd213sd	(%rcx), %xmm2, %xmm0
	vmovsd	%xmm0, (%rcx)
.L59:
	movl	-56(%rbp), %eax
	addq	$4, %r15
	movq	-88(%rbp), %rbx
	imull	%r9d, %eax
	addl	-72(%rbp), %eax
	addl	$1, %r9d
	cltq
	vmovsd	%xmm2, (%rbx,%rax,8)
	cmpl	%r13d, %r9d
	jl	.L51
	movl	-56(%rbp), %r15d
.L47:
	movl	-60(%rbp), %eax
	addq	$1, -144(%rbp)
	addq	$4, -136(%rbp)
	movl	%eax, -72(%rbp)
	cmpl	%r13d, %eax
	jl	.L62
	movl	%r15d, %r14d
	testl	%r13d, %r13d
	jle	.L133
	movq	-96(%rbp), %rax
	movl	%r14d, -112(%rbp)
	movl	%r13d, %ebx
	xorl	%r9d, %r9d
	movq	$0, -56(%rbp)
	xorl	%r12d, %r12d
	movq	-80(%rbp), %r14
	movq	16(%rax), %r10
	movslq	%r15d, %rax
	xorl	%r15d, %r15d
	leaq	8(,%rax,8), %rax
	movq	%rax, -104(%rbp)
	subq	$8, %rax
	movq	%rax, -88(%rbp)
	movq	%r10, %rcx
	jmp	.L75
.L141:
	movq	-56(%rbp), %r8
	leaq	(%r14,%rdi), %rax
	movq	%rax, %rsi
	vmovsd	(%rax,%r8), %xmm0
	leal	1(%r12), %r8d
	movl	%r8d, %eax
	vmovsd	%xmm0, (%rcx)
	cmpl	$1, %edx
	je	.L66
	movslq	%r8d, %rax
	salq	$3, %rax
	leaq	(%r14,%rax), %r11
	addq	%r10, %rax
	vmovsd	(%r11,%rdi), %xmm0
	vmovsd	%xmm0, (%rax,%r9)
	leal	2(%r12), %eax
	cmpl	$3, %edx
	jne	.L66
	cltq
	salq	$3, %rax
	vmovsd	(%rsi,%rax), %xmm0
	addq	%r10, %rax
	vmovsd	%xmm0, (%rax,%r9)
	leal	3(%r12), %eax
.L66:
	cmpl	-60(%rbp), %edx
	je	.L134
.L65:
	movl	-60(%rbp), %r11d
	subl	$1, %ebx
	movl	%edx, %esi
	subl	%edx, %r11d
	movl	%r11d, -128(%rbp)
	subl	$4, %r11d
	shrl	$2, %r11d
	addl	$1, %r11d
	movl	%r11d, -120(%rbp)
	sall	$2, %r11d
	cmpl	%r13d, %r12d
	movl	%r11d, -60(%rbp)
	movl	$0, %r11d
	cmovl	%ebx, %r11d
	subl	%edx, %r11d
	cmpl	$2, %r11d
	jbe	.L68
	salq	$3, %rsi
	movq	-72(%rbp), %rdx
	movq	%rsi, %r11
	addq	-96(%rbp), %r11
	addq	%rsi, %rdx
	xorl	%esi, %esi
	leaq	(%r10,%r11), %r12
	addq	%r14, %rdx
	xorl	%r11d, %r11d
.L69:
	vmovapd	(%rdx,%rsi), %ymm0
	addl	$1, %r11d
	vmovupd	%ymm0, (%r12,%rsi)
	addq	$32, %rsi
	cmpl	%r11d, -120(%rbp)
	ja	.L69
	movl	-60(%rbp), %edx
	addl	%edx, %eax
	cmpl	-128(%rbp), %edx
	je	.L74
.L68:
	movslq	%eax, %rdx
	addq	%r14, %rdi
	salq	$3, %rdx
	vmovsd	(%rdi,%rdx), %xmm0
	addq	%r10, %rdx
	vmovsd	%xmm0, (%rdx,%r9)
	leal	1(%rax), %edx
	cmpl	%r13d, %edx
	jge	.L74
	leaq	(%r10,%r9), %rsi
	movslq	%edx, %rdx
	addl	$2, %eax
	salq	$3, %rdx
	vmovsd	(%rdi,%rdx), %xmm0
	vmovsd	%xmm0, (%rsi,%rdx)
	cmpl	%eax, %r13d
	jle	.L74
	cltq
	salq	$3, %rax
	vmovsd	(%rdi,%rax), %xmm0
	addq	%r10, %rax
	vmovsd	%xmm0, (%rax,%r9)
.L74:
	movl	%r8d, %r12d
	addq	$1, %r15
	addq	-104(%rbp), %rcx
	addq	$8, -56(%rbp)
	addq	-88(%rbp), %r9
	cmpl	%r13d, %r8d
	je	.L133
.L75:
	movq	-152(%rbp), %rax
	movq	%rcx, %r11
	movl	-112(%rbp), %edx
	subq	%r10, %r11
	movq	%r11, -96(%rbp)
	imull	(%rax,%r15,4), %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%r15), %rsi
	leaq	0(,%rdx,8), %rdi
	leaq	4(%rdx,%r15), %rdx
	leaq	0(,%rsi,8), %rax
	leaq	(%r14,%rdx,8), %rdx
	movq	%rax, -72(%rbp)
	addq	%r14, %rax
	cmpq	%rdx, %rcx
	leaq	32(%rcx), %rdx
	setnb	%r8b
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %r8b
	je	.L64
	cmpl	$12, %ebx
	seta	%r8b
	cmpl	%r13d, %r12d
	setl	%dl
	testb	%dl, %r8b
	je	.L64
	cmpl	%r13d, %r12d
	movl	$1, %r11d
	cmovl	%ebx, %r11d
	andl	$31, %eax
	movq	%rax, %rdx
	shrq	$3, %rdx
	movl	%r11d, -60(%rbp)
	negq	%rdx
	andl	$3, %edx
	cmpl	%r11d, %edx
	cmova	%r11d, %edx
	testl	%edx, %edx
	jne	.L141
	leal	1(%r12), %r8d
	movl	%r12d, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L81:
	movl	%r8d, %eax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L52:
	leal	-1(%r13), %ecx
	subl	%eax, %ecx
	xorl	%eax, %eax
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L60:
	vmovsd	(%r12,%rax,8), %xmm0
	vfnmadd213sd	(%r8,%rax,8), %xmm2, %xmm0
	vmovsd	%xmm0, (%r8,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L60
	jmp	.L59
.L140:
	vucomisd	%xmm8, %xmm2
	jb	.L32
.L28:
	movl	$62, %edx
	movl	$1, %esi
	movl	$.LC5, %edi
	movq	stderr(%rip), %rcx
	vzeroupper
	call	fwrite
	movl	$32, %eax
.L131:
	subq	$-128, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	movq	%rdx, %r13
	movq	%rax, %r12
	xorl	%r14d, %r14d
	jmp	.L38
.L37:
	movl	-60(%rbp), %ebx
	leal	-2(%rbx), %ecx
	leaq	8(%rax,%rcx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L45:
	vmovsd	(%rax), %xmm0
	addq	$8, %rdx
	addq	$8, %rax
	vmovsd	-8(%rdx), %xmm1
	vmovsd	%xmm1, -8(%rax)
	vmovsd	%xmm0, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L45
	jmp	.L44
.L139:
	movq	-136(%rbp), %rax
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rbx
	movl	(%rax), %edi
	movq	%rax, %rsi
	movl	%r15d, %eax
	movl	%edx, %ecx
	imull	%edi, %eax
	addl	%edx, %eax
	cltq
	vmovsd	(%rbx,%rax,8), %xmm2
	jmp	.L8
.L64:
	movq	-56(%rbp), %rsi
	movl	%r12d, %edx
	xorl	%eax, %eax
	addq	%rdi, %rsi
	addq	%r14, %rsi
.L73:
	vmovsd	(%rsi,%rax), %xmm0
	addl	$1, %edx
	vmovsd	%xmm0, (%rcx,%rax)
	addq	$8, %rax
	cmpl	%r13d, %edx
	jl	.L73
	leal	1(%r12), %r8d
.L134:
	subl	$1, %ebx
	jmp	.L74
.L133:
	vzeroupper
	movq	-80(%rbp), %rdi
.L4:
	call	free
	xorl	%eax, %eax
	jmp	.L131
.L2:
	movl	$72, %edx
	movl	$1, %esi
	movl	$.LC0, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$-1, %eax
	jmp	.L131
	.cfi_endproc
.LFE4642:
	.size	lu_decomposition, .-lu_decomposition
	.section	.text.unlikely
.LCOLDE6:
	.text
.LHOTE6:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"%.17g "
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4,,15
	.globl	mult_lu
	.type	mult_lu, @function
mult_lu:
.LFB4643:
	.cfi_startproc
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jle	.L164
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
.L157:
	testl	%r12d, %r12d
	setg	%bl
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%ebp, %eax
	subl	$0, %eax
	js	.L154
	testb	%bl, %bl
	je	.L154
	movslq	36(%r13), %rsi
	movl	%r12d, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	8(%r13), %rdx
	movq	16(%r13), %rdi
	imull	%esi, %eax
	movq	%rsi, %r8
	salq	$3, %rsi
	cltq
	leaq	(%rdx,%rax,8), %rcx
	xorl	%eax, %eax
	leaq	(%rdi,%r14), %rdx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L165:
	cmpl	%ebp, %eax
	jg	.L153
.L145:
	addl	$1, %eax
	vmovsd	(%rcx), %xmm1
	addq	$8, %rcx
	vfmadd231sd	(%rdx), %xmm1, %xmm0
	addq	%rsi, %rdx
	cmpl	%r12d, %eax
	jl	.L165
.L153:
	imull	%r8d, %eax
.L150:
	addl	%ebp, %eax
	movl	$.LC7, %esi
	addl	$1, %ebp
	cltq
	addq	$8, %r14
	vaddsd	(%rdi,%rax,8), %xmm0, %xmm0
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
	cmpl	%ebp, 32(%r13)
	jg	.L152
	movl	$10, %edi
	addl	$1, %r12d
	call	putchar
	cmpl	%r12d, 32(%r13)
	jg	.L157
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	ret
.L154:
	.cfi_restore_state
	movq	16(%r13), %rdi
	xorl	%eax, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	jmp	.L150
.L164:
	.cfi_def_cfa_offset 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE4643:
	.size	mult_lu, .-mult_lu
	.section	.text.unlikely
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4,,15
	.globl	mult_inverse
	.type	mult_inverse, @function
mult_inverse:
.LFB4644:
	.cfi_startproc
	movl	32(%rdi), %edx
	testl	%edx, %edx
	jle	.L179
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movq	%rsi, %rbp
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
.L173:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L172:
	movl	36(%r12), %eax
	movl	%ebx, %ecx
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	(%r12), %rsi
	imull	%eax, %ecx
	imull	%r13d, %eax
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,8), %rsi
	movq	24(%r12), %rcx
	cltq
	leaq	(%rcx,%rax,8), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L169:
	vmovsd	(%rsi,%rax,8), %xmm1
	vfmadd231sd	(%rcx,%rax,8), %xmm1, %xmm0
	addq	$1, %rax
	cmpl	%eax, %edx
	jg	.L169
	movl	$.LC7, %edx
	movl	$1, %esi
	movq	%rbp, %rdi
	movl	$1, %eax
	addl	$1, %r13d
	call	__fprintf_chk
	movl	32(%r12), %edx
	cmpl	%r13d, %edx
	jg	.L172
	movq	%rbp, %rsi
	movl	$10, %edi
	addl	$1, %ebx
	call	fputc
	movl	32(%r12), %edx
	cmpl	%ebx, %edx
	jg	.L173
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 8
	ret
.L179:
	ret
	.cfi_endproc
.LFE4644:
	.size	mult_inverse, .-mult_inverse
	.section	.text.unlikely
.LCOLDE9:
	.text
.LHOTE9:
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"ERROR: in square_matrix_generate(): size parameter must be higher than 1.\n"
	.section	.text.unlikely
.LCOLDB12:
	.text
.LHOTB12:
	.p2align 4,,15
	.globl	square_matrix_generate
	.type	square_matrix_generate, @function
square_matrix_generate:
.LFB4645:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L195
	movq	(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L195
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	movl	32(%rdi), %r12d
	cmpl	$1, %r12d
	jle	.L182
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%r13d, %r13d
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rbx), %rdx
.L185:
	movl	36(%rbx), %eax
	imull	%ebp, %eax
	addl	%r13d, %eax
	addl	$1, %r13d
	cltq
	leaq	(%rdx,%rax,8), %r14
	call	rand
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sd	%eax, %xmm0, %xmm0
	vmulsd	.LC11(%rip), %xmm0, %xmm0
	vmovsd	%xmm0, (%r14)
	cmpl	%r13d, %r12d
	jne	.L184
	addl	$1, %ebp
	cmpl	%ebp, %r12d
	je	.L192
	movq	(%rbx), %rdx
	jmp	.L183
.L192:
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
.L195:
	ret
.L182:
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	.cfi_offset 6, -40
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	.cfi_offset 14, -16
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 40
	movl	$74, %edx
	movl	$1, %esi
	movl	$.LC10, %edi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa_offset 32
	movq	stderr(%rip), %rcx
	popq	%r12
	.cfi_restore 12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_restore 13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_restore 14
	.cfi_def_cfa_offset 8
	jmp	fwrite
	.cfi_endproc
.LFE4645:
	.size	square_matrix_generate, .-square_matrix_generate
	.section	.text.unlikely
.LCOLDE12:
	.text
.LHOTE12:
	.section	.rodata.str1.1
.LC13:
	.string	"%d\n"
	.section	.text.unlikely
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4,,15
	.globl	print_original
	.type	print_original, @function
print_original:
.LFB4646:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movl	$.LC13, %edx
	movq	%rdi, %r13
	xorl	%eax, %eax
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	xorl	%ebp, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movl	32(%rdi), %ecx
	movq	%r12, %rdi
	call	__fprintf_chk
	movl	32(%r13), %eax
	testl	%eax, %eax
	jle	.L204
	.p2align 4,,10
	.p2align 3
.L201:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L199:
	movl	36(%r13), %eax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	0(%r13), %rdx
	imull	%ebp, %eax
	addl	%ebx, %eax
	addl	$1, %ebx
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC7, %edx
	movl	$1, %eax
	call	__fprintf_chk
	cmpl	%ebx, 32(%r13)
	jg	.L199
	movq	%r12, %rsi
	movl	$10, %edi
	addl	$1, %ebp
	call	fputc
	cmpl	%ebp, 32(%r13)
	jg	.L201
.L204:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE4646:
	.size	print_original, .-print_original
	.section	.text.unlikely
.LCOLDE14:
	.text
.LHOTE14:
	.section	.text.unlikely
.LCOLDB15:
	.text
.LHOTB15:
	.p2align 4,,15
	.globl	print_inverse
	.type	print_inverse, @function
print_inverse:
.LFB4647:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movl	$.LC13, %edx
	movq	%rdi, %r13
	xorl	%eax, %eax
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	xorl	%ebp, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movl	32(%rdi), %ecx
	movq	%r12, %rdi
	call	__fprintf_chk
	movl	32(%r13), %eax
	testl	%eax, %eax
	jle	.L215
	.p2align 4,,10
	.p2align 3
.L212:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L210:
	movl	36(%r13), %eax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	24(%r13), %rdx
	imull	%ebx, %eax
	addl	$1, %ebx
	addl	%ebp, %eax
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC7, %edx
	movl	$1, %eax
	call	__fprintf_chk
	cmpl	%ebx, 32(%r13)
	jg	.L210
	movq	%r12, %rsi
	movl	$10, %edi
	addl	$1, %ebp
	call	fputc
	cmpl	%ebp, 32(%r13)
	jg	.L212
.L215:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE4647:
	.size	print_inverse, .-print_inverse
	.section	.text.unlikely
.LCOLDE15:
	.text
.LHOTE15:
	.section	.rodata.str1.1
.LC16:
	.string	"L:\n"
.LC17:
	.string	"0 "
.LC18:
	.string	"U:\n"
	.section	.text.unlikely
.LCOLDB19:
	.text
.LHOTB19:
	.p2align 4,,15
	.globl	print_lu
	.type	print_lu, @function
print_lu:
.LFB4648:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movq	%rsi, %rcx
	movl	$3, %edx
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	$1, %esi
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movq	%rdi, %rbp
	movl	$.LC16, %edi
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	call	fwrite
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jle	.L220
	.p2align 4,,10
	.p2align 3
.L232:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L221:
	movl	36(%rbp), %eax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	8(%rbp), %rdx
	imull	%r13d, %eax
	addl	%ebx, %eax
	addl	$1, %ebx
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC7, %edx
	movl	$1, %eax
	call	__fprintf_chk
	cmpl	%r13d, %ebx
	jle	.L221
	cmpl	32(%rbp), %ebx
	jge	.L225
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rcx
	movl	$2, %edx
	movl	$1, %esi
	movl	$.LC17, %edi
	addl	$1, %ebx
	call	fwrite
	cmpl	%ebx, 32(%rbp)
	jg	.L233
.L225:
	movq	%r12, %rsi
	movl	$10, %edi
	addl	$1, %r13d
	call	fputc
	cmpl	%r13d, 32(%rbp)
	jg	.L232
.L220:
	movq	%r12, %rsi
	movl	$10, %edi
	call	fputc
	movq	%r12, %rcx
	movl	$3, %edx
	movl	$1, %esi
	movl	$.LC18, %edi
	call	fwrite
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jle	.L226
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	%eax, %r13d
	jge	.L229
	.p2align 4,,10
	.p2align 3
.L230:
	movl	36(%rbp), %eax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	16(%rbp), %rdx
	imull	%ebx, %eax
	addl	%r13d, %eax
	addl	$1, %r13d
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC7, %edx
	movl	$1, %eax
	call	__fprintf_chk
	cmpl	%r13d, 32(%rbp)
	jg	.L230
.L229:
	leal	1(%rbx), %r13d
	movq	%r12, %rsi
	movl	$10, %edi
	call	fputc
	cmpl	32(%rbp), %r13d
	jge	.L226
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r12, %rcx
	movl	$2, %edx
	movl	$1, %esi
	movl	$.LC17, %edi
	addl	$1, %ebx
	call	fwrite
	cmpl	%ebx, %r13d
	jne	.L228
	movl	32(%rbp), %eax
	movl	%r13d, %ebx
	jmp	.L227
.L226:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	movq	%r12, %rsi
	movl	$10, %edi
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	jmp	fputc
	.cfi_endproc
.LFE4648:
	.size	print_lu, .-print_lu
	.section	.text.unlikely
.LCOLDE19:
	.text
.LHOTE19:
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"ERROR: in invmat_init(): Could not allocate struct.\n"
	.align 8
.LC21:
	.string	"ERROR: in invmat_init(): Could not allocate matrixes for size %ld.\n"
	.section	.text.unlikely
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4,,15
	.globl	invmat_init
	.type	invmat_init, @function
invmat_init:
.LFB4649:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	movq	%rdi, %r15
	movl	$48, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$32, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	call	malloc
	testq	%rax, %rax
	je	.L284
	movq	%rax, %r14
	movq	$0, (%rax)
	movl	$64, %esi
	movq	$0, 8(%rax)
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 40(%rax)
	movl	%r15d, 32(%rax)
	leaq	-1(%r15), %rax
	movq	%rax, -64(%rbp)
	shrq	$3, %rax
	leal	8(,%rax,8), %ebx
	movl	%ebx, 36(%r14)
	movslq	%ebx, %rbx
	movq	%rbx, %r12
	imulq	%r15, %r12
	salq	$3, %r12
	movq	%r12, %rdx
	call	posix_memalign
	movq	(%r14), %rax
	leaq	8(%r14), %rdi
	movq	%r12, %rdx
	movl	$64, %esi
	movq	%rax, -56(%rbp)
	call	posix_memalign
	movq	8(%r14), %r13
	leaq	24(%r14), %rdi
	movq	%r12, %rdx
	movl	$64, %esi
	movq	%r13, 16(%r14)
	call	posix_memalign
	leaq	40(%r14), %rdi
	movl	$64, %esi
	leaq	0(,%rbx,4), %rdx
	call	posix_memalign
	cmpq	$0, -56(%rbp)
	je	.L250
	cmpq	$0, 24(%r14)
	je	.L250
	testq	%r13, %r13
	je	.L250
	movq	40(%r14), %rdx
	testq	%rdx, %rdx
	je	.L250
	testq	%r15, %r15
	jle	.L255
	movq	%rdx, %rax
	andl	$31, %eax
	shrq	$2, %rax
	negq	%rax
	andl	$7, %eax
	cmpq	%r15, %rax
	cmova	%r15, %rax
	cmpq	$10, %r15
	jg	.L285
	movq	%r15, %rax
.L252:
	movl	$0, (%rdx)
	cmpq	$1, %rax
	je	.L263
	movl	$1, 4(%rdx)
	cmpq	$2, %rax
	je	.L264
	movl	$2, 8(%rdx)
	cmpq	$3, %rax
	je	.L265
	movl	$3, 12(%rdx)
	cmpq	$4, %rax
	je	.L266
	movl	$4, 16(%rdx)
	cmpq	$5, %rax
	je	.L267
	movl	$5, 20(%rdx)
	cmpq	$6, %rax
	je	.L268
	movl	$6, 24(%rdx)
	cmpq	$7, %rax
	je	.L269
	movl	$7, 28(%rdx)
	cmpq	$8, %rax
	je	.L270
	movl	$8, 32(%rdx)
	cmpq	$10, %rax
	jne	.L271
	movl	$9, 36(%rdx)
	movl	$10, %esi
.L254:
	cmpq	%rax, %r15
	je	.L255
.L253:
	movq	%r15, %r10
	movq	-64(%rbp), %rcx
	subq	%rax, %r10
	leaq	-8(%r10), %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	subq	%rax, %rcx
	leaq	0(,%rdi,8), %r11
	cmpq	$6, %rcx
	jbe	.L256
	vmovdqa	.LC23(%rip), %ymm1
	leaq	(%rdx,%rax,4), %r9
	vmovd	%esi, %xmm0
	xorl	%eax, %eax
	vpbroadcastd	%xmm0, %ymm0
	vpaddd	.LC22(%rip), %ymm0, %ymm0
.L257:
	addq	$1, %rax
	vmovdqa	%ymm0, (%r9)
	addq	$32, %r9
	vpaddd	%ymm1, %ymm0, %ymm0
	cmpq	%rax, %rdi
	ja	.L257
	addl	%r11d, %esi
	cmpq	%r11, %r10
	je	.L280
	vzeroupper
.L256:
	leal	1(%rsi), %edi
	movslq	%esi, %rax
	movl	%esi, (%rdx,%rax,4)
	movslq	%edi, %rax
	cmpq	%rax, %r15
	jle	.L255
	movl	%edi, (%rdx,%rax,4)
	leal	2(%rsi), %edi
	movslq	%edi, %rax
	cmpq	%rax, %r15
	jle	.L255
	movl	%edi, (%rdx,%rax,4)
	leal	3(%rsi), %edi
	movslq	%edi, %rax
	cmpq	%rax, %r15
	jle	.L255
	movl	%edi, (%rdx,%rax,4)
	leal	4(%rsi), %edi
	movslq	%edi, %rax
	cmpq	%rax, %r15
	jle	.L255
	movl	%edi, (%rdx,%rax,4)
	leal	5(%rsi), %edi
	movslq	%edi, %rax
	cmpq	%rax, %r15
	jle	.L255
	addl	$6, %esi
	movl	%edi, (%rdx,%rax,4)
	movslq	%esi, %rax
	cmpq	%rax, %r15
	jle	.L255
	movl	%esi, (%rdx,%rax,4)
.L255:
	movq	%r14, %rax
.L281:
	addq	$32, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L252
	xorl	%esi, %esi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$8, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$1, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$2, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$3, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$4, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$9, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$5, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$6, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L269:
	movl	$7, %esi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L280:
	vzeroupper
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L250:
	movq	stderr(%rip), %rdi
	movq	%r15, %rcx
	movl	$.LC21, %edx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk
	movq	%r14, %rdi
	call	free
	xorl	%eax, %eax
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$52, %edx
	movl	$1, %esi
	movl	$.LC20, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L281
	.cfi_endproc
.LFE4649:
	.size	invmat_init, .-invmat_init
	.section	.text.unlikely
.LCOLDE24:
	.text
.LHOTE24:
	.section	.text.unlikely
.LCOLDB25:
	.text
.LHOTB25:
	.p2align 4,,15
	.globl	invmat_free
	.type	invmat_free, @function
invmat_free:
.LFB4650:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L298
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	free
.L288:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	free
.L289:
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 8
	jmp	free
	.p2align 4,,10
	.p2align 3
.L298:
	ret
	.cfi_endproc
.LFE4650:
	.size	invmat_free, .-invmat_free
	.section	.text.unlikely
.LCOLDE25:
	.text
.LHOTE25:
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	-1129316352
	.align 8
.LC4:
	.long	0
	.long	1018167296
	.align 8
.LC11:
	.long	2097152
	.long	1040187392
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC22:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.align 32
.LC23:
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
	.section	.note.GNU-stack,"",@progbits
