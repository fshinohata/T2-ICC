	.file	"args.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	str_is_number
	.type	str_is_number, @function
str_is_number:
.LFB61:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L10
	movzbl	(%rdi), %eax
	leal	-43(%rax), %edx
	andl	$253, %edx
	jne	.L13
	movzbl	1(%rdi), %eax
	addq	$1, %rdi
	testb	%al, %al
	je	.L15
	.p2align 4,,10
	.p2align 3
.L6:
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L10
	addq	$1, %rdi
	movzbl	(%rdi), %eax
.L13:
	testb	%al, %al
	jne	.L6
.L15:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE61:
	.size	str_is_number, .-str_is_number
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Error: You must specify the number of refinement iterations to run!\nUsage: invmat [OPTIONS] -i <number_of_iterations> [OPTIONS]\n"
	.align 8
.LC2:
	.string	"Error: refinement iteration count must be >= 0.\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"w"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"Error while opening output file"
	.align 8
.LC5:
	.string	"Error: Random matrix size must be inside the interval 1 < n < 32768\n"
	.section	.rodata.str1.1
.LC6:
	.string	"r"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"Error while opening input file"
	.align 8
.LC8:
	.string	"Error: invalid value for option -i (must be a number)."
	.align 8
.LC9:
	.string	"Error: Parameter '-r' must be followed by a number\n"
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4,,15
	.globl	get_args
	.type	get_args, @function
get_args:
.LFB62:
	.cfi_startproc
	movq	stdin(%rip), %rax
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	$0, 24(%rdi)
	movq	%rax, (%rdi)
	movq	stdout(%rip), %rax
	movq	$-1, 16(%rdi)
	movq	%rax, 8(%rdi)
	cmpl	$2, %esi
	jle	.L36
	leal	-3(%rsi), %eax
	movq	%rdi, %rbp
	leaq	16(%rdx), %r12
	leaq	24(%rdx,%rax,8), %rbx
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-8(%r12), %rax
	cmpb	$45, (%rax)
	jne	.L27
	movzbl	1(%rax), %edx
	cmpb	$105, %dl
	jne	.L19
	cmpb	$0, 2(%rax)
	jne	.L27
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L21
	movzbl	(%rdi), %eax
	movq	%rdi, %rdx
	leal	-43(%rax), %ecx
	andl	$253, %ecx
	jne	.L22
	movzbl	1(%rdi), %eax
	leaq	1(%rdi), %rdx
.L22:
	testb	%al, %al
	jne	.L26
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L23
.L26:
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L64
.L21:
	movl	$54, %edx
	movl	$1, %esi
	movl	$.LC8, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L35
	cmpq	$0, 16(%rbp)
	js	.L36
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$10, %edx
	call	strtol
	testq	%rax, %rax
	js	.L65
	movq	%rax, 16(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L19:
	cmpb	$111, %dl
	jne	.L28
	cmpb	$0, 2(%rax)
	jne	.L27
	movq	(%r12), %rdi
	movl	$.LC3, %esi
	call	fopen
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	jne	.L27
	movl	$.LC4, %edi
	call	perror
	movl	$4, %edi
	call	exit
	.p2align 4,,10
	.p2align 3
.L28:
	cmpb	$114, %dl
	jne	.L20
	cmpb	$0, 2(%rax)
	jne	.L27
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	movzbl	(%rdi), %eax
	movq	%rdi, %rdx
	leal	-43(%rax), %ecx
	andl	$253, %ecx
	jne	.L30
	movzbl	1(%rdi), %eax
	leaq	1(%rdi), %rdx
.L30:
	testb	%al, %al
	jne	.L34
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L31
.L34:
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L66
.L29:
	movl	$.LC9, %edi
	movl	$51, %edx
	movl	$1, %esi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$8, %edi
	call	exit
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol
	leaq	-2(%rax), %rdx
	cmpq	$32765, %rdx
	ja	.L67
	movq	%rax, 24(%rbp)
	jmp	.L27
.L20:
	cmpb	$101, %dl
	jne	.L27
	cmpb	$0, 2(%rax)
	jne	.L27
	movq	(%r12), %rdi
	movl	$.LC6, %esi
	call	fopen
	movq	%rax, 0(%rbp)
	testq	%rax, %rax
	jne	.L27
	movl	$.LC7, %edi
	call	perror
	movl	$2, %edi
	call	exit
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$.LC1, %edi
	movl	$128, %edx
	movl	$1, %esi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$1, %edi
	call	exit
.L67:
	movl	$.LC5, %edi
	movl	$68, %edx
	movl	$1, %esi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$8, %edi
	call	exit
.L65:
	movl	$.LC2, %edi
	movl	$48, %edx
	movl	$1, %esi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$16, %edi
	call	exit
	.cfi_endproc
.LFE62:
	.size	get_args, .-get_args
	.section	.text.unlikely
.LCOLDE10:
	.text
.LHOTE10:
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"ERROR: in process_args(): could not allocate memory for random matrix. Aborting...\n"
	.section	.rodata.str1.1
.LC12:
	.string	"%d\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"ERROR: in process_args(): could not read matrix size. Aborting...\n"
	.align 8
.LC14:
	.string	"ERROR: in process_args(): could not allocated memory for matrix_t. Aborting...\n"
	.section	.rodata.str1.1
.LC15:
	.string	"%lf"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"ERROR: in process_args(): unexpected EOF in input file. Aborting...\n"
	.section	.text.unlikely
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4,,15
	.globl	process_args
	.type	process_args, @function
process_args:
.LFB63:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$24, %rsp
	.cfi_def_cfa_offset 64
	movq	%fs:40, %rax
	movq	%rax, 8(%rsp)
	xorl	%eax, %eax
	movq	88(%rsp), %rbx
	testq	%rbx, %rbx
	je	.L69
	movl	%ebx, %edi
	call	square_matrix_init
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L85
	movq	(%rax), %rdi
	call	free
	movq	%rbx, %rdi
	call	square_matrix_generate
	movl	%ebx, 16(%r13)
	movq	%rax, 0(%r13)
	movq	%r13, %rax
.L71:
	movq	8(%rsp), %rcx
	xorq	%fs:40, %rcx
	jne	.L86
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	64(%rsp), %rbp
	xorl	%eax, %eax
	movl	$.LC12, %esi
	leaq	4(%rsp), %rdx
	movq	%rbp, %rdi
	call	__isoc99_fscanf
	cmpl	$-1, %eax
	je	.L87
	movl	4(%rsp), %edi
	call	square_matrix_init
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L73
	movl	16(%rax), %edx
	xorl	%ebx, %ebx
	testl	%edx, %edx
	jle	.L84
.L79:
	xorl	%r12d, %r12d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L75:
	movl	16(%r13), %edx
	addl	$1, %r12d
	cmpl	%r12d, %edx
	jle	.L88
.L76:
	imull	%ebx, %edx
	movq	0(%r13), %rax
	movl	$.LC15, %esi
	movq	%rbp, %rdi
	addl	%r12d, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,8), %rdx
	xorl	%eax, %eax
	call	__isoc99_fscanf
	cmpl	$-1, %eax
	jne	.L75
	movl	$68, %edx
	movl	$1, %esi
	movl	$.LC16, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movq	0(%r13), %rdi
	call	free
	movq	%r13, %rdi
	call	free
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L88:
	addl	$1, %ebx
	cmpl	%ebx, %edx
	jg	.L79
.L84:
	movq	%r13, %rax
	jmp	.L71
.L87:
	movl	$66, %edx
	movl	$1, %esi
	movl	$.LC13, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L71
.L85:
	movl	$83, %edx
	movl	$1, %esi
	movl	$.LC11, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L71
.L73:
	movl	$79, %edx
	movl	$1, %esi
	movl	$.LC14, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L71
.L86:
	call	__stack_chk_fail
	.cfi_endproc
.LFE63:
	.size	process_args, .-process_args
	.section	.text.unlikely
.LCOLDE17:
	.text
.LHOTE17:
	.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
	.section	.note.GNU-stack,"",@progbits
