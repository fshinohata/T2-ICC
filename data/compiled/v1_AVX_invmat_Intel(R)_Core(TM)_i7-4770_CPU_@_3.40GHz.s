	.file	"invmat.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ERROR: in invert_matrix(): could not allocate memory for inverse matrix. Aborting...\n"
	.align 8
.LC2:
	.string	"ERROR: in invert_matrix(): could not allocate memory for residue matrix. Aborting...\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4,,15
	.globl	invert_matrix
	.type	invert_matrix, @function
invert_matrix:
.LFB63:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	movq	%rdi, %r15
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$104, %rsp
	.cfi_def_cfa_offset 160
	movq	24(%rdi), %rax
	movq	%rax, 16(%rsp)
	movl	16(%rdi), %eax
	movl	%eax, %edi
	movl	%eax, 36(%rsp)
	call	square_matrix_init
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.L37
	movl	36(%rsp), %ebp
	movslq	%ebp, %rbx
	leaq	0(,%rbx,8), %rdi
	call	malloc
	movq	%rax, 40(%rsp)
	testq	%rax, %rax
	je	.L4
	movq	%rax, %r14
	movl	$0, 32(%rsp)
	movq	$0, 48(%rsp)
	testl	%ebp, %ebp
	jle	.L35
	movl	36(%rsp), %edi
	subq	$1, %rbx
	vxorpd	%xmm2, %xmm2, %xmm2
	movq	%r15, %r13
	movq	%rbx, 56(%rsp)
	vmovapd	%xmm2, %xmm1
	movl	%edi, %eax
	subl	$1, %eax
	movl	%eax, 12(%rsp)
	movq	%rax, 64(%rsp)
	movl	%edi, %eax
	subl	$2, %eax
	movl	%eax, 92(%rsp)
.L24:
	movl	32(%rsp), %ebx
	movl	36(%rsp), %r10d
	vmovsd	.LC3(%rip), %xmm6
	movl	%ebx, %eax
	vmovsd	%xmm6, (%r14)
	addl	$1, %eax
	movl	%eax, 88(%rsp)
	cmpl	%eax, %r10d
	jle	.L23
	movq	40(%rsp), %rsi
	leal	2(%rbx), %r8d
	movl	%eax, %edi
	cltq
	leaq	(%rsi,%rax,8), %rdx
	movl	%ebx, %eax
	movq	48(%rsp), %rbx
	movl	%eax, %r11d
	movl	%ebx, %r9d
	notl	%r9d
	.p2align 4,,10
	.p2align 3
.L10:
	movq	$0, (%rdx)
	cmpl	%edi, %r11d
	jge	.L34
	leal	(%r9,%rdi), %ecx
	movl	%edi, %eax
	vmovapd	%xmm2, %xmm0
	imull	%r8d, %eax
	addq	$1, %rcx
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%esi, %eax
	movq	0(%r13), %rsi
	sarl	%eax
	cltq
	addq	%rbx, %rax
	leaq	(%rsi,%rax,8), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	vmovsd	(%r14,%rax,8), %xmm3
	vfnmadd231sd	(%rsi,%rax,8), %xmm3, %xmm0
	addq	$1, %rax
	vmovsd	%xmm0, (%rdx)
	cmpq	%rcx, %rax
	jne	.L9
.L34:
	movl	%r8d, %esi
	addq	$8, %rdx
	addl	$1, %edi
	addl	$1, %r8d
	cmpl	%esi, %r10d
	jg	.L10
.L6:
	leal	-1(%rsi), %eax
	cmpl	32(%rsp), %eax
	jl	.L11
	movq	40(%rsp), %rdx
	movslq	%eax, %rdi
	movl	%eax, %r9d
	movq	%r13, 80(%rsp)
	movq	24(%rsp), %rcx
	movq	%r14, 72(%rsp)
	leal	-2(%rsi), %r10d
	movl	16(%r13), %eax
	movq	56(%rsp), %r12
	leaq	(%rdx,%rdi,8), %r11
	movq	16(%rsp), %rdx
	movq	(%rcx), %r8
	movl	12(%rsp), %ebp
	movl	%eax, %ebx
	movl	%eax, %r14d
	movq	8(%r13), %r15
	imull	%r9d, %ebx
	subq	%rdi, %r12
	movl	(%rdx), %edx
	imull	16(%rcx), %edx
	movl	32(%rsp), %r13d
	subl	%esi, %ebp
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx), %rcx
	addq	64(%rsp), %rdx
	leaq	(%r8,%rcx,8), %rcx
	leaq	(%r8,%rdx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r9d, %eax
	vmovsd	(%r11), %xmm0
	movl	%ebx, %r8d
	imull	%r10d, %eax
	vmovsd	%xmm0, (%rcx)
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	subl	%eax, %r8d
	cmpl	12(%rsp), %r9d
	jge	.L38
	movl	%ebp, %edx
	movslq	%r8d, %r8
	leaq	(%r8,%r12), %rax
	notq	%rdx
	leaq	(%r15,%rax,8), %rsi
	salq	$3, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L14:
	vmovsd	(%rdi,%rax), %xmm4
	vfnmadd231sd	(%rsi,%rax), %xmm4, %xmm0
	subq	$8, %rax
	vmovsd	%xmm0, (%rcx)
	cmpq	%rdx, %rax
	jne	.L14
	movl	%r10d, %eax
.L13:
	vdivsd	(%r15,%r8,8), %xmm0, %xmm0
	subq	$8, %r11
	vmovsd	%xmm0, (%rcx)
	subl	$1, %r9d
	subq	$8, %rcx
	subl	$1, %r10d
	subl	%r14d, %ebx
	addq	$1, %r12
	addl	$1, %ebp
	cmpl	%eax, %r13d
	jle	.L15
	movq	72(%rsp), %r14
	movq	80(%rsp), %r13
.L11:
	testl	%eax, %eax
	js	.L22
	movq	24(%rsp), %rbx
	movslq	%eax, %rdi
	movl	%eax, %r9d
	movq	16(%rsp), %rcx
	movl	16(%r13), %r12d
	movq	56(%rsp), %r11
	movq	(%rbx), %rsi
	movl	(%rcx), %edx
	imull	16(%rbx), %edx
	movl	92(%rsp), %r10d
	imull	%r12d, %r9d
	subq	%rdi, %r11
	movq	8(%r13), %rbp
	movslq	%edx, %rdx
	movl	12(%rsp), %r15d
	leaq	(%rdi,%rdx), %rcx
	addq	64(%rsp), %rdx
	subl	%eax, %r10d
	leaq	(%rsi,%rcx,8), %rcx
	leaq	(%rsi,%rdx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L21:
	movq	$0, (%rcx)
	leal	-1(%rax), %ebx
	cmpl	%r15d, %eax
	jge	.L39
	imull	%ebx, %eax
	movl	%r9d, %r8d
	vmovapd	%xmm1, %xmm0
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	movl	%r10d, %edx
	sarl	%eax
	notq	%rdx
	subl	%eax, %r8d
	salq	$3, %rdx
	movslq	%r8d, %r8
	leaq	(%r8,%r11), %rax
	leaq	0(%rbp,%rax,8), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L20:
	vmovsd	(%rdi,%rax), %xmm5
	vfnmadd231sd	(%rsi,%rax), %xmm5, %xmm0
	subq	$8, %rax
	vmovsd	%xmm0, (%rcx)
	cmpq	%rax, %rdx
	jne	.L20
.L19:
	vdivsd	0(%rbp,%r8,8), %xmm0, %xmm0
	subq	$8, %rcx
	vmovsd	%xmm0, 8(%rcx)
	movl	%ebx, %eax
	subl	%r12d, %r9d
	addq	$1, %r11
	addl	$1, %r10d
	testl	%ebx, %ebx
	jns	.L21
.L22:
	movl	88(%rsp), %eax
	addq	$8, %r14
	addq	$4, 16(%rsp)
	addq	$1, 48(%rsp)
	movl	%eax, 32(%rsp)
	cmpl	%eax, 36(%rsp)
	jne	.L24
.L35:
	movq	24(%rsp), %rax
	addq	$104, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L39:
	.cfi_restore_state
	imull	%ebx, %eax
	movl	%r9d, %r8d
	vmovapd	%xmm1, %xmm0
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	jmp	.L19
.L38:
	movl	%r10d, %eax
	movslq	%r8d, %r8
	jmp	.L13
.L23:
	movl	%eax, %esi
	jmp	.L6
.L37:
	movl	$85, %edx
	movl	$1, %esi
	movl	$.LC1, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movq	16(%rsp), %rdi
	call	free
	addq	$104, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L4:
	.cfi_restore_state
	movl	$85, %edx
	movl	$1, %esi
	movl	$.LC2, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movq	16(%rsp), %rdi
	call	free
	movq	24(%rsp), %rdi
	call	free
	addq	$104, %rsp
	.cfi_def_cfa_offset 56
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE63:
	.size	invert_matrix, .-invert_matrix
	.section	.text.unlikely
.LCOLDE4:
	.text
.LHOTE4:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC7:
	.string	"# iter %d: <%.17g>\n"
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4,,15
	.globl	refine
	.type	refine, @function
refine:
.LFB64:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$160, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movq	(%r10), %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L131
	movslq	16(%rsi), %rax
	movq	%rdi, -80(%rbp)
	movl	%ecx, %r14d
	movq	$0, (%r8)
	movq	$0, (%r9)
	movq	%r9, -136(%rbp)
	leaq	0(,%rax,8), %r12
	movq	%r8, -128(%rbp)
	movq	%rax, %rbx
	movq	%r12, %rdi
	movl	%ecx, -172(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -120(%rbp)
	call	malloc
	movq	%rax, %r15
	testl	%r14d, %r14d
	jle	.L43
	leal	-1(%rbx), %eax
	vxorpd	%xmm2, %xmm2, %xmm2
	movl	$1, -160(%rbp)
	movl	%eax, %edi
	movl	%eax, -104(%rbp)
	leal	-2(%rbx), %eax
	movl	%eax, %esi
	movl	%eax, -100(%rbp)
	imull	%edi, %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	negl	%eax
	movl	%eax, -156(%rbp)
	movl	%edi, %eax
	leaq	8(%r15,%rax,8), %rax
	movq	%rax, -144(%rbp)
	movslq	%esi, %rax
	leaq	8(%r15,%rax,8), %rax
	movq	%rax, -152(%rbp)
	leaq	-8(%r15,%r12), %rax
	movq	%rax, -168(%rbp)
.L86:
	testl	%ebx, %ebx
	jle	.L135
	xorl	%eax, %eax
	vmovsd	%xmm2, -56(%rbp)
	call	timestamp
	movq	-120(%rbp), %rsi
	vxorpd	%xmm5, %xmm5, %xmm5
	movq	$8, -96(%rbp)
	vcvtsi2sdq	%rax, %xmm5, %xmm5
	movq	-64(%rbp), %rax
	vmovsd	-56(%rbp), %xmm2
	movl	$0, -56(%rbp)
	movq	(%rsi), %r13
	vmovsd	%xmm2, -72(%rbp)
	vmovq	%xmm2, %r12
	movl	16(%rsi), %r9d
	movq	-80(%rbp), %rsi
	movq	24(%rax), %r11
	movq	(%rsi), %r14
	movl	16(%rsi), %esi
	movslq	(%r11), %rax
	movl	%esi, -88(%rbp)
	xorl	%esi, %esi
.L94:
	movl	%esi, %r8d
	movl	-88(%rbp), %edx
	xorl	%edi, %edi
	vmovq	%r12, %xmm1
	imull	%r9d, %r8d
	leaq	(%r15,%rax,8), %rcx
	vmovq	%r12, %xmm0
	imull	-56(%rbp), %edx
	movslq	%r8d, %rax
	leaq	0(%r13,%rax,8), %rax
	movslq	%edx, %rdx
	leaq	(%r14,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L54:
	vmovsd	(%rax,%rdi,8), %xmm7
	vfnmsub231sd	(%rdx,%rdi,8), %xmm7, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	addq	$1, %rdi
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm1
	vmovapd	%xmm3, %xmm0
	cmpl	%edi, %ebx
	jg	.L54
	vaddsd	.LC3(%rip), %xmm3, %xmm0
	vucomisd	%xmm2, %xmm0
	vmovsd	%xmm0, (%rcx)
	jp	.L102
	je	.L55
.L102:
	vucomisd	%xmm0, %xmm2
	ja	.L136
	vmovsd	.LC6(%rip), %xmm6
	vucomisd	%xmm0, %xmm6
	jb	.L59
.L55:
	movq	$0, (%rcx)
.L59:
	leal	1(%rsi), %ecx
	movslq	%ecx, %rdi
	movslq	(%r11,%rdi,4), %rax
	cmpl	%ecx, %ebx
	jle	.L61
	movl	-100(%rbp), %r14d
	addl	%r9d, %r8d
	movslq	%r9d, %rcx
	vmovsd	.LC5(%rip), %xmm6
	leaq	(%r11,%rdi,4), %r9
	movslq	%r8d, %r8
	salq	$3, %rcx
	leaq	0(%r13,%r8,8), %r8
	xorl	%edi, %edi
	subl	%esi, %r14d
	movq	%r14, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	(%r15,%rax,8), %r10
	vmovq	%r12, %xmm1
	xorl	%eax, %eax
	vmovq	%r12, %xmm0
	.p2align 4,,10
	.p2align 3
.L62:
	vmovsd	(%r8,%rax,8), %xmm7
	vfnmsub231sd	(%rdx,%rax,8), %xmm7, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	addq	$1, %rax
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm1
	vmovapd	%xmm3, %xmm0
	cmpl	%eax, %ebx
	jg	.L62
	vaddsd	%xmm2, %xmm3, %xmm0
	vucomisd	%xmm2, %xmm0
	vmovsd	%xmm0, (%r10)
	jp	.L104
	je	.L63
.L104:
	vucomisd	%xmm0, %xmm2
	ja	.L137
	vmovsd	.LC6(%rip), %xmm4
	vucomisd	%xmm0, %xmm4
	jb	.L67
.L63:
	movq	$0, (%r10)
.L67:
	movslq	4(%r9,%rdi,4), %rax
	addq	$1, %rdi
	addq	%rcx, %r8
	cmpq	%rsi, %rdi
	jne	.L69
.L61:
	xorl	%eax, %eax
	vmovsd	%xmm2, -112(%rbp)
	vmovsd	%xmm5, -88(%rbp)
	call	timestamp
	movq	-128(%rbp), %rsi
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovq	%r12, %xmm4
	vcvtsi2sdq	%rax, %xmm0, %xmm0
	vmovsd	-88(%rbp), %xmm5
	movq	-144(%rbp), %rdx
	vmovsd	-112(%rbp), %xmm2
	vsubsd	%xmm5, %xmm0, %xmm5
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	(%rsi), %xmm0, %xmm0
	vaddsd	%xmm0, %xmm5, %xmm0
	vcvttsd2siq	%xmm0, %rax
	vmovq	%r12, %xmm0
	movq	%rax, (%rsi)
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L70:
	vmovsd	(%rax), %xmm1
	addq	$8, %rax
	vfmsub132sd	%xmm1, %xmm4, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm4
	vmovapd	%xmm3, %xmm0
	cmpq	%rdx, %rax
	jne	.L70
	vmovsd	%xmm2, -112(%rbp)
	vaddsd	-72(%rbp), %xmm3, %xmm2
	xorl	%eax, %eax
	vmovsd	%xmm2, -72(%rbp)
	call	timestamp
	vxorpd	%xmm2, %xmm2, %xmm2
	cmpl	$1, %ebx
	vcvtsi2sdq	%rax, %xmm2, %xmm2
	vmovsd	%xmm2, -88(%rbp)
	vmovsd	-112(%rbp), %xmm2
	je	.L71
	movq	-64(%rbp), %rax
	leaq	8(%r15), %rdi
	movl	$2, %esi
	movl	$1, %edx
	movq	(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L72:
	movl	%edx, %eax
	vmovq	%r12, %xmm1
	vmovq	%r12, %xmm0
	imull	%esi, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	sarl	%eax
	cltq
	leaq	(%r8,%rax,8), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L73:
	vmovsd	(%r15,%rax,8), %xmm5
	vfnmsub231sd	(%rcx,%rax,8), %xmm5, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	addq	$1, %rax
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm1
	vmovapd	%xmm3, %xmm0
	cmpl	%eax, %edx
	jne	.L73
	vaddsd	(%rdi), %xmm3, %xmm0
	vmovsd	%xmm0, (%rdi)
	cmpl	%esi, %ebx
	jle	.L71
	addl	$1, %edx
	addl	$1, %esi
	addq	$8, %rdi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L137:
	vucomisd	%xmm6, %xmm0
	jnb	.L63
	jmp	.L67
.L71:
	movq	-64(%rbp), %rax
	movq	-168(%rbp), %rsi
	movq	8(%rax), %r11
	movl	16(%rax), %r14d
	movl	-104(%rbp), %eax
	vmovsd	(%rsi), %xmm0
	imull	%r14d, %eax
	addl	-156(%rbp), %eax
	cltq
	vdivsd	(%r11,%rax,8), %xmm0, %xmm0
	movl	-100(%rbp), %eax
	vmovsd	%xmm0, (%rsi)
	testl	%eax, %eax
	js	.L74
	movq	-152(%rbp), %rdx
	leal	-3(%rbx), %edi
	movl	%eax, %r9d
	xorl	%r10d, %r10d
	imull	%r14d, %r9d
	.p2align 4,,10
	.p2align 3
.L78:
	leal	1(%rdi), %eax
	movl	%edi, %r13d
	imull	%edi, %eax
	leal	2(%rdi), %ecx
	cmpl	%ecx, %ebx
	movl	%eax, %ecx
	jle	.L138
	shrl	$31, %ecx
	movl	%r9d, %r8d
	vmovq	%r12, %xmm1
	addl	%ecx, %eax
	movl	%r10d, %ecx
	vmovq	%r12, %xmm0
	sarl	%eax
	addq	$1, %rcx
	subl	%eax, %r8d
	xorl	%eax, %eax
	movslq	%r8d, %r8
	leaq	(%r11,%r8,8), %rsi
	.p2align 4,,10
	.p2align 3
.L77:
	vmovsd	8(%rsi,%rax,8), %xmm6
	vfnmsub231sd	(%rdx,%rax,8), %xmm6, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	addq	$1, %rax
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm1
	vmovapd	%xmm3, %xmm0
	cmpq	%rax, %rcx
	jne	.L77
.L76:
	vaddsd	-8(%rdx), %xmm3, %xmm3
	addl	$1, %r10d
	vdivsd	(%r11,%r8,8), %xmm3, %xmm3
	subl	$1, %edi
	vmovsd	%xmm3, -8(%rdx)
	subl	%r14d, %r9d
	subq	$8, %rdx
	testl	%r13d, %r13d
	jns	.L78
.L74:
	xorl	%eax, %eax
	vmovsd	%xmm2, -112(%rbp)
	call	timestamp
	movq	-136(%rbp), %rdi
	vxorpd	%xmm0, %xmm0, %xmm0
	vxorpd	%xmm1, %xmm1, %xmm1
	vcvtsi2sdq	%rax, %xmm1, %xmm1
	movl	-56(%rbp), %r13d
	vsubsd	-88(%rbp), %xmm1, %xmm1
	vmovsd	-112(%rbp), %xmm2
	vcvtsi2sdq	(%rdi), %xmm0, %xmm0
	vaddsd	%xmm0, %xmm1, %xmm0
	vcvttsd2siq	%xmm0, %rax
	movq	%rax, (%rdi)
	movq	-80(%rbp), %rax
	imull	16(%rax), %r13d
	movq	(%rax), %rdx
	movslq	%r13d, %r8
	leaq	(%rdx,%r8,8), %rax
	movq	%rax, %rdi
	andl	$31, %edi
	shrq	$3, %rdi
	negq	%rdi
	andl	$3, %edi
	cmpl	%ebx, %edi
	cmova	%ebx, %edi
	cmpl	$4, %ebx
	cmovbe	%ebx, %edi
	testl	%edi, %edi
	je	.L96
	vmovsd	(%rax), %xmm0
	vaddsd	(%r15), %xmm0, %xmm0
	vmovsd	%xmm0, (%rax)
	movl	$1, %eax
	cmpl	$1, %edi
	je	.L91
	leal	1(%r13), %ecx
	movslq	%ecx, %rcx
	salq	$3, %rcx
	leaq	(%rdx,%rcx), %rax
	vmovsd	(%rax), %xmm0
	vaddsd	8(%r15), %xmm0, %xmm0
	vmovsd	%xmm0, (%rax)
	movl	$2, %eax
	cmpl	$2, %edi
	je	.L91
	leaq	8(%rdx,%rcx), %rax
	vmovsd	(%rax), %xmm0
	vaddsd	16(%r15), %xmm0, %xmm0
	vmovsd	%xmm0, (%rax)
	movl	$3, %eax
	cmpl	$4, %edi
	jne	.L91
	leaq	16(%rdx,%rcx), %rax
	vmovsd	(%rax), %xmm0
	vaddsd	24(%r15), %xmm0, %xmm0
	vmovsd	%xmm0, (%rax)
	movl	$4, %eax
.L91:
	cmpl	%ebx, %edi
	je	.L92
.L90:
	movl	%ebx, %r9d
	movl	-104(%rbp), %r14d
	movl	%edi, %ecx
	subl	%edi, %r9d
	leal	-4(%r9), %esi
	shrl	$2, %esi
	addl	$1, %esi
	subl	%edi, %r14d
	leal	0(,%rsi,4), %r10d
	cmpl	$2, %r14d
	jbe	.L93
	leaq	(%r8,%rcx), %rdi
	leaq	(%rdx,%rdi,8), %r8
	xorl	%edi, %edi
	leaq	(%r15,%rcx,8), %r11
	xorl	%ecx, %ecx
.L79:
	vmovupd	(%r11,%rcx), %ymm0
	addl	$1, %edi
	vaddpd	(%r8,%rcx), %ymm0, %ymm0
	vmovapd	%ymm0, (%r8,%rcx)
	addq	$32, %rcx
	cmpl	%esi, %edi
	jb	.L79
	addl	%r10d, %eax
	cmpl	%r9d, %r10d
	je	.L130
	vzeroupper
.L93:
	leal	0(%r13,%rax), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,8), %rsi
	movslq	%eax, %rcx
	vmovsd	(%rsi), %xmm0
	vaddsd	(%r15,%rcx,8), %xmm0, %xmm0
	leal	1(%rax), %ecx
	vmovsd	%xmm0, (%rsi)
	cmpl	%ecx, %ebx
	jle	.L92
	leal	(%rcx,%r13), %esi
	movslq	%ecx, %rcx
	addl	$2, %eax
	movslq	%esi, %rsi
	leaq	(%rdx,%rsi,8), %rsi
	vmovsd	(%rsi), %xmm0
	vaddsd	(%r15,%rcx,8), %xmm0, %xmm0
	vmovsd	%xmm0, (%rsi)
	cmpl	%eax, %ebx
	jle	.L92
	leal	(%rax,%r13), %ecx
	cltq
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,8), %rdx
	vmovsd	(%rdx), %xmm0
	vaddsd	(%r15,%rax,8), %xmm0, %xmm0
	vmovsd	%xmm0, (%rdx)
.L92:
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, %ebx
	je	.L88
.L83:
	xorl	%eax, %eax
	vmovsd	%xmm2, -88(%rbp)
	call	timestamp
	vxorpd	%xmm5, %xmm5, %xmm5
	movl	-56(%rbp), %ecx
	vcvtsi2sdq	%rax, %xmm5, %xmm5
	movq	-64(%rbp), %rax
	movq	-120(%rbp), %rsi
	vmovsd	-88(%rbp), %xmm2
	testl	%ecx, %ecx
	movq	24(%rax), %r11
	movq	(%rsi), %r13
	movslq	(%r11), %rax
	jle	.L44
	movslq	16(%rsi), %r8
	movq	%r13, %rdx
	movq	-80(%rbp), %rsi
	movq	-96(%rbp), %r10
	vmovsd	.LC6(%rip), %xmm6
	vmovsd	.LC5(%rip), %xmm7
	movq	%r8, %r9
	salq	$3, %r8
	movq	(%rsi), %r14
	movl	16(%rsi), %esi
	addq	%r11, %r10
	movl	%esi, %edi
	movl	%esi, -88(%rbp)
	imull	%edi, %ecx
	leaq	4(%r11), %rsi
	movslq	%ecx, %rcx
	leaq	(%r14,%rcx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	(%r15,%rax,8), %rdi
	vmovq	%r12, %xmm1
	xorl	%eax, %eax
	vmovq	%r12, %xmm0
	.p2align 4,,10
	.p2align 3
.L45:
	vmovsd	(%rdx,%rax,8), %xmm4
	vfnmsub231sd	(%rcx,%rax,8), %xmm4, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm3
	addq	$1, %rax
	vsubsd	%xmm0, %xmm3, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm1
	vmovapd	%xmm3, %xmm0
	cmpl	%eax, %ebx
	jg	.L45
	vaddsd	%xmm2, %xmm3, %xmm0
	vucomisd	%xmm2, %xmm0
	vmovsd	%xmm0, (%rdi)
	jp	.L100
	je	.L46
.L100:
	vucomisd	%xmm0, %xmm2
	ja	.L139
	vucomisd	%xmm0, %xmm6
	jb	.L50
.L46:
	movq	$0, (%rdi)
.L50:
	movslq	(%rsi), %rax
	addq	$4, %rsi
	addq	%r8, %rdx
	cmpq	%r10, %rsi
	jne	.L52
	movl	-56(%rbp), %esi
.L53:
	addq	$4, -96(%rbp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L139:
	vucomisd	%xmm7, %xmm0
	jnb	.L46
	jmp	.L50
.L138:
	shrl	$31, %ecx
	movl	%r9d, %r8d
	vmovq	%r12, %xmm3
	addl	%ecx, %eax
	sarl	%eax
	subl	%eax, %r8d
	movslq	%r8d, %r8
	jmp	.L76
.L96:
	xorl	%eax, %eax
	jmp	.L90
.L130:
	vzeroupper
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, %ebx
	jne	.L83
.L88:
	vsqrtsd	-72(%rbp), %xmm0, %xmm0
	vucomisd	%xmm0, %xmm0
	jp	.L140
.L84:
	movl	$.LC7, %edx
	movl	$1, %esi
	movl	$1, %eax
	movl	-160(%rbp), %r14d
	movq	-184(%rbp), %rdi
	vmovsd	%xmm2, -56(%rbp)
	movl	%r14d, %ecx
	call	__fprintf_chk
	movl	%r14d, %eax
	vmovsd	-56(%rbp), %xmm2
	addl	$1, %eax
	cmpl	%eax, -172(%rbp)
	movl	%eax, -160(%rbp)
	jge	.L86
.L43:
	movq	-128(%rbp), %rsi
	movslq	-172(%rbp), %rcx
	movq	(%rsi), %rax
	cqto
	idivq	%rcx
	movq	%rax, (%rsi)
	movq	-136(%rbp), %rsi
	movq	(%rsi), %rax
	cqto
	idivq	%rcx
	movq	%rax, (%rsi)
	movq	-80(%rbp), %rax
.L131:
	addq	$160, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L136:
	.cfi_restore_state
	vucomisd	.LC5(%rip), %xmm0
	jnb	.L55
	jmp	.L59
.L135:
	vmovsd	%xmm2, -72(%rbp)
	vsqrtsd	-72(%rbp), %xmm0, %xmm0
	vucomisd	%xmm0, %xmm0
	jnp	.L84
.L140:
	vmovsd	-72(%rbp), %xmm0
	vmovsd	%xmm2, -56(%rbp)
	call	sqrt
	vmovsd	-56(%rbp), %xmm2
	jmp	.L84
.L44:
	movl	16(%rsi), %r9d
	movq	-80(%rbp), %rsi
	movq	(%rsi), %r14
	movl	16(%rsi), %esi
	movl	%esi, -88(%rbp)
	xorl	%esi, %esi
	jmp	.L53
	.cfi_endproc
.LFE64:
	.size	refine, .-refine
	.section	.text.unlikely
.LCOLDE8:
	.text
.LHOTE8:
	.section	.rodata.str1.1
.LC9:
	.string	"#\n"
.LC11:
	.string	"# Tempo LU: %.3gs\n"
.LC12:
	.string	"# Tempo iter: %.3gs\n"
.LC13:
	.string	"# Tempo Residuo: %.3gs\n#\n"
	.section	.text.unlikely
.LCOLDB14:
	.section	.text.startup,"ax",@progbits
.LHOTB14:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB65:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rsi, %rbp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	%edi, %ebx
	movl	$20172, %edi
	subq	$80, %rsp
	.cfi_def_cfa_offset 112
	movq	%fs:40, %rax
	movq	%rax, 72(%rsp)
	xorl	%eax, %eax
	movq	$0, (%rsp)
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	call	srand
	leaq	32(%rsp), %rdi
	movq	%rbp, %rdx
	movl	%ebx, %esi
	call	get_args
	pushq	56(%rsp)
	.cfi_def_cfa_offset 120
	pushq	56(%rsp)
	.cfi_def_cfa_offset 128
	pushq	56(%rsp)
	.cfi_def_cfa_offset 136
	pushq	56(%rsp)
	.cfi_def_cfa_offset 144
	call	process_args
	addq	$32, %rsp
	.cfi_def_cfa_offset 112
	testq	%rax, %rax
	je	.L144
	movq	%rsp, %rdx
	movq	%rax, %rdi
	movq	%rax, %rbx
	leaq	24(%rsp), %rsi
	call	lu_decomposition
	movl	%eax, %ebp
	testl	%eax, %eax
	je	.L152
.L143:
	movq	72(%rsp), %rcx
	xorq	%fs:40, %rcx
	movl	%ebp, %eax
	jne	.L153
	addq	$80, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L152:
	.cfi_restore_state
	movq	24(%rsp), %rdi
	call	invert_matrix
	movq	40(%rsp), %rcx
	movl	$2, %edx
	movl	$1, %esi
	movl	$.LC9, %edi
	movq	%rax, %r12
	call	fwrite
	movq	%rbx, %rsi
	movq	%r12, %rdi
	pushq	%rax
	.cfi_def_cfa_offset 120
	pushq	48(%rsp)
	.cfi_def_cfa_offset 128
	movl	64(%rsp), %ecx
	leaq	24(%rsp), %r9
	movq	40(%rsp), %rdx
	leaq	32(%rsp), %r8
	call	refine
	movq	56(%rsp), %rdi
	movl	$.LC11, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	16(%rsp), %xmm0, %xmm0
	vdivsd	.LC10(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	movq	56(%rsp), %rdi
	movl	$.LC12, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	24(%rsp), %xmm0, %xmm0
	vdivsd	.LC10(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	movq	56(%rsp), %rdi
	movl	$.LC13, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	32(%rsp), %xmm0, %xmm0
	vdivsd	.LC10(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	popq	%rdx
	.cfi_def_cfa_offset 120
	popq	%rcx
	.cfi_def_cfa_offset 112
	testq	%r12, %r12
	je	.L144
	movq	%r12, %rdi
	call	free_matrix
	movq	%rbx, %rdi
	call	free_matrix
	movq	24(%rsp), %rdi
	call	free_lu_matrix
	jmp	.L143
.L144:
	orl	$-1, %ebp
	jmp	.L143
.L153:
	call	__stack_chk_fail
	.cfi_endproc
.LFE65:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE14:
	.section	.text.startup
.LHOTE14:
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	0
	.long	-1129316352
	.align 8
.LC6:
	.long	0
	.long	1018167296
	.align 8
.LC10:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
