	.file	"timer.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4,,15
	.globl	timestamp
	.type	timestamp, @function
timestamp:
.LFB0:
	.cfi_startproc
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	movl	$2, %edi
	movq	%fs:40, %rax
	movq	%rax, 24(%rsp)
	xorl	%eax, %eax
	movq	%rsp, %rsi
	call	clock_gettime
	vxorpd	%xmm1, %xmm1, %xmm1
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	24(%rsp), %rdx
	xorq	%fs:40, %rdx
	vcvtsi2sdq	(%rsp), %xmm1, %xmm1
	vcvtsi2sdq	8(%rsp), %xmm0, %xmm0
	vdivsd	.LC0(%rip), %xmm0, %xmm0
	vfmadd231sd	.LC1(%rip), %xmm1, %xmm0
	jne	.L6
	vcvttsd2siq	%xmm0, %rax
	addq	$40, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail
	.cfi_endproc
.LFE0:
	.size	timestamp, .-timestamp
	.section	.text.unlikely
.LCOLDE2:
	.text
.LHOTE2:
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1093567616
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
	.section	.note.GNU-stack,"",@progbits
