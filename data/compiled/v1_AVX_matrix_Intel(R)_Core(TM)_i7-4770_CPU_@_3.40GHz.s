	.file	"matrix.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Error in square_matrix_generate(): size parameter must be higher than 1. Returning NULL...\n"
	.align 8
.LC1:
	.string	"ERROR: in square_matrix_generate(): could not allocated memory for matrix elements. Aborting...\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4,,15
	.globl	square_matrix_generate
	.type	square_matrix_generate, @function
square_matrix_generate:
.LFB62:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	cmpq	$1, %rdi
	jle	.L11
	imulq	%rdi, %rdi
	leaq	0(,%rdi,8), %rbp
	movq	%rbp, %rdi
	call	malloc
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L12
	addq	%rax, %rbp
	cmpq	%rbp, %rax
	je	.L6
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L5:
	call	rand
	vxorpd	%xmm0, %xmm0, %xmm0
	addq	$8, %rbx
	vcvtsi2sd	%eax, %xmm0, %xmm0
	vmulsd	.LC2(%rip), %xmm0, %xmm0
	vmovsd	%xmm0, -8(%rbx)
	cmpq	%rbx, %rbp
	jne	.L5
.L6:
	movq	%r12, %rax
.L8:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L11:
	.cfi_restore_state
	movl	$91, %edx
	movl	$1, %esi
	movl	$.LC0, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L8
.L12:
	movl	$96, %edx
	movl	$1, %esi
	movl	$.LC1, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	xorl	%eax, %eax
	jmp	.L8
	.cfi_endproc
.LFE62:
	.size	square_matrix_generate, .-square_matrix_generate
	.section	.text.unlikely
.LCOLDE3:
	.text
.LHOTE3:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%d\n"
.LC5:
	.string	"%.17g "
	.section	.text.unlikely
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4,,15
	.globl	print_matrix
	.type	print_matrix, @function
print_matrix:
.LFB63:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movl	$.LC4, %edx
	movq	%rdi, %r13
	xorl	%eax, %eax
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movq	%rsi, %rbp
	movl	$1, %esi
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movl	16(%rdi), %ecx
	movq	%rbp, %rdi
	call	__fprintf_chk
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L16:
	imull	%ebx, %eax
	movq	0(%r13), %rdx
	movl	$1, %esi
	movq	%rbp, %rdi
	addl	%r12d, %eax
	addl	$1, %r12d
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC5, %edx
	movl	$1, %eax
	call	__fprintf_chk
	movl	16(%r13), %eax
	cmpl	%r12d, %eax
	jg	.L16
	movq	%rbp, %rsi
	movl	$10, %edi
	addl	$1, %ebx
	call	fputc
	movl	16(%r13), %eax
	cmpl	%ebx, %eax
	jg	.L18
.L21:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE63:
	.size	print_matrix, .-print_matrix
	.section	.text.unlikely
.LCOLDE6:
	.text
.LHOTE6:
	.section	.text.unlikely
.LCOLDB7:
	.text
.LHOTB7:
	.p2align 4,,15
	.globl	print_inverse
	.type	print_inverse, @function
print_inverse:
.LFB64:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movl	$.LC4, %edx
	movq	%rdi, %r13
	xorl	%eax, %eax
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movq	%rsi, %rbp
	movl	$1, %esi
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	movl	16(%rdi), %ecx
	movq	%rbp, %rdi
	call	__fprintf_chk
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L27:
	imull	%r12d, %eax
	movq	0(%r13), %rdx
	movl	$1, %esi
	movq	%rbp, %rdi
	addl	$1, %r12d
	addl	%ebx, %eax
	cltq
	vmovsd	(%rdx,%rax,8), %xmm0
	movl	$.LC5, %edx
	movl	$1, %eax
	call	__fprintf_chk
	movl	16(%r13), %eax
	cmpl	%r12d, %eax
	jg	.L27
	movq	%rbp, %rsi
	movl	$10, %edi
	addl	$1, %ebx
	call	fputc
	movl	16(%r13), %eax
	cmpl	%ebx, %eax
	jg	.L29
.L32:
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE64:
	.size	print_inverse, .-print_inverse
	.section	.text.unlikely
.LCOLDE7:
	.text
.LHOTE7:
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4,,15
	.globl	square_matrix_init
	.type	square_matrix_init, @function
square_matrix_init:
.LFB65:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r14
	pushq	%r13
	.cfi_escape 0x10,0xe,0x2,0x76,0x78
	.cfi_escape 0x10,0xd,0x2,0x76,0x70
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x60,0x6
	.cfi_escape 0x10,0xc,0x2,0x76,0x68
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x58
	movl	%edi, %ebx
	movl	$32, %edi
	subq	$8, %rsp
	call	malloc
	testq	%rax, %rax
	je	.L66
	movl	%ebx, %edi
	movl	%ebx, 16(%rax)
	movq	%rax, %r12
	imull	%ebx, %edi
	movslq	%edi, %rdi
	salq	$3, %rdi
	movq	%rdi, 8(%rax)
	call	malloc
	movq	%rax, %r14
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L68
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	malloc
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L38
	testl	%ebx, %ebx
	jle	.L43
	movq	%rax, %rdx
	andl	$31, %edx
	shrq	$2, %rdx
	negq	%rdx
	andl	$7, %edx
	cmpl	%ebx, %edx
	cmova	%ebx, %edx
	cmpl	$10, %ebx
	jg	.L70
	movl	%ebx, %edx
.L40:
	movl	$0, (%rax)
	leaq	4(%rax), %rcx
	cmpl	$1, %edx
	je	.L53
	movl	$1, 4(%rax)
	leaq	8(%rax), %rcx
	cmpl	$2, %edx
	je	.L54
	movl	$2, 8(%rax)
	leaq	12(%rax), %rcx
	cmpl	$3, %edx
	je	.L55
	movl	$3, 12(%rax)
	leaq	16(%rax), %rcx
	cmpl	$4, %edx
	je	.L56
	movl	$4, 16(%rax)
	leaq	20(%rax), %rcx
	cmpl	$5, %edx
	je	.L57
	movl	$5, 20(%rax)
	leaq	24(%rax), %rcx
	cmpl	$6, %edx
	je	.L58
	movl	$6, 24(%rax)
	leaq	28(%rax), %rcx
	cmpl	$7, %edx
	je	.L59
	movl	$7, 28(%rax)
	leaq	32(%rax), %rcx
	cmpl	$8, %edx
	je	.L60
	movl	$8, 32(%rax)
	leaq	36(%rax), %rcx
	cmpl	$10, %edx
	jne	.L61
	movl	$9, 36(%rax)
	leaq	40(%rax), %rcx
	movl	$10, %esi
.L42:
	cmpl	%edx, %ebx
	je	.L43
.L41:
	leal	-1(%rbx), %r8d
	movl	%ebx, %r9d
	movl	%edx, %r11d
	subl	%edx, %r9d
	subl	%edx, %r8d
	leal	-8(%r9), %edi
	shrl	$3, %edi
	addl	$1, %edi
	leal	0(,%rdi,8), %r10d
	cmpl	$6, %r8d
	jbe	.L44
	vmovdqa	.LC9(%rip), %ymm1
	leaq	(%rax,%r11,4), %rdx
	vmovd	%esi, %xmm0
	xorl	%eax, %eax
	vpbroadcastd	%xmm0, %ymm0
	vpaddd	.LC8(%rip), %ymm0, %ymm0
.L45:
	addl	$1, %eax
	vmovdqa	%ymm0, (%rdx)
	addq	$32, %rdx
	vpaddd	%ymm1, %ymm0, %ymm0
	cmpl	%eax, %edi
	ja	.L45
	movl	%r10d, %eax
	addl	%r10d, %esi
	leaq	(%rcx,%rax,4), %rcx
	cmpl	%r10d, %r9d
	je	.L65
	vzeroupper
.L44:
	leal	1(%rsi), %eax
	movl	%esi, (%rcx)
	cmpl	%eax, %ebx
	jle	.L43
	movl	%eax, 4(%rcx)
	leal	2(%rsi), %eax
	cmpl	%eax, %ebx
	jle	.L43
	movl	%eax, 8(%rcx)
	leal	3(%rsi), %eax
	cmpl	%eax, %ebx
	jle	.L43
	movl	%eax, 12(%rcx)
	leal	4(%rsi), %eax
	cmpl	%eax, %ebx
	jle	.L43
	movl	%eax, 16(%rcx)
	leal	5(%rsi), %eax
	cmpl	%eax, %ebx
	jle	.L43
	addl	$6, %esi
	movl	%eax, 20(%rcx)
	cmpl	%esi, %ebx
	jle	.L43
	movl	%esi, 24(%rcx)
.L43:
	movq	%r12, %r13
.L66:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L40
	movq	%rax, %rcx
	xorl	%esi, %esi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L65:
	vzeroupper
	movq	%r12, %r13
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$9, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$1, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$2, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$3, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$4, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$5, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$6, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$7, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$8, %esi
	jmp	.L42
.L38:
	movq	%r14, %rdi
	call	free
.L68:
	movq	%r12, %rdi
	call	free
	jmp	.L66
	.cfi_endproc
.LFE65:
	.size	square_matrix_init, .-square_matrix_init
	.section	.text.unlikely
.LCOLDE10:
	.text
.LHOTE10:
	.section	.text.unlikely
.LCOLDB12:
	.text
.LHOTB12:
	.p2align 4,,15
	.globl	lu_matrix_init
	.type	lu_matrix_init, @function
lu_matrix_init:
.LFB66:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movl	%edi, %ebx
	movl	$32, %edi
	call	malloc
	testq	%rax, %rax
	je	.L114
	movq	%rax, %r13
	movl	%ebx, 16(%rax)
	leal	1(%rbx), %eax
	imull	%ebx, %eax
	movslq	%eax, %rdi
	movq	%rdi, %r14
	salq	$3, %rdi
	call	malloc
	movq	%rax, %r12
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L73
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jle	.L120
	.p2align 4,,10
	.p2align 3
.L104:
	vmovsd	.LC11(%rip), %xmm2
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	imull	%eax, %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%esi, %edx
	sarl	%edx
	addl	%edx, %eax
	cltq
	vmovsd	%xmm2, (%r12,%rax,8)
	movl	%ecx, %eax
	cmpl	%ecx, %ebx
	jne	.L104
	movl	%r14d, %eax
	movslq	%ebx, %rdi
	shrl	$31, %eax
	salq	$2, %rdi
	addl	%r14d, %eax
	sarl	%eax
	cltq
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 8(%r13)
	call	malloc
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.L82
	movq	%rax, %rdx
	andl	$31, %edx
	shrq	$2, %rdx
	negq	%rdx
	andl	$7, %edx
	cmpl	%ebx, %edx
	cmova	%ebx, %edx
	cmpl	$10, %ebx
	cmovbe	%ebx, %edx
	testl	%edx, %edx
	je	.L89
	movl	$0, (%rax)
	cmpl	$1, %edx
	je	.L90
	movl	$1, 4(%rax)
	cmpl	$2, %edx
	je	.L91
	movl	$2, 8(%rax)
	cmpl	$3, %edx
	je	.L92
	movl	$3, 12(%rax)
	cmpl	$4, %edx
	je	.L93
	movl	$4, 16(%rax)
	cmpl	$5, %edx
	je	.L94
	movl	$5, 20(%rax)
	cmpl	$6, %edx
	je	.L95
	movl	$6, 24(%rax)
	cmpl	$7, %edx
	je	.L96
	movl	$7, 28(%rax)
	cmpl	$8, %edx
	je	.L97
	movl	$8, 32(%rax)
	cmpl	$10, %edx
	jne	.L98
	movl	$9, 36(%rax)
	movl	$10, %ecx
.L85:
	cmpl	%edx, %ebx
	je	.L116
.L84:
	leal	-1(%rbx), %edi
	movl	%ebx, %r8d
	movl	%edx, %r10d
	subl	%edx, %r8d
	subl	%edx, %edi
	leal	-8(%r8), %esi
	shrl	$3, %esi
	addl	$1, %esi
	leal	0(,%rsi,8), %r9d
	cmpl	$6, %edi
	jbe	.L87
	vmovdqa	.LC9(%rip), %ymm1
	leaq	(%rax,%r10,4), %rdi
	vmovd	%ecx, %xmm0
	xorl	%edx, %edx
	vpbroadcastd	%xmm0, %ymm0
	vpaddd	.LC8(%rip), %ymm0, %ymm0
.L78:
	addl	$1, %edx
	vmovdqa	%ymm0, (%rdi)
	addq	$32, %rdi
	vpaddd	%ymm1, %ymm0, %ymm0
	cmpl	%edx, %esi
	ja	.L78
	addl	%r9d, %ecx
	cmpl	%r9d, %r8d
	je	.L113
	vzeroupper
.L87:
	movslq	%ecx, %rdx
	movl	%ecx, (%rax,%rdx,4)
	leal	1(%rcx), %edx
	cmpl	%edx, %ebx
	jle	.L116
	movslq	%edx, %rsi
	movl	%edx, (%rax,%rsi,4)
	leal	2(%rcx), %edx
	cmpl	%edx, %ebx
	jle	.L116
	movslq	%edx, %rsi
	movl	%edx, (%rax,%rsi,4)
	leal	3(%rcx), %edx
	cmpl	%edx, %ebx
	jle	.L116
	movslq	%edx, %rsi
	movl	%edx, (%rax,%rsi,4)
	leal	4(%rcx), %edx
	cmpl	%edx, %ebx
	jle	.L116
	movslq	%edx, %rsi
	movl	%edx, (%rax,%rsi,4)
	leal	5(%rcx), %edx
	cmpl	%edx, %ebx
	jle	.L116
	movslq	%edx, %rsi
	addl	$6, %ecx
	movl	%edx, (%rax,%rsi,4)
	cmpl	%ecx, %ebx
	jle	.L116
	movslq	%ecx, %rdx
	movl	%ecx, (%rax,%rdx,4)
.L116:
	movq	%r13, %r15
.L114:
	popq	%rbx
	movq	%r15, %rax
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L113:
	vzeroupper
	jmp	.L116
.L120:
	movl	%r14d, %eax
	movslq	%ebx, %rdi
	shrl	$31, %eax
	salq	$2, %rdi
	addl	%r14d, %eax
	sarl	%eax
	cltq
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 8(%r13)
	call	malloc
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	jne	.L116
.L82:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	free
	movq	%r13, %rdi
	call	free
	jmp	.L114
.L91:
	movl	$2, %ecx
	jmp	.L85
.L92:
	movl	$3, %ecx
	jmp	.L85
.L93:
	movl	$4, %ecx
	jmp	.L85
.L94:
	movl	$5, %ecx
	jmp	.L85
.L95:
	movl	$6, %ecx
	jmp	.L85
.L96:
	movl	$7, %ecx
	jmp	.L85
.L97:
	movl	$8, %ecx
	jmp	.L85
.L98:
	movl	$9, %ecx
	jmp	.L85
.L90:
	movl	$1, %ecx
	jmp	.L85
.L73:
	movq	%r13, %rdi
	call	free
	jmp	.L114
	.cfi_endproc
.LFE66:
	.size	lu_matrix_init, .-lu_matrix_init
	.section	.text.unlikely
.LCOLDE12:
	.text
.LHOTE12:
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"ERROR: in lu_decomposition(): could not allocate memory for matrixes L and U. Aborting...\n"
	.align 8
.LC18:
	.string	"WARNING: lu_decomposition(): Received matrix is not inversible.\n"
	.section	.text.unlikely
.LCOLDB19:
	.text
.LHOTB19:
	.p2align 4,,15
	.globl	lu_decomposition
	.type	lu_decomposition, @function
lu_decomposition:
.LFB61:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$96, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	testq	%rdi, %rdi
	je	.L122
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L122
	cmpq	$0, 24(%rdi)
	je	.L122
	movl	16(%rdi), %eax
	movq	%rdi, %r13
	movl	$32, %edi
	movq	%rsi, -120(%rbp)
	movq	%rdx, -144(%rbp)
	movl	%eax, %ebx
	movl	%eax, -60(%rbp)
	call	malloc
	movq	%rax, %r15
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L123
	movq	%rax, %rdi
	movl	%ebx, 16(%rdi)
	movl	%ebx, %edi
	imull	%ebx, %edi
	movslq	%edi, %rdi
	salq	$3, %rdi
	movq	%rdi, 8(%r15)
	call	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r15)
	testq	%rbx, %rbx
	je	.L251
	movslq	-60(%rbp), %r15
	salq	$2, %r15
	movq	%r15, %rdi
	call	malloc
	movq	-112(%rbp), %rdi
	movq	%rax, %r12
	movq	%rax, 24(%rdi)
	testq	%rax, %rax
	je	.L125
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L138
	movq	%r12, %rax
	movl	-60(%rbp), %edi
	andl	$31, %eax
	shrq	$2, %rax
	negq	%rax
	andl	$7, %eax
	cmpl	%edi, %eax
	cmova	%edi, %eax
	cmpl	$10, %edi
	jg	.L255
	movl	%edi, %eax
.L129:
	movl	$0, (%r12)
	leaq	4(%r12), %rcx
	cmpl	$1, %eax
	je	.L195
	movl	$1, 4(%r12)
	leaq	8(%r12), %rcx
	cmpl	$2, %eax
	je	.L196
	movl	$2, 8(%r12)
	leaq	12(%r12), %rcx
	cmpl	$3, %eax
	je	.L197
	movl	$3, 12(%r12)
	leaq	16(%r12), %rcx
	cmpl	$4, %eax
	je	.L198
	movl	$4, 16(%r12)
	leaq	20(%r12), %rcx
	cmpl	$5, %eax
	je	.L199
	movl	$5, 20(%r12)
	leaq	24(%r12), %rcx
	cmpl	$6, %eax
	je	.L200
	movl	$6, 24(%r12)
	leaq	28(%r12), %rcx
	cmpl	$7, %eax
	je	.L201
	movl	$7, 28(%r12)
	leaq	32(%r12), %rcx
	cmpl	$8, %eax
	je	.L202
	movl	$8, 32(%r12)
	leaq	36(%r12), %rcx
	cmpl	$10, %eax
	jne	.L203
	movl	$9, 36(%r12)
	leaq	40(%r12), %rcx
	movl	$10, %edx
.L131:
	cmpl	%eax, -60(%rbp)
	je	.L138
.L130:
	movl	-60(%rbp), %edi
	movl	%eax, %r8d
	movl	%edi, %r10d
	subl	$1, %edi
	subl	%eax, %r10d
	subl	%eax, %edi
	leal	-8(%r10), %r9d
	shrl	$3, %r9d
	addl	$1, %r9d
	leal	0(,%r9,8), %esi
	cmpl	$6, %edi
	jbe	.L133
	vmovdqa	.LC9(%rip), %ymm1
	leaq	(%r12,%r8,4), %rdi
	vmovd	%edx, %xmm0
	xorl	%eax, %eax
	vpbroadcastd	%xmm0, %ymm0
	vpaddd	.LC8(%rip), %ymm0, %ymm0
.L134:
	addl	$1, %eax
	vmovdqa	%ymm0, (%rdi)
	addq	$32, %rdi
	vpaddd	%ymm1, %ymm0, %ymm0
	cmpl	%eax, %r9d
	ja	.L134
	movl	%esi, %eax
	addl	%esi, %edx
	leaq	(%rcx,%rax,4), %rcx
	cmpl	%r10d, %esi
	je	.L246
	vzeroupper
.L133:
	movl	-60(%rbp), %edi
	leal	1(%rdx), %eax
	movl	%edx, (%rcx)
	cmpl	%edi, %eax
	jge	.L138
	movl	%eax, 4(%rcx)
	leal	2(%rdx), %eax
	cmpl	%edi, %eax
	jge	.L138
	movl	%eax, 8(%rcx)
	leal	3(%rdx), %eax
	cmpl	%edi, %eax
	jge	.L138
	movl	%eax, 12(%rcx)
	leal	4(%rdx), %eax
	cmpl	%eax, %edi
	jle	.L138
	movl	%eax, 16(%rcx)
	leal	5(%rdx), %eax
	cmpl	%edi, %eax
	jge	.L138
	addl	$6, %edx
	movl	%eax, 20(%rcx)
	cmpl	%edi, %edx
	jge	.L138
	movl	%edx, 24(%rcx)
.L138:
	movq	8(%r13), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcpy
	movslq	16(%r13), %rdx
	movq	%r12, %rdi
	movq	24(%r13), %rsi
	call	memcpy
	movl	-60(%rbp), %edi
	call	lu_matrix_init
	movq	-120(%rbp), %rdi
	movq	%rax, (%rdi)
	testq	%rax, %rax
	je	.L256
	movq	24(%rax), %r14
	xorl	%eax, %eax
	leaq	(%r14,%r15), %r11
	movq	%r11, -56(%rbp)
	call	timestamp
	movq	-144(%rbp), %rdi
	movq	%rax, (%rdi)
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L140
	movq	-56(%rbp), %r11
	movl	%eax, %edx
	movl	$1, %r10d
	subl	$1, %eax
	movq	%rbx, %r15
	movl	%eax, -64(%rbp)
	movq	%r14, %r9
	movl	%r10d, %r12d
	vmovsd	.LC14(%rip), %xmm5
	vxorpd	%xmm4, %xmm4, %xmm4
	movl	$0, -132(%rbp)
	vmovapd	%xmm5, %xmm6
	movq	%r11, %r13
.L176:
	leaq	4(%r9), %rax
	movq	%rax, -104(%rbp)
	leal	-1(%r12), %ebx
	cmpq	%rax, %r13
	movq	%rax, %rdi
	movl	%edx, %eax
	jbe	.L257
	imull	-4(%rdi), %eax
	movq	%r9, %r14
	addl	%ebx, %eax
	cltq
	vmovsd	(%r15,%rax,8), %xmm0
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L155:
	vucomisd	%xmm0, %xmm4
	vmovapd	%xmm0, %xmm2
	jbe	.L144
	vxorpd	%xmm5, %xmm2, %xmm2
.L144:
	movl	(%rax), %ecx
	imull	%edx, %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	vmovsd	(%r15,%rcx,8), %xmm3
	vucomisd	%xmm3, %xmm4
	vmovapd	%xmm3, %xmm1
	jbe	.L147
	vxorpd	%xmm6, %xmm1, %xmm1
.L147:
	vucomisd	%xmm2, %xmm1
	vcmpltsd	%xmm1, %xmm2, %xmm1
	cmova	%rax, %r14
	addq	$4, %rax
	vandpd	%xmm1, %xmm3, %xmm3
	vandnpd	%xmm0, %xmm1, %xmm0
	vorpd	%xmm3, %xmm0, %xmm0
	cmpq	%rax, %r13
	ja	.L155
.L142:
	vucomisd	%xmm4, %xmm0
	jp	.L205
	je	.L156
.L205:
	vucomisd	%xmm0, %xmm4
	ja	.L258
	vmovsd	.LC17(%rip), %xmm1
	vucomisd	%xmm0, %xmm1
	jb	.L160
.L156:
	movl	$64, %edx
	movl	$1, %esi
	movl	$.LC18, %edi
	movq	stderr(%rip), %rcx
	movq	%r9, -128(%rbp)
	vmovaps	%xmm6, -96(%rbp)
	vmovaps	%xmm5, -80(%rbp)
	vmovsd	%xmm4, -56(%rbp)
	call	fwrite
	movq	-128(%rbp), %r9
	movl	$32, -132(%rbp)
	vmovapd	-96(%rbp), %xmm6
	vmovapd	-80(%rbp), %xmm5
	vmovsd	-56(%rbp), %xmm4
.L160:
	movl	%r12d, -128(%rbp)
	cmpq	%r9, %r14
	je	.L163
	testl	%ebx, %ebx
	je	.L259
	movl	(%r14), %r11d
	movl	%ebx, %ecx
	xorl	%eax, %eax
	movq	-120(%rbp), %rdi
	imull	%r12d, %ecx
	leal	1(%r11), %edx
	sarl	%ecx
	imull	%r11d, %edx
	movq	(%rdi), %r8
	movslq	%ecx, %rcx
	salq	$3, %rcx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%esi, %edx
	sarl	%edx
	movl	%edx, %r10d
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r8), %rdi
	leal	(%r10,%rax), %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%rcx), %rsi
	movq	(%rdi,%rdx,8), %rdx
	movq	(%rsi), %rdi
#APP
# 112 "src/matrix.c" 1
	XCHG %rdi, %rdx
	
# 0 "" 2
#NO_APP
	addl	$1, %eax
	movq	%rdx, (%rsi)
	addq	$8, %rcx
	cmpl	%ebx, %eax
	jne	.L166
.L165:
	movq	-104(%rbp), %rax
	movl	-4(%rax), %eax
#APP
# 115 "src/matrix.c" 1
	XCHG %eax, %r11d
	
# 0 "" 2
#NO_APP
	movq	-112(%rbp), %rax
	movl	%r11d, (%r9)
	movq	(%rax), %r15
.L163:
	cmpq	-104(%rbp), %r13
	jbe	.L174
	movq	-120(%rbp), %rax
	movslq	%r12d, %r11
	movq	-104(%rbp), %rdi
	movq	(%rax), %r8
	movq	-112(%rbp), %rax
	movq	(%r8), %r10
	movl	16(%rax), %eax
	movl	%eax, -80(%rbp)
	imull	-4(%rdi), %eax
	leal	(%rbx,%rax), %edx
	cltq
	movslq	%edx, %rdx
	movq	%rax, -96(%rbp)
	leaq	(%r15,%rdx,8), %r9
	movl	-64(%rbp), %edx
	subl	%r12d, %edx
	leaq	1(%r11,%rdx), %r14
	.p2align 4,,10
	.p2align 3
.L170:
	movl	(%rdi), %ecx
	leal	1(%rcx), %eax
	imull	%ecx, %eax
	imull	-80(%rbp), %ecx
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	addl	%ebx, %eax
	cltq
	leaq	(%r10,%rax,8), %rdx
	leal	(%rcx,%rbx), %eax
	cltq
	vmovsd	(%r15,%rax,8), %xmm0
	vdivsd	(%r9), %xmm0, %xmm0
	vmovsd	%xmm0, (%rdx)
	cmpl	%r12d, -60(%rbp)
	jle	.L171
	movslq	%ecx, %rcx
	leaq	(%r11,%rcx), %rax
	leaq	(%r15,%rax,8), %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	leaq	(%r14,%rcx), %rsi
	leaq	(%r15,%rsi,8), %rsi
	subq	%rcx, %rax
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L260:
	vmovsd	(%rdx), %xmm0
.L172:
	vmovsd	(%rax), %xmm7
	vfnmadd132sd	(%rax,%rcx,8), %xmm7, %xmm0
	addq	$8, %rax
	vmovsd	%xmm0, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L260
	addq	$4, %rdi
	cmpq	%rdi, %r13
	ja	.L170
.L174:
	addl	$1, %r12d
	movl	-128(%rbp), %ebx
	cmpl	%ebx, -60(%rbp)
	jle	.L261
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r9
	movl	16(%rax), %edx
	jmp	.L176
.L122:
	movl	16, %eax
	ud2
.L171:
	addq	$4, %rdi
	cmpq	%rdi, %r13
	ja	.L170
	movq	%r8, %r12
.L169:
	movq	24(%r12), %rax
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	-60(%rbp), %ebx
	movq	%rax, -80(%rbp)
	jmp	.L185
.L264:
	leal	(%r14,%rcx), %r8d
	leal	1(%r14), %r10d
	movslq	%r8d, %r8
	vmovsd	(%r15,%r8,8), %xmm0
	vmovsd	%xmm0, (%rdx)
	movl	%r10d, %edx
	cmpl	$1, %eax
	je	.L179
	leal	(%rcx,%r10), %edx
	movslq	%edx, %rdx
	vmovsd	(%r15,%rdx,8), %xmm0
	leal	1(%rsi), %edx
	movslq	%edx, %rdx
	vmovsd	%xmm0, (%rdi,%rdx,8)
	leal	2(%r14), %edx
	cmpl	$3, %eax
	jne	.L179
	addl	%ecx, %edx
	movslq	%edx, %rdx
	vmovsd	(%r15,%rdx,8), %xmm0
	leal	2(%rsi), %edx
	movslq	%edx, %rdx
	vmovsd	%xmm0, (%rdi,%rdx,8)
	leal	3(%r14), %edx
.L179:
	movl	%ebx, %r8d
	movl	%edx, -120(%rbp)
	subl	%eax, %r8d
	salq	$3, %rax
	addq	%rax, %r11
	addq	-56(%rbp), %rax
	movl	%r8d, -104(%rbp)
	subl	$4, %r8d
	shrl	$2, %r8d
	addq	%r15, %r11
	addl	$1, %r8d
	leal	0(,%r8,4), %r9d
	addq	%rdi, %rax
	movl	%r9d, -96(%rbp)
	xorl	%r9d, %r9d
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	xorl	%eax, %eax
.L181:
	vmovapd	(%r11,%r9), %ymm0
	addl	$1, %eax
	vmovupd	%ymm0, (%rdx,%r9)
	addq	$32, %r9
	cmpl	%eax, %r8d
	ja	.L181
	movl	-120(%rbp), %edx
	movl	-96(%rbp), %eax
	addl	%eax, %edx
	cmpl	-104(%rbp), %eax
	je	.L249
	leal	(%rdx,%rcx), %eax
	cltq
	vmovsd	(%r15,%rax,8), %xmm0
	movl	%edx, %eax
	subl	%r14d, %eax
	addl	%esi, %eax
	cltq
	vmovsd	%xmm0, (%rdi,%rax,8)
	leal	1(%rdx), %eax
	cmpl	%eax, -60(%rbp)
	jle	.L249
	addl	$2, %edx
	leal	(%rax,%rcx), %r8d
	subl	%r14d, %eax
	movslq	%r8d, %r8
	addl	%esi, %eax
	vmovsd	(%r15,%r8,8), %xmm0
	cltq
	vmovsd	%xmm0, (%rdi,%rax,8)
	cmpl	-60(%rbp), %edx
	jge	.L249
	subl	$1, %ebx
	leal	(%rdx,%rcx), %eax
	subl	%r14d, %edx
	cltq
	vmovsd	(%r15,%rax,8), %xmm0
	leal	(%rdx,%rsi), %eax
	cltq
	vmovsd	%xmm0, (%rdi,%rax,8)
.L188:
	movl	%r10d, %r14d
	addq	$1, %r13
	cmpl	-60(%rbp), %r10d
	je	.L262
.L185:
	cmpl	%r14d, -60(%rbp)
	jle	.L263
	movl	16(%r12), %esi
	leal	-1(%r14), %eax
	movq	8(%r12), %rdi
	imull	%r14d, %eax
	imull	%r14d, %esi
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	subl	%eax, %esi
	movq	-112(%rbp), %rax
	movslq	%esi, %r8
	movl	16(%rax), %ecx
	movq	-80(%rbp), %rax
	imull	(%rax,%r13,4), %ecx
	leaq	0(,%r8,8), %rax
	leaq	(%rdi,%rax), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rdi,%r8,8), %r8
	movslq	%ecx, %r10
	leaq	(%r10,%r13), %r11
	leaq	4(%r10,%r13), %r10
	salq	$3, %r11
	leaq	(%r15,%r10,8), %r10
	leaq	(%r15,%r11), %rax
	cmpq	%r10, %rdx
	setnb	%r10b
	cmpq	%r8, %rax
	setnb	%r8b
	orb	%r8b, %r10b
	je	.L191
	cmpl	$12, %ebx
	jbe	.L191
	andl	$31, %eax
	shrq	$3, %rax
	negq	%rax
	andl	$3, %eax
	cmpl	%ebx, %eax
	cmova	%rbx, %rax
	testl	%eax, %eax
	jne	.L264
	leal	1(%r14), %r10d
	movl	%r14d, %edx
	jmp	.L179
.L249:
	subl	$1, %ebx
	movl	%r10d, %r14d
	addq	$1, %r13
	cmpl	-60(%rbp), %r10d
	jne	.L185
.L262:
	movq	-112(%rbp), %rax
	movq	24(%rax), %r12
	xorl	%eax, %eax
	vzeroupper
	call	timestamp
	movq	-144(%rbp), %rbx
	subq	(%rbx), %rax
	movq	%rax, (%rbx)
	testq	%r15, %r15
	je	.L187
.L186:
	movq	%r15, %rdi
	call	free
.L187:
	testq	%r12, %r12
	je	.L192
	movq	%r12, %rdi
	call	free
.L192:
	movq	-112(%rbp), %rdi
	call	free
	movl	-132(%rbp), %eax
.L247:
	addq	$96, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L258:
	.cfi_restore_state
	vucomisd	.LC16(%rip), %xmm0
	jnb	.L156
	jmp	.L160
.L191:
	leal	-1(%rbx), %esi
	xorl	%ecx, %ecx
	movq	%rsi, %rbx
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L177:
	vmovsd	(%rax,%rcx,8), %xmm0
	vmovsd	%xmm0, (%rdx,%rcx,8)
	addq	$1, %rcx
	cmpq	%rsi, %rcx
	jne	.L177
	leal	1(%r14), %r10d
	jmp	.L188
.L263:
	leal	1(%r14), %r10d
	subl	$1, %ebx
	jmp	.L188
.L257:
	imull	-4(%rdi), %eax
	movq	%r9, %r14
	addl	%ebx, %eax
	cltq
	vmovsd	(%r15,%rax,8), %xmm0
	jmp	.L142
.L125:
	movq	%rbx, %rdi
	call	free
.L251:
	movq	-112(%rbp), %rdi
	call	free
.L123:
	movq	0, %rax
	ud2
.L259:
	movl	(%r14), %r11d
	jmp	.L165
.L255:
	movq	%r12, %rcx
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.L130
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-120(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L169
.L140:
	xorl	%eax, %eax
	movq	%rbx, %r15
	call	timestamp
	movq	-144(%rbp), %rdi
	movl	$0, -132(%rbp)
	subq	(%rdi), %rax
	movq	%rax, (%rdi)
	jmp	.L186
.L246:
	vzeroupper
	jmp	.L138
.L195:
	movl	$1, %edx
	jmp	.L131
.L256:
	movl	$90, %edx
	movl	$1, %esi
	movl	$.LC13, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$-1, %eax
	jmp	.L247
.L203:
	movl	$9, %edx
	jmp	.L131
.L202:
	movl	$8, %edx
	jmp	.L131
.L201:
	movl	$7, %edx
	jmp	.L131
.L200:
	movl	$6, %edx
	jmp	.L131
.L199:
	movl	$5, %edx
	jmp	.L131
.L198:
	movl	$4, %edx
	jmp	.L131
.L197:
	movl	$3, %edx
	jmp	.L131
.L196:
	movl	$2, %edx
	jmp	.L131
	.cfi_endproc
.LFE61:
	.size	lu_decomposition, .-lu_decomposition
	.section	.text.unlikely
.LCOLDE19:
	.text
.LHOTE19:
	.section	.text.unlikely
.LCOLDB20:
	.text
.LHOTB20:
	.p2align 4,,15
	.globl	free_matrix
	.type	free_matrix, @function
free_matrix:
.LFB67:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L277
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	free
.L267:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L268
	call	free
.L268:
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 8
	jmp	free
	.p2align 4,,10
	.p2align 3
.L277:
	ret
	.cfi_endproc
.LFE67:
	.size	free_matrix, .-free_matrix
	.section	.text.unlikely
.LCOLDE20:
	.text
.LHOTE20:
	.section	.text.unlikely
.LCOLDB21:
	.text
.LHOTB21:
	.p2align 4,,15
	.globl	free_lu_matrix
	.type	free_lu_matrix, @function
free_lu_matrix:
.LFB68:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L286
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	free
.L280:
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_restore 3
	.cfi_def_cfa_offset 8
	jmp	free
	.p2align 4,,10
	.p2align 3
.L286:
	ret
	.cfi_endproc
.LFE68:
	.size	free_lu_matrix, .-free_lu_matrix
	.section	.text.unlikely
.LCOLDE21:
	.text
.LHOTE21:
	.section	.text.unlikely
.LCOLDB22:
	.text
.LHOTB22:
	.p2align 4,,15
	.globl	matrix_copy
	.type	matrix_copy, @function
matrix_copy:
.LFB69:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L303
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L305
	cmpq	$0, 24(%rdi)
	je	.L305
	movl	16(%rdi), %r15d
	movq	%rdi, %rbx
	movl	$32, %edi
	call	malloc
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L289
	movl	%r15d, %edi
	movl	%r15d, 16(%rax)
	imull	%r15d, %edi
	movslq	%edi, %rdi
	salq	$3, %rdi
	movq	%rdi, 8(%rax)
	call	malloc
	movq	%rax, %r14
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L326
	movslq	%r15d, %rdi
	salq	$2, %rdi
	call	malloc
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.L291
	testl	%r15d, %r15d
	jle	.L302
	movq	%rax, %rdx
	andl	$31, %edx
	shrq	$2, %rdx
	negq	%rdx
	andl	$7, %edx
	cmpl	%r15d, %edx
	cmova	%r15d, %edx
	cmpl	$10, %r15d
	jg	.L328
	movl	%r15d, %edx
.L293:
	movl	$0, (%rax)
	leaq	4(%rax), %rcx
	cmpl	$1, %edx
	je	.L308
	movl	$1, 4(%rax)
	leaq	8(%rax), %rcx
	cmpl	$2, %edx
	je	.L309
	movl	$2, 8(%rax)
	leaq	12(%rax), %rcx
	cmpl	$3, %edx
	je	.L310
	movl	$3, 12(%rax)
	leaq	16(%rax), %rcx
	cmpl	$4, %edx
	je	.L311
	movl	$4, 16(%rax)
	leaq	20(%rax), %rcx
	cmpl	$5, %edx
	je	.L312
	movl	$5, 20(%rax)
	leaq	24(%rax), %rcx
	cmpl	$6, %edx
	je	.L313
	movl	$6, 24(%rax)
	leaq	28(%rax), %rcx
	cmpl	$7, %edx
	je	.L314
	movl	$7, 28(%rax)
	leaq	32(%rax), %rcx
	cmpl	$8, %edx
	je	.L315
	movl	$8, 32(%rax)
	leaq	36(%rax), %rcx
	cmpl	$10, %edx
	jne	.L316
	movl	$9, 36(%rax)
	leaq	40(%rax), %rcx
	movl	$10, %esi
.L295:
	cmpl	%edx, %r15d
	je	.L302
.L294:
	leal	-1(%r15), %r11d
	movl	%r15d, %r9d
	movl	%edx, %r10d
	subl	%edx, %r9d
	subl	%edx, %r11d
	leal	-8(%r9), %edi
	shrl	$3, %edi
	addl	$1, %edi
	leal	0(,%rdi,8), %r8d
	cmpl	$6, %r11d
	jbe	.L297
	vmovdqa	.LC9(%rip), %ymm1
	leaq	(%rax,%r10,4), %rdx
	vmovd	%esi, %xmm0
	xorl	%eax, %eax
	vpbroadcastd	%xmm0, %ymm0
	vpaddd	.LC8(%rip), %ymm0, %ymm0
.L298:
	addl	$1, %eax
	vmovdqa	%ymm0, (%rdx)
	addq	$32, %rdx
	vpaddd	%ymm1, %ymm0, %ymm0
	cmpl	%eax, %edi
	ja	.L298
	movl	%r8d, %eax
	addl	%r8d, %esi
	leaq	(%rcx,%rax,4), %rcx
	cmpl	%r8d, %r9d
	je	.L323
	vzeroupper
.L297:
	leal	1(%rsi), %eax
	movl	%esi, (%rcx)
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 4(%rcx)
	leal	2(%rsi), %eax
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 8(%rcx)
	leal	3(%rsi), %eax
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 12(%rcx)
	leal	4(%rsi), %eax
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 16(%rcx)
	leal	5(%rsi), %eax
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 20(%rcx)
	leal	6(%rsi), %eax
	cmpl	%eax, %r15d
	jle	.L302
	movl	%eax, 24(%rcx)
.L302:
	movq	8(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcpy
	movq	24(%r13), %rdi
	movslq	16(%rbx), %rdx
	movq	24(%rbx), %rsi
	call	memcpy
	movq	%r13, %rax
.L324:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r10
	.cfi_restore 10
	.cfi_def_cfa 10, 0
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L291:
	.cfi_restore_state
	movq	%r14, %rdi
	call	free
.L326:
	movq	%r13, %rdi
	call	free
	.p2align 4,,10
	.p2align 3
.L289:
	movq	0, %rax
	ud2
	.p2align 4,,10
	.p2align 3
.L328:
	testl	%edx, %edx
	jne	.L293
	movq	%rax, %rcx
	xorl	%esi, %esi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L323:
	vzeroupper
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L305:
	xorl	%eax, %eax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$8, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$1, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$2, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$3, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L311:
	movl	$4, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$9, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$5, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$6, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L314:
	movl	$7, %esi
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE69:
	.size	matrix_copy, .-matrix_copy
	.section	.text.unlikely
.LCOLDE22:
	.text
.LHOTE22:
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	2097152
	.long	1040187392
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC8:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.align 32
.LC9:
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC14:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC16:
	.long	0
	.long	-1129316352
	.align 8
.LC17:
	.long	0
	.long	1018167296
	.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
	.section	.note.GNU-stack,"",@progbits
