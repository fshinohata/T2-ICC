	.file	"invmat.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ERROR: in invert_matrix(): could not allocate memory for solution vector.\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4,,15
	.globl	invert_matrix
	.type	invert_matrix, @function
invert_matrix:
.LFB4655:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$64, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movq	40(%rdi), %rax
	movslq	32(%rdi), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdi, %r13
	salq	$3, %rdi
	call	malloc
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L2
	testl	%r13d, %r13d
	jle	.L25
	movq	%rax, -72(%rbp)
	movl	36(%r15), %eax
	vxorpd	%xmm4, %xmm4, %xmm4
	xorl	%r14d, %r14d
	movq	$0, -88(%rbp)
	vmovapd	%xmm4, %xmm5
	movl	%eax, -52(%rbp)
	movq	24(%r15), %rax
	movq	%rax, -104(%rbp)
.L24:
	movq	-72(%rbp), %rbx
	leal	1(%r14), %eax
	vmovsd	.LC2(%rip), %xmm6
	movl	%eax, -56(%rbp)
	movl	%eax, %r9d
	vmovsd	%xmm6, (%rbx)
	cmpl	%r13d, %eax
	jge	.L6
	movq	-64(%rbp), %rbx
	leal	4(%r14), %r10d
	cltq
	movl	$-4, %edi
	movq	8(%r15), %r8
	leaq	(%rbx,%rax,8), %rsi
	movq	-88(%rbp), %rax
	leaq	0(,%rax,8), %rbx
	leaq	4(%rax), %r11
	.p2align 4,,10
	.p2align 3
.L11:
	movl	-52(%rbp), %eax
	movq	$0, (%rsi)
	imull	%r9d, %eax
	cltq
	leaq	(%r8,%rax,8), %r12
	leal	-4(%r9), %eax
	cmpl	%eax, %r14d
	jge	.L26
	leaq	(%r12,%rbx), %rax
	movl	%edi, %r13d
	vxorpd	%xmm1, %xmm1, %xmm1
	shrl	$2, %r13d
	movl	%r13d, %edx
	leaq	(%r11,%rdx,4), %rdx
	leaq	(%r12,%rdx,8), %rcx
	movq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L8:
#APP
# 84 "v2/src/invmat.c" 1
	# [START] (invert_matrix) AVX CHECK 1
# 0 "" 2
#NO_APP
	vmovupd	(%rax), %ymm6
	vfmadd231pd	(%rdx), %ymm6, %ymm1
#APP
# 86 "v2/src/invmat.c" 1
	# [END] (invert_matrix) AVX CHECK 1
# 0 "" 2
#NO_APP
	addq	$32, %rax
	addq	$32, %rdx
	cmpq	%rcx, %rax
	jne	.L8
	leal	(%r10,%r13,4), %eax
	movl	32(%r15), %r13d
	vmovsd	(%rsi), %xmm0
.L7:
	cmpl	%r9d, %eax
	jge	.L9
	movq	-64(%rbp), %rcx
	leal	-1(%r9), %edx
	subl	%eax, %edx
	cltq
	salq	$3, %rax
	addq	$1, %rdx
	addq	%rax, %r12
	addq	%rax, %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L10:
	vmovsd	(%rcx,%rax,8), %xmm7
	vfnmadd231sd	(%r12,%rax,8), %xmm7, %xmm0
	addq	$1, %rax
	vmovsd	%xmm0, (%rsi)
	cmpq	%rax, %rdx
	jne	.L10
.L9:
	vmovapd	%xmm1, %xmm2
	addl	$1, %r9d
	addq	$8, %rsi
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm1, %xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	addl	$1, %edi
	vsubsd	%xmm3, %xmm0, %xmm0
	vmovapd	%xmm1, %xmm3
	vunpckhpd	%xmm1, %xmm1, %xmm1
	vsubsd	%xmm2, %xmm0, %xmm2
	vsubsd	%xmm3, %xmm2, %xmm2
	vsubsd	%xmm1, %xmm2, %xmm2
	vmovsd	%xmm2, -8(%rsi)
	cmpl	%r13d, %r9d
	jl	.L11
.L6:
	movq	-96(%rbp), %rbx
	subl	$1, %r9d
	movq	-88(%rbp), %rdi
	movl	-52(%rbp), %eax
	imull	(%rbx,%rdi,4), %eax
	movq	-104(%rbp), %rbx
	cltq
	leaq	(%rbx,%rax,8), %rdi
	cmpl	%r14d, %r9d
	jl	.L12
	movq	16(%r15), %rax
	leal	-4(%r13), %edx
	movslq	%r9d, %r8
	salq	$3, %r8
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L17:
	movl	-52(%rbp), %eax
	imull	%r9d, %eax
	cltq
	leaq	(%r12,%rax,8), %rbx
	movq	-64(%rbp), %rax
	vmovsd	(%rax,%r8), %xmm0
	leal	1(%r9), %eax
	vmovsd	%xmm0, (%rdi,%r8)
	cmpl	%eax, %edx
	jle	.L27
	movl	32(%r15), %r10d
	movslq	%eax, %rcx
	vxorpd	%xmm1, %xmm1, %xmm1
	salq	$3, %rcx
	leaq	(%rbx,%rcx), %rsi
	addq	%rdi, %rcx
	leal	-4(%r10), %r11d
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 126 "v2/src/invmat.c" 1
	# [START] (invert_matrix) AVX CHECK 2
# 0 "" 2
#NO_APP
	vmovupd	(%rcx), %ymm6
	vfmadd231pd	(%rsi), %ymm6, %ymm1
#APP
# 128 "v2/src/invmat.c" 1
	# [END] (invert_matrix) AVX CHECK 2
# 0 "" 2
#NO_APP
	addl	$4, %eax
	movl	%r10d, %r13d
	addq	$32, %rsi
	addq	$32, %rcx
	movl	%r11d, %edx
	cmpl	%eax, %r11d
	jg	.L14
	vmovsd	(%rdi,%r8), %xmm0
.L13:
	cmpl	%r13d, %eax
	jge	.L15
	leal	-1(%r13), %esi
	xorl	%ecx, %ecx
	subl	%eax, %esi
	cltq
	salq	$3, %rax
	addq	$1, %rsi
	leaq	(%rdi,%rax), %r10
	addq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	vmovsd	(%r10,%rcx,8), %xmm7
	vfnmadd231sd	(%rax,%rcx,8), %xmm7, %xmm0
	addq	$1, %rcx
	vmovsd	%xmm0, (%rdi,%r8)
	cmpq	%rcx, %rsi
	jne	.L16
.L15:
	vmovapd	%xmm1, %xmm2
	subl	$1, %r9d
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm1, %xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	vsubsd	%xmm3, %xmm0, %xmm3
	vsubsd	%xmm2, %xmm3, %xmm2
	vmovapd	%xmm1, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm2
	vunpckhpd	%xmm1, %xmm1, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm3
	vmovsd	%xmm3, (%rdi,%r8)
	vdivsd	(%rbx,%r8), %xmm3, %xmm3
	vmovsd	%xmm3, (%rdi,%r8)
	subq	$8, %r8
	cmpl	%r14d, %r9d
	jge	.L17
.L12:
	testl	%r9d, %r9d
	js	.L18
	leal	1(%r9), %ebx
	movslq	%r9d, %rax
	leaq	(%rdi,%rax,8), %r8
	movq	16(%r15), %rax
	leal	-4(%r13), %edx
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L23:
	movl	36(%r15), %eax
	movq	%r8, %r14
	movq	$0, (%r8)
	movq	-80(%rbp), %rsi
	subq	%rdi, %r14
	imull	%r9d, %eax
	cltq
	leaq	(%rsi,%rax,8), %r12
	movl	%ebx, %eax
	cmpl	%edx, %ebx
	jge	.L28
	movl	32(%r15), %r10d
	movslq	%ebx, %rcx
	vxorpd	%xmm1, %xmm1, %xmm1
	salq	$3, %rcx
	leaq	(%r12,%rcx), %rsi
	addq	%rdi, %rcx
	leal	-4(%r10), %r11d
	.p2align 4,,10
	.p2align 3
.L20:
#APP
# 150 "v2/src/invmat.c" 1
	# [START] (invert_matrix) AVX CHECK 3
# 0 "" 2
#NO_APP
	vmovupd	(%rsi), %ymm2
	vfmadd231pd	(%rcx), %ymm2, %ymm1
#APP
# 152 "v2/src/invmat.c" 1
	# [END] (invert_matrix) AVX CHECK 3
# 0 "" 2
#NO_APP
	addl	$4, %eax
	movl	%r10d, %r13d
	addq	$32, %rsi
	addq	$32, %rcx
	movl	%r11d, %edx
	cmpl	%eax, %r11d
	jg	.L20
	vmovsd	(%r8), %xmm0
.L19:
	cmpl	%r13d, %eax
	jge	.L21
	leal	-1(%r13), %esi
	xorl	%ecx, %ecx
	subl	%eax, %esi
	cltq
	salq	$3, %rax
	addq	$1, %rsi
	leaq	(%rdi,%rax), %r10
	addq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L22:
	vmovsd	(%r10,%rcx,8), %xmm3
	vfnmadd231sd	(%rax,%rcx,8), %xmm3, %xmm0
	addq	$1, %rcx
	vmovsd	%xmm0, (%r8)
	cmpq	%rcx, %rsi
	jne	.L22
.L21:
	vmovapd	%xmm1, %xmm2
	subl	$1, %r9d
	subq	$8, %r8
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm1, %xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	subl	$1, %ebx
	vsubsd	%xmm3, %xmm0, %xmm3
	vsubsd	%xmm2, %xmm3, %xmm2
	vmovapd	%xmm1, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm2
	vunpckhpd	%xmm1, %xmm1, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm3
	vmovsd	%xmm3, 8(%r8)
	vdivsd	(%r12,%r14), %xmm3, %xmm3
	vmovsd	%xmm3, 8(%r8)
	cmpl	$-1, %r9d
	jne	.L23
.L18:
	movl	-56(%rbp), %r14d
	addq	$1, -88(%rbp)
	addq	$8, -72(%rbp)
	cmpl	%r13d, %r14d
	jl	.L24
	vzeroupper
.L25:
	xorl	%eax, %eax
.L38:
	addq	$64, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	vmovapd	%xmm5, %xmm0
	vxorpd	%xmm1, %xmm1, %xmm1
	jmp	.L19
.L26:
	movl	%r14d, %eax
	vmovapd	%xmm4, %xmm0
	vxorpd	%xmm1, %xmm1, %xmm1
	jmp	.L7
.L27:
	vxorpd	%xmm1, %xmm1, %xmm1
	jmp	.L13
.L2:
	movl	$74, %edx
	movl	$1, %esi
	movl	$.LC1, %edi
	movq	stderr(%rip), %rcx
	call	fwrite
	movl	$-1, %eax
	jmp	.L38
	.cfi_endproc
.LFE4655:
	.size	invert_matrix, .-invert_matrix
	.section	.text.unlikely
.LCOLDE3:
	.text
.LHOTE3:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"# iter %d: <%.17g>\n"
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4,,15
	.globl	refine
	.type	refine, @function
refine:
.LFB4656:
	.cfi_startproc
	testl	%esi, %esi
	je	.L119
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$352, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movslq	32(%rdi), %rbx
	movq	%rdi, -216(%rbp)
	movq	$0, (%rdx)
	movq	$0, (%rcx)
	movq	%r8, -392(%rbp)
	movl	%ebx, -64(%rbp)
	salq	$3, %rbx
	movq	%rbx, %rdi
	movq	%rcx, -296(%rbp)
	movq	%rdx, -288(%rbp)
	movl	%esi, -384(%rbp)
	call	malloc
	movq	%rax, %r15
	testl	%r14d, %r14d
	jle	.L44
	movl	$1, -380(%rbp)
	movl	-64(%rbp), %eax
	leal	-2(%rax), %esi
	movl	%esi, %edi
	movl	%esi, -312(%rbp)
	leal	-4(%rax), %esi
	movl	%esi, -60(%rbp)
	movl	%eax, %esi
	subl	$1, %eax
	movl	%eax, -192(%rbp)
	leaq	4(,%rax,4), %rax
	movq	%rax, -304(%rbp)
	movl	%esi, %eax
	subl	$5, %eax
	shrl	$2, %eax
	movl	%eax, %edx
	leal	4(,%rax,4), %eax
	addq	$1, %rdx
	movq	%rdx, %rcx
	movl	%eax, -228(%rbp)
	salq	$5, %rdx
	salq	$4, %rcx
	movq	%rdx, -264(%rbp)
	movq	%rcx, -240(%rbp)
	movl	%eax, %ecx
	movl	%esi, %eax
	subl	$10, %eax
	andl	$-4, %eax
	addl	$9, %eax
	movl	%eax, -344(%rbp)
	movslq	%edi, %rax
	leaq	0(,%rax,8), %rdx
	leaq	8(,%rax,8), %rax
	movq	%rdx, -336(%rbp)
	addq	%r15, %rdx
	movq	%rax, -320(%rbp)
	movl	%esi, %eax
	subl	$6, %eax
	movq	%rdx, -328(%rbp)
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$3, %edi
	cmovle	%edi, %edx
	leal	1(%rdx), %esi
	movl	%edx, -232(%rbp)
	movl	%esi, -308(%rbp)
	movl	%edx, %esi
	movslq	%edx, %rdx
	leaq	0(,%rdx,8), %rdi
	subl	%esi, %eax
	movl	%eax, -340(%rbp)
	leaq	-8(%rbx), %rax
	movq	%rax, -368(%rbp)
	addq	%r15, %rax
	movq	%rax, -376(%rbp)
	movslq	%ecx, %rax
	movq	%rdi, -360(%rbp)
	salq	$3, %rax
	addq	%r15, %rdi
	movq	%rdi, -352(%rbp)
	movq	%rax, -248(%rbp)
.L90:
	movl	-64(%rbp), %eax
	vxorpd	%xmm1, %xmm1, %xmm1
	testl	%eax, %eax
	jle	.L80
	movq	%r15, %r14
	vxorpd	%xmm1, %xmm1, %xmm1
	vxorpd	%xmm5, %xmm5, %xmm5
	movq	$0, -256(%rbp)
.L79:
	movq	-216(%rbp), %rbx
	vmovapd	%ymm5, -144(%rbp)
	movq	-256(%rbp), %rdi
	vmovsd	%xmm1, -112(%rbp)
	movq	40(%rbx), %rax
	movq	24(%rbx), %rdx
	movl	(%rax,%rdi), %eax
	imull	36(%rbx), %eax
	cltq
	leaq	(%rdx,%rax,8), %r15
	xorl	%eax, %eax
	vzeroupper
	call	timestamp
	movl	-60(%rbp), %edi
	vxorpd	%xmm5, %xmm5, %xmm5
	vcvtsi2sdq	%rax, %xmm5, %xmm5
	vmovsd	-112(%rbp), %xmm1
	testl	%edi, %edi
	vmovsd	%xmm5, -56(%rbp)
	vmovapd	-144(%rbp), %ymm5
	jle	.L92
	movq	%rbx, %rsi
	movq	%r14, %rax
	movq	%r14, -144(%rbp)
	xorl	%r11d, %r11d
	movq	(%rsi), %r12
	movl	36(%rsi), %esi
	movq	40(%rbx), %rbx
	movl	-64(%rbp), %r13d
	movq	-264(%rbp), %r14
	movl	%esi, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L49:
	movl	-112(%rbp), %esi
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%ymm5, (%rax)
	vmovapd	%ymm0, %ymm8
	movl	(%rbx,%r11), %edx
	vmovapd	%ymm0, %ymm7
	vmovapd	%ymm0, %ymm6
	imull	%esi, %edx
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,8), %r10
	movl	4(%rbx,%r11), %edx
	imull	%esi, %edx
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,8), %r9
	movl	8(%rbx,%r11), %edx
	imull	%esi, %edx
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,8), %r8
	movl	12(%rbx,%r11), %edx
	imull	%esi, %edx
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,8), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L46:
#APP
# 246 "v2/src/invmat.c" 1
	# [START] (residue) AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%r15,%rdx), %ymm4
	vfmadd231pd	(%r10,%rdx), %ymm4, %ymm0
	vfmadd231pd	(%r9,%rdx), %ymm4, %ymm8
	vfmadd231pd	(%r8,%rdx), %ymm4, %ymm7
	vfmadd231pd	(%rdi,%rdx), %ymm4, %ymm6
#APP
# 251 "v2/src/invmat.c" 1
	# [END] (residue) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rdx
	cmpq	%r14, %rdx
	jne	.L46
	movl	-228(%rbp), %esi
	cmpl	%esi, %r13d
	jle	.L47
	movq	-248(%rbp), %rdx
	movq	%rdx, %rcx
	addq	%rdx, %r10
	addq	%rdx, %r9
	addq	%rdx, %r8
	addq	%rdx, %rdi
	addq	%r15, %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L48:
	addl	$1, %esi
	vmovsd	(%r10,%rdx), %xmm2
	addq	$8, %rcx
	vmovsd	(%rax), %xmm3
	vfnmadd132sd	-8(%rcx), %xmm3, %xmm2
	vmovsd	%xmm2, (%rax)
	vmovsd	8(%rax), %xmm3
	vmovsd	(%r9,%rdx), %xmm2
	vfnmadd132sd	-8(%rcx), %xmm3, %xmm2
	vmovsd	%xmm2, 8(%rax)
	vmovsd	16(%rax), %xmm3
	vmovsd	(%r8,%rdx), %xmm2
	vfnmadd132sd	-8(%rcx), %xmm3, %xmm2
	vmovsd	%xmm2, 16(%rax)
	vmovsd	24(%rax), %xmm3
	vmovsd	(%rdi,%rdx), %xmm2
	addq	$8, %rdx
	vfnmadd132sd	-8(%rcx), %xmm3, %xmm2
	vmovsd	%xmm2, 24(%rax)
	cmpl	%esi, %r13d
	jg	.L48
.L47:
	vmovsd	(%rax), %xmm4
	vmovapd	%xmm0, %xmm2
	addq	$32, %rax
	addq	$16, %r11
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm0, %xmm0, %xmm2
	vextractf128	$0x1, %ymm0, %xmm0
	vsubsd	%xmm3, %xmm4, %xmm3
	vmovapd	%xmm0, %xmm4
	vunpckhpd	%xmm0, %xmm0, %xmm0
	vsubsd	%xmm2, %xmm3, %xmm2
	vsubsd	%xmm4, %xmm2, %xmm2
	vmovsd	-24(%rax), %xmm4
	vsubsd	%xmm0, %xmm2, %xmm3
	vmovapd	%xmm8, %xmm0
	vmovapd	%xmm0, %xmm2
	vunpckhpd	%xmm8, %xmm8, %xmm0
	vextractf128	$0x1, %ymm8, %xmm8
	vsubsd	%xmm2, %xmm4, %xmm2
	vmovapd	%xmm8, %xmm4
	vunpckhpd	%xmm8, %xmm8, %xmm8
	vmovsd	%xmm3, -32(%rax)
	vfmadd132sd	%xmm3, %xmm1, %xmm3
	vsubsd	%xmm0, %xmm2, %xmm0
	vsubsd	%xmm4, %xmm0, %xmm0
	vmovapd	%xmm7, %xmm4
	vsubsd	%xmm8, %xmm0, %xmm2
	vmovsd	-16(%rax), %xmm8
	vmovapd	%xmm4, %xmm0
	vunpckhpd	%xmm7, %xmm7, %xmm4
	vextractf128	$0x1, %ymm7, %xmm7
	vsubsd	%xmm0, %xmm8, %xmm0
	vmovapd	%xmm7, %xmm8
	vunpckhpd	%xmm7, %xmm7, %xmm7
	vmovsd	%xmm2, -24(%rax)
	vfmadd132sd	%xmm2, %xmm3, %xmm2
	vsubsd	%xmm4, %xmm0, %xmm4
	vsubsd	%xmm8, %xmm4, %xmm0
	vmovapd	%xmm6, %xmm4
	vmovsd	-8(%rax), %xmm8
	vsubsd	%xmm7, %xmm0, %xmm0
	vmovapd	%xmm4, %xmm7
	vunpckhpd	%xmm6, %xmm6, %xmm4
	vsubsd	%xmm7, %xmm8, %xmm8
	vmovsd	%xmm0, -16(%rax)
	vfmadd132sd	%xmm0, %xmm2, %xmm0
	vsubsd	%xmm4, %xmm8, %xmm8
	vextractf128	$0x1, %ymm6, %xmm4
	vmovapd	%xmm4, %xmm7
	vunpckhpd	%xmm4, %xmm4, %xmm4
	vsubsd	%xmm7, %xmm8, %xmm7
	vsubsd	%xmm4, %xmm7, %xmm6
	vmovapd	%xmm6, %xmm1
	vmovsd	%xmm6, -8(%rax)
	vfmadd132sd	%xmm6, %xmm0, %xmm1
	cmpq	%r11, -240(%rbp)
	jne	.L49
	movq	-144(%rbp), %r14
	movl	-228(%rbp), %eax
	cmpl	%eax, -64(%rbp)
	jle	.L50
	movq	-216(%rbp), %r13
	movl	%eax, %r9d
	movl	%eax, %r11d
.L45:
	movq	-264(%rbp), %rax
	movslq	%r9d, %rcx
	movl	-64(%rbp), %r10d
	salq	$2, %rcx
	leaq	(%r15,%rax), %r8
	.p2align 4,,10
	.p2align 3
.L56:
	movl	(%rbx,%rcx), %eax
	movq	$0, (%r14,%rcx,2)
	imull	36(%r13), %eax
	movl	-60(%rbp), %esi
	cltq
	leaq	(%r12,%rax,8), %rdi
	testl	%esi, %esi
	jle	.L93
	movq	%r15, %rax
	movq	%rdi, %rdx
	vxorpd	%xmm2, %xmm2, %xmm2
	.p2align 4,,10
	.p2align 3
.L52:
#APP
# 283 "v2/src/invmat.c" 1
	# [START] (residue) AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%rax), %ymm7
	vfmadd231pd	(%rdx), %ymm7, %ymm2
#APP
# 285 "v2/src/invmat.c" 1
	# [END] (residue) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rax
	addq	$32, %rdx
	cmpq	%r8, %rax
	jne	.L52
	vmovsd	(%r14,%rcx,2), %xmm0
	cmpl	%r11d, %r10d
	jle	.L54
	movl	%r11d, %edx
.L51:
	movslq	%edx, %rsi
	xorl	%eax, %eax
	salq	$3, %rsi
	addq	%rsi, %rdi
	addq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L55:
	addl	$1, %edx
	vmovsd	(%rdi,%rax), %xmm7
	vfnmadd231sd	(%rsi,%rax), %xmm7, %xmm0
	addq	$8, %rax
	vmovsd	%xmm0, (%r14,%rcx,2)
	cmpl	%edx, %r10d
	jg	.L55
.L54:
	vmovapd	%xmm2, %xmm3
	addl	$1, %r9d
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm2, %xmm2, %xmm3
	vsubsd	%xmm4, %xmm0, %xmm0
	vsubsd	%xmm3, %xmm0, %xmm0
	vextractf128	$0x1, %ymm2, %xmm3
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm3, %xmm3, %xmm3
	vsubsd	%xmm4, %xmm0, %xmm0
	vsubsd	%xmm3, %xmm0, %xmm2
	vmovsd	%xmm2, (%r14,%rcx,2)
	vfmadd231sd	%xmm2, %xmm2, %xmm1
	addq	$4, %rcx
	cmpl	%r9d, %r10d
	jg	.L56
.L50:
	movq	-256(%rbp), %rax
	vmovapd	%ymm5, -144(%rbp)
	vmovsd	(%r14,%rax,2), %xmm0
	vfnmadd231sd	%xmm0, %xmm0, %xmm1
	vaddsd	.LC2(%rip), %xmm0, %xmm0
	vmovsd	%xmm0, (%r14,%rax,2)
	xorl	%eax, %eax
	vfmadd231sd	%xmm0, %xmm0, %xmm1
	vmovsd	%xmm1, -112(%rbp)
	vzeroupper
	call	timestamp
	movq	-288(%rbp), %rsi
	vxorpd	%xmm0, %xmm0, %xmm0
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsi2sdq	%rax, %xmm2, %xmm2
	vsubsd	-56(%rbp), %xmm2, %xmm2
	vcvtsi2sdq	(%rsi), %xmm0, %xmm0
	vaddsd	%xmm0, %xmm2, %xmm0
	vcvttsd2siq	%xmm0, %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	call	timestamp
	vxorpd	%xmm5, %xmm5, %xmm5
	cmpl	$5, -60(%rbp)
	vmovsd	-112(%rbp), %xmm1
	vcvtsi2sdq	%rax, %xmm5, %xmm5
	movq	-216(%rbp), %rax
	vmovsd	%xmm5, -272(%rbp)
	vmovapd	-144(%rbp), %ymm5
	jle	.L122
	movl	36(%rax), %esi
	leaq	40(%r14), %r13
	movq	$40, -144(%rbp)
	movq	8(%rax), %rax
	leaq	48(%r14), %r12
	movl	$5, -56(%rbp)
	leaq	56(%r14), %rcx
	movq	%r15, -176(%rbp)
	movl	%esi, -112(%rbp)
	leaq	64(%r14), %rsi
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L63:
	movl	-56(%rbp), %edx
	vxorpd	%xmm6, %xmm6, %xmm6
	vmovapd	%ymm6, %ymm4
	vmovapd	%ymm6, %ymm3
	movl	-112(%rbp), %r15d
	vmovapd	%ymm6, %ymm2
	movq	-168(%rbp), %r9
	leal	-5(%rdx), %r8d
	movl	%edx, %eax
	imull	%r15d, %eax
	shrl	$2, %r8d
	cltq
	leaq	(%r9,%rax,8), %rdi
	movl	%edx, %eax
	addl	$1, %eax
	imull	%r15d, %eax
	cltq
	leaq	(%r9,%rax,8), %rbx
	movl	%edx, %eax
	addl	$2, %eax
	imull	%r15d, %eax
	cltq
	leaq	(%r9,%rax,8), %r11
	movl	%edx, %eax
	movl	%r8d, %edx
	addl	$3, %eax
	addq	$1, %rdx
	imull	%r15d, %eax
	salq	$5, %rdx
	cltq
	leaq	(%r9,%rax,8), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L59:
#APP
# 334 "v2/src/invmat.c" 1
	# [START] (Ly=b) AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%r14,%rax), %ymm0
	vfmadd231pd	(%rdi,%rax), %ymm0, %ymm6
	vfmadd231pd	(%rbx,%rax), %ymm0, %ymm4
	vfmadd231pd	(%r11,%rax), %ymm0, %ymm3
	vfmadd231pd	(%r9,%rax), %ymm0, %ymm2
#APP
# 339 "v2/src/invmat.c" 1
	# [END] (Ly=b) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rax
	cmpq	%rdx, %rax
	jne	.L59
	leal	4(,%r8,4), %eax
	cmpl	%eax, -56(%rbp)
	jle	.L123
	movl	-56(%rbp), %r15d
	movq	%r12, %rdx
	movslq	%eax, %r8
	movq	%r14, -160(%rbp)
	subq	%r14, %rdx
	salq	$3, %r8
	movq	%rdx, -80(%rbp)
	movq	%rcx, %rdx
	addq	%r8, %rdi
	subq	%r14, %rdx
	leal	-1(%r15), %r10d
	movq	%rdx, -72(%rbp)
	leaq	(%r14,%r8), %rdx
	subl	%eax, %r10d
	leaq	1(%r10), %rax
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %r14
	leaq	(%rbx,%r8), %r15
	xorl	%eax, %eax
	leaq	(%r11,%r8), %r10
	addq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L62:
	vmovsd	(%rdx), %xmm0
	addq	$8, %rdx
	vmovsd	0(%r13), %xmm7
	vfnmadd132sd	(%rdi,%rax,8), %xmm7, %xmm0
	vmovsd	%xmm0, 0(%r13)
	vmovsd	-8(%rdx), %xmm0
	vmovsd	(%r12), %xmm7
	vfnmadd132sd	(%r15,%rax,8), %xmm7, %xmm0
	vmovsd	%xmm0, (%r12)
	vmovsd	-8(%rdx), %xmm0
	vmovsd	(%rcx), %xmm7
	vfnmadd132sd	(%r10,%rax,8), %xmm7, %xmm0
	vmovsd	%xmm0, (%rcx)
	vmovsd	-8(%rdx), %xmm0
	vmovsd	(%rsi), %xmm7
	vfnmadd132sd	(%r8,%rax,8), %xmm7, %xmm0
	addq	$1, %rax
	vmovsd	%xmm0, (%rsi)
	cmpq	%rax, %r14
	jne	.L62
	movq	-160(%rbp), %r14
.L61:
	vmovapd	%xmm6, %xmm0
	movq	-80(%rbp), %rdi
	addq	$32, %r13
	addq	$32, %r12
	vmovsd	-32(%r13), %xmm8
	vmovapd	%xmm0, %xmm7
	vunpckhpd	%xmm6, %xmm6, %xmm0
	addq	$32, %rcx
	vextractf128	$0x1, %ymm6, %xmm6
	movq	-144(%rbp), %rax
	addq	$32, %rsi
	vsubsd	%xmm7, %xmm8, %xmm7
	vsubsd	%xmm0, %xmm7, %xmm0
	vmovapd	%xmm6, %xmm7
	vunpckhpd	%xmm6, %xmm6, %xmm6
	vsubsd	%xmm7, %xmm0, %xmm0
	vsubsd	%xmm6, %xmm0, %xmm0
	vmovsd	%xmm0, -32(%r13)
	vmovapd	%xmm4, %xmm0
	vmovsd	-32(%r12), %xmm7
	vmovapd	%xmm0, %xmm6
	vunpckhpd	%xmm4, %xmm4, %xmm0
	vextractf128	$0x1, %ymm4, %xmm4
	vsubsd	%xmm6, %xmm7, %xmm6
	vsubsd	%xmm0, %xmm6, %xmm0
	vmovapd	%xmm4, %xmm6
	vunpckhpd	%xmm4, %xmm4, %xmm4
	vsubsd	%xmm6, %xmm0, %xmm0
	vsubsd	%xmm4, %xmm0, %xmm0
	vmovsd	%xmm0, -32(%r12)
	vmovapd	%xmm3, %xmm0
	vmovsd	-32(%rcx), %xmm6
	vmovapd	%xmm0, %xmm4
	vunpckhpd	%xmm3, %xmm3, %xmm0
	vextractf128	$0x1, %ymm3, %xmm3
	vsubsd	%xmm4, %xmm6, %xmm4
	vsubsd	%xmm0, %xmm4, %xmm0
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm3, %xmm3, %xmm3
	vsubsd	%xmm4, %xmm0, %xmm0
	vsubsd	%xmm3, %xmm0, %xmm0
	vmovsd	%xmm0, -32(%rcx)
	vmovapd	%xmm2, %xmm0
	vmovsd	-32(%rsi), %xmm4
	vmovapd	%xmm0, %xmm3
	vunpckhpd	%xmm2, %xmm2, %xmm0
	vextractf128	$0x1, %ymm2, %xmm2
	vsubsd	%xmm3, %xmm4, %xmm3
	vsubsd	%xmm0, %xmm3, %xmm0
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vsubsd	%xmm3, %xmm0, %xmm0
	vsubsd	%xmm2, %xmm0, %xmm0
	vmovsd	%xmm0, -32(%rsi)
	vmovsd	-32(%r12), %xmm6
	vmovsd	-32(%r13), %xmm0
	vfnmadd132sd	(%rbx,%rax), %xmm6, %xmm0
	vmovsd	%xmm0, -32(%r12)
	vmovsd	-32(%rcx), %xmm4
	vmovsd	-32(%r13), %xmm0
	vfnmadd132sd	(%r11,%rax), %xmm4, %xmm0
	vmovsd	%xmm0, -32(%rcx)
	vmovsd	-32(%rsi), %xmm6
	vmovsd	-32(%r13), %xmm0
	vfnmadd132sd	(%r9,%rax), %xmm6, %xmm0
	vmovsd	%xmm0, -32(%rsi)
	addq	$32, %rax
	vmovsd	-32(%rcx), %xmm4
	vmovsd	-32(%r12), %xmm0
	vfnmadd132sd	(%r11,%rdi), %xmm4, %xmm0
	vmovsd	%xmm0, -32(%rcx)
	vmovsd	-32(%r12), %xmm0
	vmovsd	-32(%rsi), %xmm6
	vfnmadd132sd	(%r9,%rdi), %xmm6, %xmm0
	vmovsd	%xmm0, -32(%rsi)
	movq	-72(%rbp), %rdi
	vmovsd	-32(%rcx), %xmm4
	vfnmadd231sd	(%r9,%rdi), %xmm4, %xmm0
	addl	$4, -56(%rbp)
	vmovsd	%xmm0, -32(%rsi)
	movl	-56(%rbp), %ebx
	movq	%rax, -144(%rbp)
	cmpl	-60(%rbp), %ebx
	jl	.L63
	movq	-176(%rbp), %r15
	movl	-344(%rbp), %r8d
.L58:
	movl	-64(%rbp), %edx
	cmpl	%r8d, %edx
	jle	.L64
	movl	-112(%rbp), %esi
	leal	-5(%r8), %r9d
	movslq	%r8d, %rax
	leaq	(%r14,%rax,8), %rdi
	movq	-216(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L70:
	movl	%r8d, %eax
	imull	%esi, %eax
	cltq
	leaq	(%rcx,%rax,8), %rbx
	leal	-4(%r8), %eax
	testl	%eax, %eax
	jle	.L94
	movl	%r9d, %r12d
	movq	%rbx, %rax
	movq	%r14, %r10
	vxorpd	%xmm2, %xmm2, %xmm2
	shrl	$2, %r12d
	movl	%r12d, %r11d
	addq	$1, %r11
	salq	$5, %r11
	addq	%rbx, %r11
	.p2align 4,,10
	.p2align 3
.L66:
#APP
# 375 "v2/src/invmat.c" 1
	# [START] (Ly=b) AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%r10), %ymm6
	vfmadd231pd	(%rax), %ymm6, %ymm2
#APP
# 377 "v2/src/invmat.c" 1
	# [END] (Ly=b) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rax
	addq	$32, %r10
	cmpq	%r11, %rax
	jne	.L66
	leal	4(,%r12,4), %eax
.L65:
	vmovsd	(%rdi), %xmm0
	cmpl	%r8d, %eax
	jge	.L68
	leal	-1(%r8), %r11d
	movslq	%eax, %r10
	subl	%eax, %r11d
	salq	$3, %r10
	xorl	%eax, %eax
	leaq	(%r14,%r10), %r12
	addq	$1, %r11
	addq	%rbx, %r10
	.p2align 4,,10
	.p2align 3
.L69:
	vmovsd	(%r12,%rax,8), %xmm6
	vfnmadd231sd	(%r10,%rax,8), %xmm6, %xmm0
	addq	$1, %rax
	vmovsd	%xmm0, (%rdi)
	cmpq	%rax, %r11
	jne	.L69
.L68:
	vmovapd	%xmm2, %xmm3
	addl	$1, %r8d
	addq	$8, %rdi
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm2, %xmm2, %xmm3
	vextractf128	$0x1, %ymm2, %xmm2
	addl	$1, %r9d
	vsubsd	%xmm4, %xmm0, %xmm0
	vmovapd	%xmm2, %xmm4
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vsubsd	%xmm3, %xmm0, %xmm3
	vsubsd	%xmm4, %xmm3, %xmm3
	vsubsd	%xmm2, %xmm3, %xmm3
	vmovsd	%xmm3, -8(%rdi)
	cmpl	%r8d, %edx
	jne	.L70
.L64:
	movq	-216(%rbp), %rax
	movl	-192(%rbp), %edi
	movq	-376(%rbp), %rbx
	movq	16(%rax), %rax
	vmovsd	(%rbx), %xmm0
	movq	%rax, %rsi
	movq	%rax, -224(%rbp)
	movl	-112(%rbp), %eax
	addl	$1, %eax
	imull	%edi, %eax
	cltq
	vdivsd	(%rsi,%rax,8), %xmm0, %xmm0
	movq	-368(%rbp), %rax
	vmovsd	%xmm0, (%rbx)
	addq	%r15, %rax
	vaddsd	(%rax), %xmm0, %xmm0
	vmovsd	%xmm0, (%rax)
	movl	-312(%rbp), %eax
	cmpl	$3, %eax
	jle	.L71
	movq	-336(%rbp), %rsi
	movq	%r15, -280(%rbp)
	movq	-320(%rbp), %r11
	movl	%edi, -72(%rbp)
	movq	-328(%rbp), %r13
	movl	%eax, -56(%rbp)
	movl	$-4, -188(%rbp)
	leaq	(%r15,%rsi), %rbx
	movq	%rsi, -144(%rbp)
	movq	%r11, %r15
	movq	%rbx, %r11
	.p2align 4,,10
	.p2align 3
.L77:
	movl	-56(%rbp), %ebx
	movl	-112(%rbp), %ecx
	movq	-224(%rbp), %rdx
	movl	%ebx, %eax
	subl	$3, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%rdx,%rax,8), %rsi
	movl	%ebx, %eax
	subl	$2, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%rdx,%rax,8), %rdi
	movl	%ebx, %eax
	subl	$1, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%rdx,%rax,8), %r8
	movl	%ebx, %eax
	movl	-72(%rbp), %ebx
	imull	%ecx, %eax
	movl	-60(%rbp), %ecx
	cltq
	leaq	(%rdx,%rax,8), %r9
	movl	%ebx, %eax
	cmpl	%ecx, %ebx
	jge	.L95
	movl	-188(%rbp), %r10d
	vxorpd	%xmm6, %xmm6, %xmm6
	xorl	%edx, %edx
	vmovapd	%ymm6, %ymm4
	vmovapd	%ymm6, %ymm3
	vmovapd	%ymm6, %ymm0
	shrl	$2, %r10d
	movl	%r10d, %ecx
	addq	$1, %rcx
	salq	$5, %rcx
	.p2align 4,,10
	.p2align 3
.L73:
#APP
# 408 "v2/src/invmat.c" 1
	# [START] (Ux=y) AVX CHECK
# 0 "" 2
#NO_APP
	leaq	(%rdx,%r15), %rax
	vmovupd	(%r14,%rax), %ymm2
	vfmadd231pd	(%rsi,%rax), %ymm2, %ymm6
	vfmadd231pd	(%rdi,%rax), %ymm2, %ymm4
	vfmadd231pd	(%r8,%rax), %ymm2, %ymm3
	vfmadd231pd	(%r9,%rax), %ymm2, %ymm0
#APP
# 413 "v2/src/invmat.c" 1
	# [END] (Ux=y) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rdx
	cmpq	%rcx, %rdx
	jne	.L73
	movl	-56(%rbp), %eax
	leal	5(%rax,%r10,4), %eax
.L72:
	cmpl	%eax, -64(%rbp)
	jle	.L124
	movq	-144(%rbp), %rbx
	movq	%r14, -208(%rbp)
	movl	-192(%rbp), %r10d
	leaq	-24(%rbx), %rcx
	movq	%rcx, -184(%rbp)
	leaq	-24(%r13), %rcx
	subl	%eax, %r10d
	movq	%rcx, -176(%rbp)
	leaq	-16(%rbx), %rcx
	subq	$8, %rbx
	movq	%rcx, -168(%rbp)
	leaq	-16(%r13), %rcx
	movq	%rcx, -160(%rbp)
	movslq	%eax, %rcx
	leaq	1(%r10), %rax
	salq	$3, %rcx
	movq	%rbx, -152(%rbp)
	leaq	(%r14,%rcx), %rdx
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	-200(%rbp), %r14
	leaq	-8(%r13), %rbx
	movq	%rbx, -80(%rbp)
	leaq	(%rsi,%rcx), %r12
	leaq	(%rdi,%rcx), %rbx
	leaq	(%r8,%rcx), %r10
	addq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L76:
	vmovsd	(%rdx), %xmm2
	addq	$8, %rdx
	vmovsd	-24(%r13), %xmm7
	vfnmadd132sd	(%r12,%rax,8), %xmm7, %xmm2
	vmovsd	%xmm2, -24(%r13)
	vmovsd	-16(%r13), %xmm7
	vmovsd	-8(%rdx), %xmm2
	vfnmadd132sd	(%rbx,%rax,8), %xmm7, %xmm2
	vmovsd	%xmm2, -16(%r13)
	vmovsd	-8(%r13), %xmm7
	vmovsd	-8(%rdx), %xmm2
	vfnmadd132sd	(%r10,%rax,8), %xmm7, %xmm2
	vmovsd	%xmm2, -8(%r13)
	vmovsd	0(%r13), %xmm7
	vmovsd	-8(%rdx), %xmm2
	vfnmadd132sd	(%rcx,%rax,8), %xmm7, %xmm2
	addq	$1, %rax
	vmovsd	%xmm2, 0(%r13)
	cmpq	%rax, %r14
	jne	.L76
	movq	-208(%rbp), %r14
	movq	-160(%rbp), %rbx
	movq	-80(%rbp), %rcx
.L75:
	movq	-176(%rbp), %rax
	vmovapd	%xmm6, %xmm2
	subq	$32, %r13
	subq	$32, %r11
	vmovapd	%xmm2, %xmm7
	vunpckhpd	%xmm6, %xmm6, %xmm2
	vextractf128	$0x1, %ymm6, %xmm6
	subq	$32, %r15
	movq	-144(%rbp), %rdx
	vmovsd	(%rax), %xmm8
	vsubsd	%xmm7, %xmm8, %xmm7
	vsubsd	%xmm2, %xmm7, %xmm2
	vmovapd	%xmm6, %xmm7
	vunpckhpd	%xmm6, %xmm6, %xmm6
	vsubsd	%xmm7, %xmm2, %xmm2
	vsubsd	%xmm6, %xmm2, %xmm2
	vmovsd	%xmm2, (%rax)
	vmovapd	%xmm4, %xmm2
	vmovsd	(%rbx), %xmm7
	vmovapd	%xmm2, %xmm6
	vunpckhpd	%xmm4, %xmm4, %xmm2
	vextractf128	$0x1, %ymm4, %xmm4
	vsubsd	%xmm6, %xmm7, %xmm6
	vsubsd	%xmm2, %xmm6, %xmm2
	vmovapd	%xmm4, %xmm6
	vunpckhpd	%xmm4, %xmm4, %xmm4
	vsubsd	%xmm6, %xmm2, %xmm2
	vsubsd	%xmm4, %xmm2, %xmm2
	vmovsd	%xmm2, (%rbx)
	vmovapd	%xmm3, %xmm2
	vmovsd	(%rcx), %xmm6
	vmovapd	%xmm2, %xmm4
	vunpckhpd	%xmm3, %xmm3, %xmm2
	vextractf128	$0x1, %ymm3, %xmm3
	vsubsd	%xmm4, %xmm6, %xmm4
	vsubsd	%xmm2, %xmm4, %xmm2
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm3, %xmm3, %xmm3
	vsubsd	%xmm4, %xmm2, %xmm2
	vsubsd	%xmm3, %xmm2, %xmm2
	vmovsd	%xmm2, (%rcx)
	vmovapd	%xmm0, %xmm2
	vmovsd	32(%r13), %xmm4
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm0, %xmm0, %xmm2
	vextractf128	$0x1, %ymm0, %xmm0
	vsubsd	%xmm3, %xmm4, %xmm3
	vsubsd	%xmm2, %xmm3, %xmm2
	vmovapd	%xmm0, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm2
	vunpckhpd	%xmm0, %xmm0, %xmm3
	vsubsd	%xmm3, %xmm2, %xmm0
	vmovsd	%xmm0, 32(%r13)
	vdivsd	(%r9,%rdx), %xmm0, %xmm0
	movq	-152(%rbp), %r9
	vmovsd	%xmm0, 32(%r13)
	vmovsd	(%rcx), %xmm6
	vfnmadd132sd	(%r8,%rdx), %xmm6, %xmm0
	vmovsd	%xmm0, (%rcx)
	vdivsd	(%r8,%r9), %xmm0, %xmm0
	movq	-168(%rbp), %r8
	vmovsd	%xmm0, (%rcx)
	vmovsd	(%rbx), %xmm4
	vfnmadd132sd	(%rdi,%r9), %xmm4, %xmm0
	vmovsd	%xmm0, (%rbx)
	vmovsd	32(%r13), %xmm6
	vfnmadd231sd	(%rdi,%rdx), %xmm6, %xmm0
	vmovsd	%xmm0, (%rbx)
	vdivsd	(%rdi,%r8), %xmm0, %xmm0
	vmovsd	%xmm0, (%rbx)
	vmovsd	(%rax), %xmm4
	vfnmadd132sd	(%rsi,%r8), %xmm4, %xmm0
	vmovsd	%xmm0, (%rax)
	vmovsd	(%rcx), %xmm6
	vfnmadd231sd	(%rsi,%r9), %xmm6, %xmm0
	vmovsd	%xmm0, (%rax)
	movq	-184(%rbp), %rdi
	vmovsd	32(%r13), %xmm4
	vfnmadd231sd	(%rsi,%rdx), %xmm4, %xmm0
	vmovsd	%xmm0, (%rax)
	subq	$32, %rdx
	subl	$4, -56(%rbp)
	movq	%rdx, -144(%rbp)
	subl	$4, -72(%rbp)
	vdivsd	(%rsi,%rdi), %xmm0, %xmm0
	addl	$4, -188(%rbp)
	vmovsd	%xmm0, (%rax)
	vaddsd	8(%r11), %xmm0, %xmm0
	movl	-56(%rbp), %eax
	vmovsd	%xmm0, 8(%r11)
	vmovsd	16(%r11), %xmm0
	vaddsd	(%rbx), %xmm0, %xmm0
	vmovsd	%xmm0, 16(%r11)
	vmovsd	24(%r11), %xmm0
	vaddsd	(%rcx), %xmm0, %xmm0
	vmovsd	%xmm0, 24(%r11)
	vmovsd	32(%r11), %xmm0
	vaddsd	32(%r13), %xmm0, %xmm0
	vmovsd	%xmm0, 32(%r11)
	cmpl	$3, %eax
	jg	.L77
	movq	-280(%rbp), %r15
.L71:
	movl	-232(%rbp), %edx
	testl	%edx, %edx
	js	.L87
	movl	-340(%rbp), %r10d
	movq	-352(%rbp), %rcx
	movq	-360(%rbp), %rsi
	movl	-308(%rbp), %r8d
	movl	-232(%rbp), %edi
	movq	-224(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-216(%rbp), %rbx
	movl	%r8d, %edx
	movl	36(%rbx), %eax
	imull	%edi, %eax
	cltq
	leaq	0(%r13,%rax,8), %r9
	cmpl	-60(%rbp), %r8d
	jge	.L96
	movl	%r10d, %r11d
	movslq	%r8d, %rbx
	vxorpd	%xmm2, %xmm2, %xmm2
	leaq	0(,%rbx,8), %rdx
	shrl	$2, %r11d
	leaq	(%r9,%rdx), %rax
	movl	%r11d, %r12d
	addq	%r14, %rdx
	leaq	4(%rbx,%r12,4), %rbx
	leaq	(%r9,%rbx,8), %rbx
	.p2align 4,,10
	.p2align 3
.L82:
#APP
# 456 "v2/src/invmat.c" 1
	# [START] (Ux=y) (Remainder) AVX CHECK
# 0 "" 2
#NO_APP
	vmovupd	(%rax), %ymm6
	vfmadd231pd	(%rdx), %ymm6, %ymm2
#APP
# 458 "v2/src/invmat.c" 1
	# [END] (Ux=y) (Remainder) AVX CHECK
# 0 "" 2
#NO_APP
	addq	$32, %rax
	addq	$32, %rdx
	cmpq	%rbx, %rax
	jne	.L82
	leal	5(%rdi,%r11,4), %edx
.L81:
	vmovsd	(%rcx), %xmm0
	cmpl	%edx, -64(%rbp)
	jle	.L84
	movl	-192(%rbp), %r11d
	xorl	%eax, %eax
	subl	%edx, %r11d
	movslq	%edx, %rdx
	salq	$3, %rdx
	addq	$1, %r11
	leaq	(%r14,%rdx), %rbx
	addq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L85:
	vmovsd	(%rbx,%rax,8), %xmm6
	vfnmadd231sd	(%rdx,%rax,8), %xmm6, %xmm0
	addq	$1, %rax
	vmovsd	%xmm0, (%rcx)
	cmpq	%r11, %rax
	jne	.L85
.L84:
	vmovapd	%xmm2, %xmm3
	subl	$1, %edi
	subl	$1, %r8d
	vmovapd	%xmm3, %xmm4
	vunpckhpd	%xmm2, %xmm2, %xmm3
	subq	$8, %rcx
	addl	$1, %r10d
	vsubsd	%xmm4, %xmm0, %xmm4
	vextractf128	$0x1, %ymm2, %xmm2
	vsubsd	%xmm3, %xmm4, %xmm3
	vmovapd	%xmm2, %xmm4
	vsubsd	%xmm4, %xmm3, %xmm3
	vunpckhpd	%xmm2, %xmm2, %xmm4
	vsubsd	%xmm4, %xmm3, %xmm4
	vmovsd	%xmm4, 8(%rcx)
	vdivsd	(%r9,%rsi), %xmm4, %xmm4
	vmovsd	%xmm4, 8(%rcx)
	vaddsd	(%r15,%rsi), %xmm4, %xmm4
	vmovsd	%xmm4, (%r15,%rsi)
	subq	$8, %rsi
	cmpl	$-1, %edi
	jne	.L86
.L87:
	xorl	%eax, %eax
	vmovapd	%ymm5, -112(%rbp)
	vmovsd	%xmm1, -56(%rbp)
	vzeroupper
	call	timestamp
	movq	-296(%rbp), %rsi
	vxorpd	%xmm0, %xmm0, %xmm0
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsi2sdq	%rax, %xmm2, %xmm2
	vmovsd	-56(%rbp), %xmm1
	vsubsd	-272(%rbp), %xmm2, %xmm2
	addq	$4, -256(%rbp)
	vmovapd	-112(%rbp), %ymm5
	vcvtsi2sdq	(%rsi), %xmm0, %xmm0
	vaddsd	%xmm0, %xmm2, %xmm0
	vcvttsd2siq	%xmm0, %rax
	movq	%rax, (%rsi)
	movq	-256(%rbp), %rax
	cmpq	-304(%rbp), %rax
	jne	.L79
	movq	%r14, %r15
	vzeroupper
.L80:
	vsqrtsd	%xmm1, %xmm0, %xmm0
	vucomisd	%xmm0, %xmm0
	jp	.L125
.L88:
	movl	-380(%rbp), %ebx
	movl	$.LC4, %edx
	movl	$1, %esi
	movl	$1, %eax
	movq	-392(%rbp), %rdi
	movl	%ebx, %ecx
	call	__fprintf_chk
	movl	%ebx, %eax
	addl	$1, %eax
	movl	%eax, -380(%rbp)
	cmpl	%eax, -384(%rbp)
	jge	.L90
.L44:
	movq	-288(%rbp), %rsi
	movslq	-384(%rbp), %rcx
	movq	(%rsi), %rax
	cqto
	idivq	%rcx
	movq	%rax, (%rsi)
	movq	-296(%rbp), %rsi
	movq	(%rsi), %rax
	cqto
	idivq	%rcx
	movq	%rax, (%rsi)
	addq	$352, %rsp
	xorl	%eax, %eax
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r10
	.cfi_restore 10
	.cfi_def_cfa 10, 0
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L124:
	.cfi_restore_state
	movq	-144(%rbp), %rax
	leaq	-24(%rax), %rbx
	movq	%rbx, -184(%rbp)
	leaq	-24(%r13), %rbx
	movq	%rbx, -176(%rbp)
	leaq	-16(%rax), %rbx
	subq	$8, %rax
	movq	%rbx, -168(%rbp)
	leaq	-16(%r13), %rbx
	movq	%rax, -152(%rbp)
	leaq	-8(%r13), %rax
	movq	%rbx, -160(%rbp)
	movq	%rax, %rcx
	movq	%rax, -80(%rbp)
	jmp	.L75
.L95:
	vxorpd	%xmm6, %xmm6, %xmm6
	vmovapd	%ymm6, %ymm4
	vmovapd	%ymm6, %ymm3
	vmovapd	%ymm6, %ymm0
	jmp	.L72
.L94:
	vxorpd	%xmm2, %xmm2, %xmm2
	xorl	%eax, %eax
	jmp	.L65
.L123:
	movq	%r12, %rax
	subq	%r14, %rax
	movq	%rax, -80(%rbp)
	movq	%rcx, %rax
	subq	%r14, %rax
	movq	%rax, -72(%rbp)
	jmp	.L61
.L96:
	vxorpd	%xmm2, %xmm2, %xmm2
	jmp	.L81
.L93:
	vxorpd	%xmm0, %xmm0, %xmm0
	vxorpd	%xmm2, %xmm2, %xmm2
	xorl	%edx, %edx
	jmp	.L51
.L92:
	movq	-216(%rbp), %rax
	xorl	%r9d, %r9d
	movl	-228(%rbp), %r11d
	movq	40(%rax), %rbx
	movq	%rax, %r13
	movq	(%rax), %r12
	jmp	.L45
.L122:
	movl	36(%rax), %eax
	movl	$5, %r8d
	movl	%eax, -112(%rbp)
	jmp	.L58
.L125:
	vmovapd	%xmm1, %xmm0
	call	sqrt
	jmp	.L88
.L119:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE4656:
	.size	refine, .-refine
	.section	.text.unlikely
.LCOLDE5:
	.text
.LHOTE5:
	.section	.rodata.str1.1
.LC6:
	.string	"#\n"
.LC8:
	.string	"# Tempo LU: %.3gs\n"
.LC9:
	.string	"# Tempo iter: %.3gs\n"
.LC10:
	.string	"# Tempo Residuo: %.3gs\n#\n"
	.section	.text.unlikely
.LCOLDB11:
	.section	.text.startup,"ax",@progbits
.LHOTB11:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB4657:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rsi, %rbp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	%edi, %ebx
	movl	$20172, %edi
	subq	$64, %rsp
	.cfi_def_cfa_offset 96
	movq	%fs:40, %rax
	movq	%rax, 56(%rsp)
	xorl	%eax, %eax
	movq	$0, (%rsp)
	movq	$0, 8(%rsp)
	call	srand
	leaq	16(%rsp), %rdi
	movq	%rbp, %rdx
	movl	%ebx, %esi
	call	get_args
	pushq	40(%rsp)
	.cfi_def_cfa_offset 104
	pushq	40(%rsp)
	.cfi_def_cfa_offset 112
	pushq	40(%rsp)
	.cfi_def_cfa_offset 120
	pushq	40(%rsp)
	.cfi_def_cfa_offset 128
	call	process_args
	addq	$32, %rsp
	.cfi_def_cfa_offset 96
	testq	%rax, %rax
	je	.L129
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	timestamp
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	lu_decomposition
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L127
	xorl	%eax, %eax
	call	timestamp
	movq	%rbx, %rdi
	subq	%rbp, %rax
	movq	%rax, %rbp
	call	invert_matrix
	movq	24(%rsp), %rcx
	movl	$2, %edx
	movl	$1, %esi
	movl	$.LC6, %edi
	call	fwrite
	movq	24(%rsp), %r8
	movq	%rsp, %rcx
	movq	%rbx, %rdi
	movl	32(%rsp), %esi
	leaq	8(%rsp), %rdx
	call	refine
	movq	24(%rsp), %rdi
	movl	$.LC8, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	%rbp, %xmm0, %xmm0
	vdivsd	.LC7(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	movq	24(%rsp), %rdi
	movl	$.LC9, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	(%rsp), %xmm0, %xmm0
	vdivsd	.LC7(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	movq	24(%rsp), %rdi
	movl	$.LC10, %edx
	movl	$1, %esi
	movl	$1, %eax
	vxorpd	%xmm0, %xmm0, %xmm0
	vcvtsi2sdq	8(%rsp), %xmm0, %xmm0
	vdivsd	.LC7(%rip), %xmm0, %xmm0
	call	__fprintf_chk
	movq	%rbx, %rdi
	call	invmat_free
.L127:
	movq	56(%rsp), %rcx
	xorq	%fs:40, %rcx
	movl	%r12d, %eax
	jne	.L133
	addq	$64, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
.L129:
	.cfi_restore_state
	orl	$-1, %r12d
	jmp	.L127
.L133:
	call	__stack_chk_fail
	.cfi_endproc
.LFE4657:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE11:
	.section	.text.startup
.LHOTE11:
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC7:
	.long	0
	.long	1083129856
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
