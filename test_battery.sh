# !/bin/bash

CPU=$(cat /proc/cpuinfo | grep -m 1 'model name' | cut -d : -f 2 | sed 's/ /_/g' | sed -r 's/^_//g')
LIKWID_PERFCTR="likwid-perfctr -C 0 -f -m"
LIKWID_GROUPS=(L2CACHE FLOPS_AVX L3)
PREFIX=()

# Prepares prefixes for using likwid-perfctr
for i in ${LIKWID_GROUPS[@]};
do
	PREFIX+=("${LIKWID_PERFCTR} -g $i")
done;

VERSIONS=(v2 v1)
EXECUTABLES=(./invmat)
MATRIXES=(32 33 64 65 128 129 256 257 512 1000 2000)
OUTPUT_FOLDER="../data/benchmark"

for version in ${VERSIONS[@]};
do
	cd $version
	make -B > /dev/null

	for item in $( ls src/ );
	do
		item=${item%.c}
		gcc -S src/$item.c -o ${OUTPUT_FOLDER}/../compiled/${version}_AVX_${item}_${CPU}.s -Ilib -Wall -O3 -mavx -march=native -std=gnu99 -I/home/soft/likwid/include -L/home/soft/likwid/lib -llikwid -DLIKWID_PERFMON 2> /dev/null
	done;

	for i in ${!PREFIX[@]};
	do
		printf "[[VERSION: $version]]\n"

		group=$(grep -P -o "\-g [a-zA-Z0-9_]+" <<< "${PREFIX[$i]}" | sed 's/-g //g')
		OUTPUT="${OUTPUT_FOLDER}/${version}_${CPU}_${group}"

		printf "[GROUP: ${group}]\n"

		if [ -f $OUTPUT.txt ];
		then
			if [ ! "$1" == "-f" ];
			then
				printf "Benchmark $group for $CPU already made.\n"
				continue
			else
				rm $OUTPUT.txt
			fi;
		fi;

		for exec in ${EXECUTABLES[@]};
		do
			for n in ${MATRIXES[@]};
			do
				echo "Running test for n: $n..."
				printf "\nN: $n\n" >> ${OUTPUT}.txt
				${PREFIX[$i]} $exec -r $n -i 10 | grep -P "Tempo|MFLOP|data cache miss rate|L2 miss|L3 bandwidth" >> ${OUTPUT}.txt
			done;
		done;
	done;

	make clean > /dev/null
	cd ..
done;
