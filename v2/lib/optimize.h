#ifndef __INVMAT_OPTIMIZATIONS__
#define __INVMAT_OPTIMIZATIONS__

#define CACHE_LINE_SIZE 64
#define CACHE_LINE_DOUBLES (CACHE_LINE_SIZE/sizeof(double))
#define CACHE_ALIGN(n) ( ((n-1)/CACHE_LINE_DOUBLES + 1)*CACHE_LINE_DOUBLES )
#define CACHE_ALIGNED_MALLOC(ptr, n) { int r = posix_memalign((void **) &(ptr), CACHE_LINE_SIZE, (n)); r++; }

#endif
