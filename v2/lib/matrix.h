#ifndef __OPTIMIZED_MATRIX_OPERATIONS__
#define __OPTIMIZED_MATRIX_OPERATIONS__

#include <stdio.h>
#include "optimize.h"

/**
 * @file matrix.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief File with definitions of macros and functions of matrix.c
 * 
 * @see matrix.c
 */

/**
 * @brief Struct for matrix inversion.
 */
typedef struct invmat_t {
	double *A; /**< Original Matrix */
	double *L; /**< Lower Matrix */
	double *U; /**< Upper Matrix */
	double *I; /**< Inverse Matrix */
	int n;     /**< Size of Matrix (No. rows/columns) */
	int N;    /**< Size of each matrix line (N + padding) */
	int *swap; /**< Line swaps (Column swaps for Inverse) */
} invmat_t;



/**
 * @brief      Access L matrix
 *
 * @param      M     invmat_t struct pointer
 * @param      i     line i to access (must be > j)
 * @param      j     column j to access
 *
 * @return     Value of L[i][j]
 */

#define LOWER(M, i, j) (M)->L[(i)*(M)->N + j]


/**
 * @brief      Access U matrix
 *
 * @param      M     invmat_t struct pointer
 * @param      i     line i to access (must be > j)
 * @param      j     column j to access
 *
 * @return     Value of U[i][j]
 */

#define UPPER(M, i, j) (M)->U[(i)*(M)->N + j]



/**
 * @brief      Macro-function to access a regular matrix (represented as a vector)
 * 
 * The matrix's row major, as regular matrixes are often used that way.
 *
 * @param      matrix  The invmat_t struct pointer
 * @param      i       Line to access
 * @param      j       Column to access
 *
 * @return     Value of A[i][j]
 */

#define ORIGINAL(M, i, j) (M)->A[((i)*(M)->N)+j]




/**
 *
 * @brief      Macro-function to access a inverse matrix (represented as a vector)
 *
 * Difference? The inverse matrix's column major, as it's used that way.
 *
 * @param      M     The invmat_t pointer
 * @param      i     Line to access
 * @param      j     Column to access
 *
 * @return     Value of I[i][j]
 */

#define INVERSE(M, i, j) (M)->I[((j)*(M)->N)+i]

/**
 * @brief      Default error value to return on process termination by function lu_decomposition().
 * 
 * Set as the 6th error for executable 'invmat'.\n
 * Error is set 32 as args.h defines other 5 errors with values 1, 2, 4, 8 and 16.
 *
 * @see matrix.c
 * @see args.h
 */

#define __MATRIX_ERROR_NOT_INVERSIBLE 32

int lu_decomposition(invmat_t *M);

invmat_t *invmat_init(long n);

void invmat_free(invmat_t *M);

void square_matrix_generate(invmat_t *M);

void mult_lu(invmat_t *M, FILE *output);
void mult_inverse(invmat_t *M, FILE *output);

void print_matrix(invmat_t *M, FILE *out);
void print_inverse(invmat_t *M, FILE *out);
void print_lu(invmat_t *M, FILE *out);

#endif
