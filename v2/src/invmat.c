#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "timer.h"
#include "matrix.h"
#include "args.h"
#include "double_ops.h"
#include "optimize.h"

#include <likwid.h>
#include <unistd.h>
#include "immintrin.h"

/**
 * @file invmat.c
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 *
 * @brief File contains optimized functions and macros to deal with inversion of double-type matrixes
 *
 * This file contains optimized functions to invert matrixes (allocated as vectors) through:
 * 1. LU Decomposition using Gauss Elimination (with partial pivoting);\n
 * 2. Solving the lar systems Ly = I and Ux = y through Gauss-Seidel;
 * 3. Solution refinement using residues.
 */


/**
 * @brief      Inverts received invmat_t matrix <b>factorized by LU
 *             decomposition with swapped ls</b>!
 *
 *             <b>WARNING:</b> Received matrix must be a resulting matrix from
 *             LU decomposition, executed by lu_decomposition()!
 *
 *             The return will be the inverse matrix of the original matrix,
 *             with unswapped ls from LU decomposition.
 *
 * @param      M     The invmat_t pointer
 *
 * @see        lu_decomposition()
 *
 * @return     Inverse matrix of original matrix
 */
int invert_matrix(invmat_t *M)
{
	// Inverse matrix column-organization
	int *inv = M->swap;

	// Y array to solve both systems (Ly = b) and (Ux = y) (y is also used as x)
	double *restrict y = (double *) malloc (M->n * sizeof(double));

	if(!y)
	{
		fprintf(stderr, "ERROR: in invert_matrix(): could not allocate memory for solution vector.\n");
		return -1;
	}

	int l;

	// Calculation of first solution for LU A⁻¹ = I
	for(int i = 0; i < M->n; i++, inv++)
	{

		//---------------------------------------
		// Ly = b
		//
		// Note that we begin at index i,
		// as the ones before are surely zeroes
		//---------------------------------------

		LIKWID_MARKER_START("op1");
		for(l = i+1, y[i] = 1.0; l < M->n; l++)
		{
			double *restrict L = &(LOWER(M, l, 0));

			__m256d tmp = _mm256_set1_pd(0.0);

			y[l] = 0.0;

			int c;
			for(c = i; c < l-4; c += 4)
			{
				__asm__ volatile ("# [START] (invert_matrix) AVX CHECK 1");
				tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(y + c), _mm256_loadu_pd(L + c)));
				__asm__ volatile ("# [END] (invert_matrix) AVX CHECK 1");
			}
			
			for(; c < l; c++)
			{
				y[l] -= y[c] * L[c];
			}

			y[l] = y[l] - tmp[0] - tmp[1] - tmp[2] - tmp[3];
		}





		//---------------------------------------
		// Ux = y
		//
		// We use y as x, too
		// Two loops:
		//   - one for [(n-1)..i], where
		//     y[l] is defined
		//   - one for [(i-1)..0], where
		//     y[l] is surely zero,
		//     and the array has trash
		//---------------------------------------

		// Loop for defined values of y[l]
		double *restrict I = &(INVERSE(M, 0, *inv));
		for(l--; l >= i; l--)
		{
			double *restrict U = &(UPPER(M, l, 0));

			__m256d tmp = _mm256_set1_pd(0.0);

			I[l] = y[l];

			int c;
			for(c = l+1; c < M->n - 4; c += 4)
			{
				__asm__ volatile ("# [START] (invert_matrix) AVX CHECK 2");
				tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(I + c), _mm256_loadu_pd(U + c)));
				__asm__ volatile ("# [END] (invert_matrix) AVX CHECK 2");
			}

			for(; c < M->n; c++)
				I[l] -= I[c] * U[c];

			I[l] = I[l] - tmp[0] - tmp[1] - tmp[2] - tmp[3];
			I[l] /= U[l];
		}

		// Loop where y[l] has trash, and thus we use 0.0
		for(; l >= 0; l --)
		{
			double *restrict U = &(UPPER(M, l, 0));

			__m256d tmp = _mm256_set1_pd(0.0);

			I[l] = 0.0;

			int c;
			for(c = l+1; c < M->n - 4; c += 4)
			{
				__asm__ volatile ("# [START] (invert_matrix) AVX CHECK 3");
				tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(I + c), _mm256_loadu_pd(U + c)));
				__asm__ volatile ("# [END] (invert_matrix) AVX CHECK 3");
			}
			for(; c < M->n; c++)
				I[l] -= I[c] * U[c];

			I[l] = I[l] - tmp[0] - tmp[1] - tmp[2] - tmp[3];
			I[l] /= U[l];
		}
		LIKWID_MARKER_STOP("op1");
	}

	return 0;
}





/**
 * @brief      Refines first solution returned from function invert_matrix()
 *
 * @param      inv                 The inverse invmat_t pointer
 * @param      matrix              The Original invmat_t A pointer
 * @param      lu                  LU matrix for solving the lar system
 * @param[in]  iter_n              Number of iterations to run
 * @param      mean_residue_timer  The mean residue timer pointer
 * @param      mean_iter_timer     The mean iterator timer pointer
 *
 * @return     Inverse matrix with refined solution
 */
int refine (invmat_t *M, int iter_n, long *mean_residue_timer, long *mean_iter_timer, FILE *output)
{
	// If there are no iterations to run, just stop here
 	if(iter_n == 0) return -1;

 	// Time spent accumulators
 	*mean_iter_timer = *mean_residue_timer = 0.0;

 	// Timers for calculating the spent time for a single iteration
 	double residue_timer, iter_timer;

 	// Main dudes
	double *restrict residue = NULL; // Residue array
	CACHE_ALIGNED_MALLOC(residue, M->n * sizeof(double));
	
	double *restrict x = residue; // Coherence var
	double *restrict y = x; // Coherence var


	// Helpers
	int i,j, iter, col;
	double norm_sum;

	// Code decreasing matters
	int n = M->n;

	 for (iter=1; iter <= iter_n; iter++) {

		 norm_sum = 0;
		 for (col = 0; col < n; col++) { //col desl

		 	//---------------------------------------
		 	// Calculation of residue on column col
		 	//---------------------------------------

			double *restrict I = &(INVERSE(M, 0, M->swap[col]));
		 	{ // Hiperlocal
			 	residue_timer = timestamp();

			 	LIKWID_MARKER_START("op2");

				double *restrict A;
				double *restrict B;
				double *restrict C;
				double *restrict D;

			 	for(i = 0; i < n-4; i += 4)
			 	{
			 		residue[i] = 0;
			 		residue[i+1] = 0;
			 		residue[i+2] = 0;
			 		residue[i+3] = 0;

					__m256d tmp0 = _mm256_set1_pd(0.0);
					__m256d tmp1 = _mm256_set1_pd(0.0);
					__m256d tmp2 = _mm256_set1_pd(0.0);
					__m256d tmp3 = _mm256_set1_pd(0.0);

					A = &(ORIGINAL(M, M->swap[i], 0));
					B = &(ORIGINAL(M, M->swap[i+1], 0));
					C = &(ORIGINAL(M, M->swap[i+2], 0));
					D = &(ORIGINAL(M, M->swap[i+3], 0));

					for(j = 0; j < n-4; j += 4)
			 		{
						// tmpX[j..j+3] += Y[j..j+3] * I[j..j+3], X = [0..3] , Y = [A..D]

						__asm__ volatile("# [START] (residue) AVX CHECK");
						tmp0 = _mm256_add_pd(tmp0, _mm256_mul_pd(_mm256_loadu_pd(A + j), _mm256_loadu_pd(I + j)));
						tmp1 = _mm256_add_pd(tmp1, _mm256_mul_pd(_mm256_loadu_pd(B + j), _mm256_loadu_pd(I + j)));
						tmp2 = _mm256_add_pd(tmp2, _mm256_mul_pd(_mm256_loadu_pd(C + j), _mm256_loadu_pd(I + j)));
						tmp3 = _mm256_add_pd(tmp3, _mm256_mul_pd(_mm256_loadu_pd(D + j), _mm256_loadu_pd(I + j)));
						__asm__ volatile("# [END] (residue) AVX CHECK");
			 		}

					for(; j < n; j++)
					{
						residue[i] -= A[j] * I[j];
						residue[i+1] -= B[j] * I[j];
						residue[i+2] -= C[j] * I[j];
						residue[i+3] -= D[j] * I[j];
					}

					residue[i]   = residue[i]   - tmp0[0] - tmp0[1] - tmp0[2] - tmp0[3];
					residue[i+1] = residue[i+1] - tmp1[0] - tmp1[1] - tmp1[2] - tmp1[3];
					residue[i+2] = residue[i+2] - tmp2[0] - tmp2[1] - tmp2[2] - tmp2[3];
					residue[i+3] = residue[i+3] - tmp3[0] - tmp3[1] - tmp3[2] - tmp3[3];

			 		norm_sum += residue[i] * residue[i];
			 		norm_sum += residue[i+1] * residue[i+1];
			 		norm_sum += residue[i+2] * residue[i+2];
			 		norm_sum += residue[i+3] * residue[i+3];
			 	}

			 	for(; i < n; i++)
			 	{
			 		residue[i] = 0;
					__m256d tmp = _mm256_set1_pd(0.0);
					A = &(ORIGINAL(M, M->swap[i], 0));

					for(j = 0; j < n-4; j += 4)
			 		{
						// tmp[j..j+3] += A[j..j+3] * I[j..j+3]

						__asm__ volatile("# [START] (residue) AVX CHECK");
						tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(A + j), _mm256_loadu_pd(I + j)));
						__asm__ volatile("# [END] (residue) AVX CHECK");
			 		}

					for(; j < n; j++)
						residue[i] -= A[j] * I[j];

					residue[i] = residue[i] - tmp[0] - tmp[1] - tmp[2] - tmp[3];
			 		norm_sum += residue[i] * residue[i];
			 	}

			 	norm_sum -= residue[col] * residue[col];
			 	residue[col] += 1;
			 	norm_sum += residue[col] * residue[col];

			 	LIKWID_MARKER_STOP("op2");
				*mean_residue_timer += (timestamp() - residue_timer);
		 	}





			// LU - Tridiagonal Linear Systems

			//---------------------------------------
			// Linear System Solving
			//---------------------------------------
			//L * y = R

			iter_timer = timestamp();
			LIKWID_MARKER_START("op1");

			{
				for (i = 1; i < n-4; i += 4) {
					double *restrict L0 = &(LOWER(M, i, 0));
					double *restrict L1 = &(LOWER(M, i+1, 0));
					double *restrict L2 = &(LOWER(M, i+2, 0));
					double *restrict L3 = &(LOWER(M, i+3, 0));

					__m256d tmp0 = _mm256_set1_pd(0.0);
					__m256d tmp1 = _mm256_set1_pd(0.0);
					__m256d tmp2 = _mm256_set1_pd(0.0);
					__m256d tmp3 = _mm256_set1_pd(0.0);

					for(j = 0; j < i-4; j += 4)
					{
						// tmpX[0..3] += y[j..j+3] * LX[j..j+3], X = [0..3]

						__asm__ volatile ("# [START] (Ly=b) AVX CHECK");
						tmp0 = _mm256_add_pd(tmp0, _mm256_mul_pd(_mm256_loadu_pd(y + j), _mm256_loadu_pd(L0 + j)));
						tmp1 = _mm256_add_pd(tmp1, _mm256_mul_pd(_mm256_loadu_pd(y + j), _mm256_loadu_pd(L1 + j)));
						tmp2 = _mm256_add_pd(tmp2, _mm256_mul_pd(_mm256_loadu_pd(y + j), _mm256_loadu_pd(L2 + j)));
						tmp3 = _mm256_add_pd(tmp3, _mm256_mul_pd(_mm256_loadu_pd(y + j), _mm256_loadu_pd(L3 + j)));
						__asm__ volatile ("# [END] (Ly=b) AVX CHECK");
					}

					for(; j < i; j++)
					{
						y[i] -= y[j] * L0[j];
						y[i+1] -= y[j] * L1[j];
						y[i+2] -= y[j] * L2[j];
						y[i+3] -= y[j] * L3[j];
					}

					y[i]   = y[i]   - tmp0[0] - tmp0[1] - tmp0[2] - tmp0[3];
					y[i+1] = y[i+1] - tmp1[0] - tmp1[1] - tmp1[2] - tmp1[3];
					y[i+2] = y[i+2] - tmp2[0] - tmp2[1] - tmp2[2] - tmp2[3];
					y[i+3] = y[i+3] - tmp3[0] - tmp3[1] - tmp3[2] - tmp3[3];

					y[i+1] -= y[i] * L1[i];
					y[i+2] -= y[i] * L2[i];
					y[i+3] -= y[i] * L3[i];

					y[i+2] -= y[i+1] * L2[i+1];
					y[i+3] -= y[i+1] * L3[i+1];

					y[i+3] -= y[i+2] * L3[i+2];
				}

				for(; i < n; i++)
				{
					double *restrict L = &(LOWER(M, i, 0));

					__m256d tmp = _mm256_set1_pd(0.0);

					for(j = 0; j < i-4; j += 4)
					{
						// tmp[0..3] += y[j..j+3] * L[j..j+3]
						
						__asm__ volatile ("# [START] (Ly=b) AVX CHECK");
						tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(y + j), _mm256_loadu_pd(L + j)));
						__asm__ volatile ("# [END] (Ly=b) AVX CHECK");
					}

					for(; j < i; j++)
						y[i] -= y[j] * L[j];

					y[i] = y[i] - tmp[0] - tmp[1] - tmp[2] - tmp[3];
				}
			}



			// Solves Ux = Y and sum result to inverse at the same time
			{
				// U * x = Y
				x[n-1] /= UPPER(M, (n-1), (n-1));
				I[n-1] += x[n-1];

				for (i = n-2; i >= 4 ; i -= 4) {
					double *restrict U0 = &(UPPER(M, i-3, 0));
					double *restrict U1 = &(UPPER(M, i-2, 0));
					double *restrict U2 = &(UPPER(M, i-1, 0));
					double *restrict U3 = &(UPPER(M, i, 0));

					__m256d tmp0 = _mm256_set1_pd(0.0);
					__m256d tmp1 = _mm256_set1_pd(0.0);
					__m256d tmp2 = _mm256_set1_pd(0.0);
					__m256d tmp3 = _mm256_set1_pd(0.0);

				 	for(j = i+1; j < n-4; j += 4)
					{
						__asm__ volatile ("# [START] (Ux=y) AVX CHECK");
						tmp0 = _mm256_add_pd(tmp0, _mm256_mul_pd(_mm256_loadu_pd(x + j), _mm256_loadu_pd(U0 + j)));
						tmp1 = _mm256_add_pd(tmp1, _mm256_mul_pd(_mm256_loadu_pd(x + j), _mm256_loadu_pd(U1 + j)));
						tmp2 = _mm256_add_pd(tmp2, _mm256_mul_pd(_mm256_loadu_pd(x + j), _mm256_loadu_pd(U2 + j)));
						tmp3 = _mm256_add_pd(tmp3, _mm256_mul_pd(_mm256_loadu_pd(x + j), _mm256_loadu_pd(U3 + j)));
						__asm__ volatile ("# [END] (Ux=y) AVX CHECK");
					}

					for(; j < n; j++)
					{
						x[i-3] -= x[j] * U0[j];
						x[i-2] -= x[j] * U1[j];
						x[i-1] -= x[j] * U2[j];
						x[i] -= x[j] * U3[j];
					}

					x[i-3] = x[i-3] - tmp0[0] - tmp0[1] - tmp0[2] - tmp0[3];
					x[i-2] = x[i-2] - tmp1[0] - tmp1[1] - tmp1[2] - tmp1[3];
					x[i-1] = x[i-1] - tmp2[0] - tmp2[1] - tmp2[2] - tmp2[3];
					x[i] = x[i] - tmp3[0] - tmp3[1] - tmp3[2] - tmp3[3];

				 	x[i] /= U3[i];

				 	x[i-1] -= x[i] * U2[i];
				 	x[i-1] /= U2[i-1];

				 	x[i-2] -= x[i-1] * U1[i-1];
				 	x[i-2] -= x[i] * U1[i];
				 	x[i-2] /= U1[i-2];

					x[i-3] -= x[i-2] * U0[i-2];
					x[i-3] -= x[i-1] * U0[i-1];
					x[i-3] -= x[i] * U0[i];
				 	x[i-3] /= U0[i-3];

				 	I[i-3] += x[i-3];
				 	I[i-2] += x[i-2];
				 	I[i-1] += x[i-1];
				 	I[i] += x[i];
				}

				for(; i >= 0; i--)
				{
					double *restrict U = &(UPPER(M, i, 0));
					__m256d tmp = _mm256_set1_pd(0.0);

					for(j = i+1; j < n-4; j += 4)
					{
						__asm__ volatile ("# [START] (Ux=y) (Remainder) AVX CHECK");
						tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(x + j), _mm256_loadu_pd(U + j)));
						__asm__ volatile ("# [END] (Ux=y) (Remainder) AVX CHECK");
					}

					for(; j < n; j++)
					{
						x[i] -= x[j] * U[j];
					}

					x[i] = x[i] - tmp[0] - tmp[1] - tmp[2] - tmp[3];

					x[i] /= U[i];
					I[i] += x[i];
				}
			}
			LIKWID_MARKER_STOP("op1");
			*mean_iter_timer += timestamp() - iter_timer;

		 } //for(col) end

 		fprintf(output, "# iter %d: <%.17g>\n", iter, sqrt(norm_sum));
	 } // for(iter) end

	 *mean_residue_timer /= (long) iter_n;
	 *mean_iter_timer /= (long) iter_n;

	 return 0;
}




/**
 * @brief      Processes arguments and inverses the received matrix
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     Main function. Always returns 0.
 */
int main(int argc, char **argv)
{
	LIKWID_MARKER_INIT;

	// Status to return
	int status;

	// Timers (as they must be printed in the end)
	long lu_timer = 0, mean_iter_timer = 0, mean_residue_timer = 0;

	// Matrixes
	invmat_t *M;

	// Sets random seed as specified
	srand(20172);

	// Arguments struct
	arguments_t args;

	// Reads command-line (actually argv)
	get_args(&args, argc, argv);

	M = process_args(args);

	if(!M) return -1;

	// Factorizes matrix through LU decomposition. If matrix isn't inversible or another error occurred, abort.
	lu_timer = timestamp();
	if((status = lu_decomposition(M))) return status;
	lu_timer = timestamp() - lu_timer;

	// Actual inversion of matrix (1 calculation over LU)
	invert_matrix(M);



	// Refines solution
	fprintf(args.output, "#\n");
	refine(M, args.iterations, &mean_residue_timer, &mean_iter_timer, args.output); // Actual refining
	fprintf(args.output, "# Tempo LU: %.3gs\n", (double) lu_timer/1e3); // Time spent in LU decomposition
	fprintf(args.output, "# Tempo iter: %.3gs\n", (double) mean_iter_timer/1e3); // Mean time spent solving lar system during refinement
	fprintf(args.output, "# Tempo Residuo: %.3gs\n#\n", (double) mean_residue_timer/1e3); // Mean time spent calculating residues

	// print_lu(M, args.output);
	// mult_lu(M, args.output);
	// print_inverse(M, args.output);

	invmat_free(M);
	LIKWID_MARKER_CLOSE;

	return 0;
}
