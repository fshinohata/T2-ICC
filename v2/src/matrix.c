#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timer.h"
#include "double_ops.h"
#include "matrix.h"
#include "optimize.h"
#include "immintrin.h"

/**
 * @file matrix.c
 *
 * @author     Fernando Aoyagui Shinohata - GRR20165388
 * @author     Lucas Sampaio Franco - GRR20166836
 *
 * @brief      File contains structures and generic functions and macros to deal
 *             with double-type matrixes
 *
 *             This file contains generic functions and
 *             macros to access optimized matrixes (allocated as one-dimension
 *             arrays), as well as functions to factorize'em through LU
 *             decomposition.\n To access the matrixes, you should use the macro
 *             ORIGINAL().\n
 */



/**
 * @brief      Factorizes matrix A into matrixes L (Lower) and U (Upper)
 *             through Gauss Elimination
 *
 * @param      M     The invmat_t pointer
 *
 * @return     Error values (0 for successfull)
 */
int lu_decomposition(invmat_t *M)
{
    int *swap = M->swap;

    double *restrict A = (double *) malloc (M->N * M->n * sizeof(double));

    if(!A)
    {
        fprintf(stderr, "ERROR: in lu_decomposition(): Could not allocate memory for copy matrix\n");
        return -1;
    }

    for(int i = 0; i < M->n; i++)
      memcpy((A + M->N * i), &(ORIGINAL(M, i, 0)), M->n * sizeof(double));

    // From first to last line,
    for(int l = 0; l < M->n; l++)
    {
        // Get new pivot
        int pivot = l;
        for(int search = l+1; search < M->n; search++)
        {
            if(DBL_ABS(A[M->N*swap[pivot]+l]) < DBL_ABS(A[M->N*swap[search]+l]))
                pivot = search;
        }

        // If pivot is zero, matrix is not inversible
        if(DBL_EQZERO(A[M->N*swap[pivot]+l]))
        {
            fprintf(stderr, "ERROR: lu_decomposition(): Received matrix is not inversible.\n");
            return __MATRIX_ERROR_NOT_INVERSIBLE;
        }

        // Line exchange
        if(pivot != l)
        {
            double d;

            {
                double *restrict Ll = &LOWER(M, l, 0); // Line l of L
                double *restrict Lp = &LOWER(M, pivot, 0); // Line 'pivot' of L

                for(int c = 0; c < l; c++)
                {
                    d = *Ll;
                    *Ll++ = *Lp;
                    *Lp++ = d;
                }
            }

            // Swap lines
            swap[pivot] = swap[pivot] ^ swap[l];
            swap[l]     = swap[pivot] ^ swap[l];
            swap[pivot] = swap[pivot] ^ swap[l];
        }

        // k goes down the next lines
        {
            // double Ull = UPPER(M, l, l); // Ull = U[l][l]
            double *restrict Al = (A + M->N * swap[l]);
            for(int k = l+1; k < M->n; k++)
            {
                double *restrict Ak = (A + M->N * swap[k]);
                double m = Ak[l] / Al[l];

                int c;
                for(c = l+1; c < M->n - 4; c += 4)
                {
                  // Ak[c..c+3] -= Al[c..c+3] * m ;
                  
                  __asm__ volatile ("# [START] AVX CHECK");
                  _mm256_storeu_pd(Ak + c, _mm256_sub_pd(_mm256_loadu_pd(Ak + c), _mm256_mul_pd(_mm256_loadu_pd(Al + c), _mm256_set1_pd(m)) ));
                  __asm__ volatile ("# [END] AVX CHECK");
                }

                for(; c < M->n; c++)
                {
                  Ak[c] -= Al[c] * m;
                }

                LOWER(M, k, l) = m;
            }
        }
    }

    for(int i = 0; i < M->n; i++)
    {
        double *restrict U = &(UPPER(M, i, 0));
        double *restrict B = (A + M->N * swap[i]);
        for(int j = i; j < M->n; j++)
            U[j] = B[j];
    }

    free(A);

    return 0;
}

/**
 * @brief      Multiplies L * U
 *
 * @param      M       The invmat_t pointer
 * @param      output  The output fd
 */
void mult_lu(invmat_t *M, FILE *output)
{
    int i, j, k;
    double m;

    for(i = 0; i < M->n; i++)
    {
        for(j = 0; j < M->n; j++)
        {
            m = 0;

            for(k = 0; (k < i)&&(k <= j); k++)
            {
                m += LOWER(M, i, k) *  UPPER(M, k, j);
            }
            m += UPPER(M, k, j);
            printf("%.17g ", m);
        }
        printf("\n");
    }
}

/**
 * @brief      Multiplies A * A⁻¹
 *
 * @param      M       The invmat_t pointer
 * @param      output  The output fd
 */
void mult_inverse(invmat_t *M, FILE *output)
{
    int i, j, k;
    double m;
    for(i = 0; i < M->n; i++)
    {
        for(j = 0; j < M->n; j++)
        {
            m = 0;
            for(k = 0; k < M->n; k++)
            {
                m += ORIGINAL(M, i, k) * INVERSE(M, k, j);
            }
            fprintf(output, "%.17g ", m);
        }
        fprintf(output, "\n");
    }
}

/**
 * @brief      Generates a NxN matrix of double-type elements, optimized in an array-form
 *
 * @param[in]  n     Size of the matrix to be generated (must be > 1)
 *
 * @return     Double-type vector of size N*N, containing the random-matrix
 */
void square_matrix_generate(invmat_t *M)
{
    if(!M || !M->A)
        return;

    int n = M->n;

    if(n <= 1)
    {
        fprintf(stderr, "ERROR: in square_matrix_generate(): size parameter must be higher than 1.\n");
        return;
    }

    /* generate a randomly initialized matrix in row-major order */
    double invRandMax = 1.0/(double)RAND_MAX;

    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            ORIGINAL(M, i, j) = (double) rand() * invRandMax;
}

/**
 * @brief      Prints matrix A
 * user 0m3.280s
 * @param      M       The invmat_t pointer
 * @param      output  The output FILE *
 */
void print_original(invmat_t *M, FILE *output)
{
    fprintf(output, "%d\n", M->n);

    for(int i = 0; i < M->n; i++)
    {
        for(int j = 0; j < M->n; j++)
            fprintf(output, "%.17g ", ORIGINAL(M, i, j));
        fprintf(output, "\n");
    }
}

/**
 * @brief      Prints A⁻¹ (I)
 *
 * @param      inv     The inv
 * @param      output  The output
 */
void print_inverse(invmat_t *M, FILE *output)
{
    fprintf(output, "%d\n", M->n);

    for(int i = 0; i < M->n; i++)
    {
        for(int j = 0; j < M->n; j++)
            fprintf(output, "%.17g ", INVERSE(M, i, j));
        fprintf(output, "\n");
    }
}

/**
 * @brief      Prints L and U
 *
 * @param      M       The invmat_t pointer
 * @param      output  The output
 */
void print_lu(invmat_t *M, FILE *output)
{
    int i, j;
    fprintf(output, "L:\n");
    for(i = 0; i < M->n; i++)
    {
        for(j = 0; j <= i; j++)
            fprintf(output, "%.17g ", LOWER(M, i, j));
        for(; j < M->n; j++)
            fprintf(output, "0 ");
        fprintf(output, "\n");
    }
    fprintf(output, "\n");

    fprintf(output, "U:\n");
    for(i = 0; i < M->n; i++)
    {
        for(j = 0; j < i; j++)
            fprintf(output, "0 ");
        for(; j < M->n; j++)
            fprintf(output, "%.17g ", UPPER(M, i, j));
        fprintf(output, "\n");
    }
    fprintf(output, "\n");
}

/**
 * @brief      Initializes a invmat_t pointer by preparing a struct and
 *             returning it's pointer
 *
 * @param[in]  n     Size of all matrixes (No. rows/columns)
 *
 * @return     A struct invmat_t pointer properly initialized
 */
invmat_t *invmat_init(long n)
{
    invmat_t *M = (invmat_t *) malloc (sizeof(invmat_t));

    if(!M)
    {
        fprintf(stderr, "ERROR: in invmat_init(): Could not allocate struct.\n");
        return NULL;
    }

    M->A = NULL;
    M->L = NULL;
    M->U = NULL;
    M->I = NULL;
    M->swap = NULL;

    M->n = n;
    M->N = CACHE_ALIGN(n);

    CACHE_ALIGNED_MALLOC(M->A, M->N * n * sizeof(double));
    CACHE_ALIGNED_MALLOC(M->L, M->N * n * sizeof(double));
    // CACHE_ALIGNED_MALLOC(M->U, M->N * n * sizeof(double));
    M->U = M->L;
    CACHE_ALIGNED_MALLOC(M->I, M->N * n * sizeof(double));
    CACHE_ALIGNED_MALLOC(M->swap, M->N * sizeof(int));

    if(!M->A || !M->I || !M->L || !M->U || !M->swap)
    {
        fprintf(stderr, "ERROR: in invmat_init(): Could not allocate matrixes for size %ld.\n", n);
        free(M);
        return NULL;
    }

    for(int i = 0; i < n; i++)
        M->swap[i] = i;

    return M;
}

/**
 * @brief      Frees a invmat_t pointer properly
 *
 * @param      matrix  The invmat_t pointer
 */
void invmat_free(invmat_t *M)
{
    if(!M)
        return;

    if(M->A)
    {
        free(M->A);
    }

    if(M->swap)
    {
        free(M->swap);
    }

    free(M);
}
