# !/bin/bash

# CPU=$(cat /proc/cpuinfo | grep -m 1 'model name' | cut -d : -f 2 | sed 's/ /_/g' | sed -r 's/^_//g')
CPU="Intel(R)_Core(TM)_i7-4770_CPU_@_3.40GHz"
VERSIONS=(v2 v1)
MATRIXES=(32 33 64 65 128 129 256 257 512 1000 2000)
TIME_OUTPUT="data/TIME_${CPU}.csv"
DP_OUTPUT="data/DP_${CPU}.csv"
MEM_OUTPUT="data/MEM_${CPU}.csv"
CACHE_OUTPUT="data/CACHE_${CPU}.csv"

if [ -f $TIME_OUTPUT ];
then
	rm $TIME_OUTPUT
fi;

if [ -f $DP_OUTPUT ];
then
	rm $DP_OUTPUT
fi;

if [ -f $MEM_OUTPUT ];
then
	rm $MEM_OUTPUT
fi;

if [ -f $CACHE_OUTPUT ];
then
	rm $CACHE_OUTPUT
fi;

# TIME OUTPUT
printf ",v1_op1,v1_op2,v2_op1,v2_op2\n" >> $TIME_OUTPUT

for n in ${MATRIXES[@]};
do
	printf "N: $n," >> $TIME_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L2CACHE.txt | grep -A 3 "N: $n" | grep "Tempo iter" | cut -d : -f 2 | sed 's/s/,/g' | xargs printf >> $TIME_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L2CACHE.txt | grep -A 3 "N: $n" | grep "Tempo Residuo" | cut -d : -f 2 | sed 's/s/,/g' | xargs printf >> $TIME_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L2CACHE.txt | grep -A 3 "N: $n" | grep "Tempo iter" | cut -d : -f 2 | sed 's/s/,/g' | xargs printf >> $TIME_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L2CACHE.txt | grep -A 3 "N: $n" | grep "Tempo Residuo" | cut -d : -f 2 | sed 's/s//g' | xargs printf >> $TIME_OUTPUT
	printf "\n" >> $TIME_OUTPUT
done;

# DP OUTPUT
printf ",v1_op1,v1_op2,v2_op1,v2_op2\n" >> $DP_OUTPUT

for n in ${MATRIXES[@]};
do
	printf "N: $n," >> $DP_OUTPUT
	cat ./data/benchmark/v1_${CPU}_FLOPS_AVX.txt | grep -A 7 "N: $n" | grep "DP MFLOP" | head -n 1 | cut -d '|' -f 3 |  xargs printf >> $DP_OUTPUT
	printf "," >> $DP_OUTPUT
	cat ./data/benchmark/v1_${CPU}_FLOPS_AVX.txt | grep -A 7 "N: $n" | grep "DP MFLOP" | tail -n 1 | cut -d '|' -f 3 |  xargs printf >> $DP_OUTPUT
	printf "," >> $DP_OUTPUT
	cat ./data/benchmark/v2_${CPU}_FLOPS_AVX.txt | grep -A 7 "N: $n" | grep "DP MFLOP" | head -n 1 | cut -d '|' -f 3 | xargs printf >> $DP_OUTPUT
	printf "," >> $DP_OUTPUT
	cat ./data/benchmark/v2_${CPU}_FLOPS_AVX.txt | grep -A 7 "N: $n" | grep "DP MFLOP" | tail -n 1 | cut -d '|' -f 3 | xargs printf >> $DP_OUTPUT

	printf "\n" >> $DP_OUTPUT
done;

# CACHE OUTPUT
printf ",v1_op1,v1_op2,v2_op1,v2_op2\n" >> $CACHE_OUTPUT

for n in ${MATRIXES[@]};
do
	printf "N: $n," >> $CACHE_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L2CACHE.txt | grep -A 7 "N: $n" | grep "miss ratio" | head -n 1 | cut -d '|' -f 3 |  xargs printf >> $CACHE_OUTPUT
	printf "," >> $CACHE_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L2CACHE.txt | grep -A 7 "N: $n" | grep "miss ratio" | tail -n 1 | cut -d '|' -f 3 |  xargs printf >> $CACHE_OUTPUT
	printf "," >> $CACHE_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L2CACHE.txt | grep -A 7 "N: $n" | grep "miss ratio" | head -n 1 | cut -d '|' -f 3 | xargs printf >> $CACHE_OUTPUT
	printf "," >> $CACHE_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L2CACHE.txt | grep -A 7 "N: $n" | grep "miss ratio" | tail -n 1 | cut -d '|' -f 3 | xargs printf >> $CACHE_OUTPUT

	printf "\n" >> $CACHE_OUTPUT
done;

# DP OUTPUT
printf ",v1_op1,v1_op2,v2_op1,v2_op2\n" >> $MEM_OUTPUT

for n in ${MATRIXES[@]};
do
	printf "N: $n," >> $MEM_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L3.txt | grep -A 7 "N: $n" | grep "L3 bandwidth" | head -n 1 | cut -d '|' -f 3 |  xargs printf >> $MEM_OUTPUT
	printf "," >> $MEM_OUTPUT
	cat ./data/benchmark/v1_${CPU}_L3.txt | grep -A 7 "N: $n" | grep "L3 bandwidth" | tail -n 1 | cut -d '|' -f 3 |  xargs printf >> $MEM_OUTPUT
	printf "," >> $MEM_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L3.txt | grep -A 7 "N: $n" | grep "L3 bandwidth" | head -n 1 | cut -d '|' -f 3 | xargs printf >> $MEM_OUTPUT
	printf "," >> $MEM_OUTPUT
	cat ./data/benchmark/v2_${CPU}_L3.txt | grep -A 7 "N: $n" | grep "L3 bandwidth" | tail -n 1 | cut -d '|' -f 3 | xargs printf >> $MEM_OUTPUT

	printf "\n" >> $MEM_OUTPUT
done;
