# Trabalho de Introdução à Computação Científica

Período: 2017-2  
Docente: Daniel Weingaertner  
Descrição do trabalho: otimizar a primeira versão do inversor de matrizes, utilizando conceitos vistos em sala de aula. Comparações em gráficos e explicações devem ser colocadas e entregues em um relatório em formato de trabalho acadêmico.

## Autores

Fernando Aoyagui Shinohata -- GRR20165388  
Lucas Sampaio Franco -- GRR20166836

## Estrutura dos arquivos

Pastas:

- `data/` := Contém os dados dos testes realizados e arquivos compilados em `.s` para verificação. Alguns arquivos `.csv` com o resumo dos dados por categorias também estão nesta pasta.
- `relatorio/` := Contém os arquivos do relatório em `.tex`, incluindo imagens.
- `v1/` := Contém a versão 1 do trabalho.
- `v2/` := Contém a versão 2 do trabalho.

Arquivos do diretório `/`:

- `test_battery.sh` := Executa a bateria de testes da especificação do trabalho. Este *script* não roda os testes caso já tenham sido feitos para a CPU. Se quiser forçar a execução, utilize a opção `-f` no **primeiro argumento da chamada**. O *script* cria arquivos nas pastas `data/benchmark/` e `data/compiled/`. Os grupos de testes do LIKWID devem ser pré-configurados no *script*.
- `compare.sh` := Deve ser executado após o *script* `test_battery.sh`. Este *script* resume as saídas da bateria de testes em arquivos `.csv`. Os grupos de testes do LIKWID devem ser pré-configurados no *script*.
- `make_doc.sh` := Compila os arquivos da pasta `relatorio/` para um `.pdf` com mesmo nome. Este conjunto de comandos utiliza do programa `latexmk`, que **não gera numeração de seções no arquivo final**.