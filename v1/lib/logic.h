#ifndef __LOGICAL_OPERATIONS__
#define __LOGICAL_OPERATIONS__

/**
 * @file logic.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief File contains logical fast-paced operations, like variable value-swapping.
 * 
 */

/** @def VALUE_SWAP
 *
 * @brief      Swaps 2 variables using intel XCHG instruction
 *
 * @param      X     One variable
 * @param      Y     The other variable
 *
 */
#ifndef VALUE_SWAP
	#define VALUE_SWAP(X, Y) __asm__ volatile ("XCHG %1, %2\n\t" : "=r" (X) : "r" (X), "r" (Y) );
#else
	#warning "Macro VALUE_SWAP already defined. Errors may occur."
#endif

#endif