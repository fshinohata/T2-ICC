#ifndef __OPTIMIZED_MATRIX_OPERATIONS__
#define __OPTIMIZED_MATRIX_OPERATIONS__

#include <stdio.h>

/**
 * @file matrix.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief File with definitions of macros and functions of matrix.c
 * 
 * @see matrix.c
 */

/**
 * @brief Struct that holds general square matrixes
 *
 * @see matrix.c
 */
typedef struct matrix_t {
	double *elem; /**< Matrix elements, represented as an one-dimension array */
	long memory; /**< Total memory used for the matrix */
	int size; /**< Number of rows/columns of the matrix */
	int *actual_line_pos; /**< Original matrix A line swaps. Can be used to reorder matrixes generated by L and U. */
} matrix_t;

/**
 * @brief Struct that holds matrixes L and U, represented as sparse matrixes
 * 
 * @see matrix.c
 */
typedef struct lu_matrix_t {
	double *L; /**< Sparse matrix L */
	double *U; /**< Sparse matrix U */
	int size;  /**< Number of rows/columns for both matrixes */
	int *actual_line_pos; /**< Actual position of original lines of matrix LU (after multiplying) */
} lu_matrix_t;


/** @def LOWER_ELEM
 * @brief      Macro-function to access L matrix (represented as a sparse matrix of ((n+1)*n)/2 entries)
 *
 * The expression for obtaining the correct index was calculated as follows (Nth stands for Nth line):
 *   > - The first line of the matrix L has 1 element (which is 1), with 0 elements before itself;\n
 *   > - The second line, the same way, has 2 elements, with (0 + 1) element before itself;\n
 *   > - The third line, the same way, has 3 elements, with (0 + 1 + 2) = 3 elements before itself;\n
 *   > <b>[...]</b>\n
 *   > - The Nth line, the same way, has N elements, with (0 + 1 + 2 + ... + (N-1)) = (Nth * (Nth+1))/2 elements before itself;
 *
 * @param      lu_matrix  The lu_matrix_t pointer (allocated as a vector)
 * @param      i       Line to access
 * @param      j       Column to access
 *
 * @return     Matrix L value at position (i,j)
 */
#ifndef LOWER_ELEM
	#define LOWER_ELEM(lu_matrix, i, j) (lu_matrix)->L[(((i)*(i+1))/2)+j]
#else
	#error "Macro LOWER_ELEM already defined. Aborting."
#endif


/** @def UPPER_ELEM
 * @brief      Macro-function to access U matrix (represented as a sparse matrix of ((n+1)*n)/2 entries)
 *
 * The expression for obtaining the correct index was calculated as follows (Nth stands for Nth line):
 *   > - The first line of the matrix U has N elements, with 0 elements before itself;\n
 *   > - The second line, the same way, has (N-1) elements, with (N) elements before itself;\n
 *   > - The third line, the same way, has (N-2) elements, with (N + (N-1)) elements before itself;\n
 *   > <b>[...]</b>\n
 *   > - The Nth line, the same way, has 1 element, with (N + (N-1) + (N-2) + ... + 2) = ((N * Nth) - ( (Nth - 1)*Nth )/2 - Nth) elements before itself;
 *
 * @param      lu_matrix  The lu_matrix_t pointer (allocated as a vector)
 * @param      i       Line to access
 * @param      j       Column to access
 *
 * @return     Matrix L value at position (i,j)
 */
#ifndef UPPER_ELEM
	#define UPPER_ELEM(lu_matrix, i, j) (lu_matrix)->U[((lu_matrix)->size*(i)) - ( (i-1)*(i) )/2 + ((j) - (i))]
#else
	#error "Macro UPPER_ELEM already defined. Aborting."
#endif

/** @def MATRIX_ELEM
 * @brief      Macro-function to access a regular matrix (represented as a vector)
 * 
 * The matrix's mapped line-by-line, as regular matrixes are often used that way.
 *
 * @param      matrix  The matrix_t struct pointer
 * @param      i       Line to access
 * @param      j       Column to access
 *
 * @return     Matrix value at position (i,j)
 */
#ifndef MATRIX_ELEM
	#define MATRIX_ELEM(matrix, i, j) matrix->elem[((i)*matrix->size)+j]
#else
	#error "Macro MATRIX_ELEM already defined. Aborting."
#endif

/** @def INVERSE_ELEM
 * @brief      Macro-function to access a inverse matrix (represented as a vector)
 * 
 * Difference? The inverse matrix's mapped column-by-column, as it's used that way.
 *
 * @param      matrix  The matrix_t struct pointer
 * @param      i       Line to access
 * @param      j       Column to access
 *
 * @return     Matrix value at position (i,j)
 */
#ifndef INVERSE_ELEM
	#define INVERSE_ELEM(matrix, i, j) matrix->elem[((j)*matrix->size)+i]
#else
	#error "Macro INVERSE_ELEM already defined. Aborting."
#endif

/**
 * @brief      Default error value to return on process termination by function lu_decomposition().
 * 
 * Set as the 6th error for executable 'invmat'.\n
 * Error is set 32 as args.h defines other 5 errors with values 1, 2, 4, 8 and 16.
 *
 * @see matrix.c
 * @see args.h
 */
#ifndef __MATRIX_ERROR_NOT_INVERSIBLE
	#define __MATRIX_ERROR_NOT_INVERSIBLE 32
#else
	#warning "Constant __MATRIX_ERROR_NOT_INVERSIBLE already defined. Errors may occur."
#endif

int lu_decomposition(matrix_t *A, lu_matrix_t **holder, long *timer);

matrix_t *square_matrix_init(int size);
lu_matrix_t *lu_matrix_init(int size);

void free_matrix(matrix_t *matrix);
void free_lu_matrix(lu_matrix_t *matrix);

double *square_matrix_generate(long n);
matrix_t *matrix_copy(matrix_t *m);
void print_matrix(matrix_t *m, FILE *out);
void print_inverse(matrix_t *inv, FILE *out);

#endif
