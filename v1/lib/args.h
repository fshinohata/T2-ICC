#ifndef __INVMAT_COMMAND_LINE__
#define __INVMAT_COMMAND_LINE__

#include "matrix.h"

/**
 * @file args.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief File with definitions of macros and functions of args.c
 * 
 * @see args.c
 */

/**
 * @brief Struct that holds the necessary arguments for invmat.c
 *             
 * @see args.c
 * @see invmat.c
 */
typedef struct arguments_t {
	FILE *input, /**< Input channel. Defaults as <i>stdin</i>. */
		 *output; /**< Output channel. Defaults as <i>stdout</i>. */
	long iterations, /**< Number of refinement iterations to run. */
		 randomize_size; /**< Size of the random matrix to generate. Defaults as zero. If zero, reads the matrix from the input channel. */
} arguments_t;


/** @def __ARGS_ERROR_ITERATIONS_NOT_SPECIFIED
 *
 *	@brief Default error value for nonexistent iteration count option in argv
 *             
 *	@see invmat.c
 */
#ifndef __ARGS_ERROR_ITERATIONS_NOT_SPECIFIED
	#define __ARGS_ERROR_ITERATIONS_NOT_SPECIFIED 1
#else
	#warning "Constant __ARGS_ERROR_ITERATIONS_NOT_SPECIFIED already defined. Errors may occur."
#endif


/** @def __ARGS_ERROR_OPENING_INPUT_FILE
 * 
 *	@brief Default error value for any errors upon opening input file
 *             
 *	@see invmat.c
 */
#ifndef __ARGS_ERROR_OPENING_INPUT_FILE
	#define __ARGS_ERROR_OPENING_INPUT_FILE 2
#else
	#warning "Constant __ARGS_ERROR_OPENING_INPUT_FILE already defined. Errors may occur."
#endif


/** @def __ARGS_ERROR_OPENING_OUTPUT_FILE
 *
 *	@brief Default error value for any errors upon opening output file
 */
#ifndef __ARGS_ERROR_OPENING_OUTPUT_FILE
	#define __ARGS_ERROR_OPENING_OUTPUT_FILE 4
#else
	#warning "Constant __ARGS_ERROR_OPENING_OUTPUT_FILE already defined. Errors may occur."
#endif


/** @def __ARGS_ERROR_INVALID_RANDOM
 *
 *	@brief Default error value for invalid number/value specified by option -r
 */
#ifndef __ARGS_ERROR_INVALID_RANDOM
	#define __ARGS_ERROR_INVALID_RANDOM 8
#else
	#warning "Constant __ARGS_ERROR_INVALID_RANDOM already defined. Errors may occur."
#endif


/** @def __ARGS_ERROR_INVALID_ITERATION_COUNT
 *
 *	@brief Default error value for invalid number/value of iterations specified by option -i
 */
#ifndef __ARGS_ERROR_INVALID_ITERATION_COUNT
	#define __ARGS_ERROR_INVALID_ITERATION_COUNT 16
#else
	#warning "Constant __ARGS_ERROR_INVALID_ITERATION_COUNT already defined. Errors may occur."
#endif


void get_args(arguments_t *args, int argc, char **argv);
matrix_t *process_args(arguments_t args);

#endif
