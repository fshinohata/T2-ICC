#ifndef __FLOAT_OPERATIONS__
#define __FLOAT_OPERATIONS__

#include <float.h>

/**
 * @file double_ops.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief File with definitions and macros to deal with double-type variable operations
 * 
 * This file contains:
 *   * Kahan summation macro that should be used as follows:\n
 *     * Call of KAHAN_SUM_START() before starting actual summation (single-call);
 *     * Call of KAHAN_SUM(INPUT_TO_SUM) with a single input per call (multiple-calls);
 *     * Call of KAHAN_SUM_FINISH(VAR_TO_SUM_RESULT) at the end (single-call);
 *     * <b>WARNING:</b> KAHAN_SUM_FINISH(VAR) executes VAR += SUMMATION! It <b>does not</b> overwrite the result to the variable VAR!
 *     
 *   * Sequence of double-type operation macros and defines, all of them with the prefix "DBL_". The operations/definitions are as follows:
 *     * DBL_ZERO: 'Zero' value calculated for double-type variables;
 *     * DBL_OP_ERROR_MARGIN: Error/tolerance margin for comparison of double-type variables;
 *     * DBL_[ EQ|LT|LE|GT|GE ]ZERO(var): Comparison operations of 'var' and 'zero';
 *     * DBL_MAX(x, y): Returns biggest number between x and y;
 *     * DBL_ABS(x): Returns absolute value of x;
 *     * DBL_EQUAL(x, y): Compares if the two double-type variables 'x' and 'y' are 'equal', with the error margin defined in DBL_OP_ERROR_MARGIN;
 */

//##########################################################
// KAHAN SUMMATION DEFINITIONS
//##########################################################

// static double __kahan_double_final_sum, __kahan_double_sum_error, __kahan_double_temporary, __kahan_double_error_compensation;

/** @def KAHAN_SUM_INIT
 *
 *	@brief Creates local variables needed for Kahan Summation Algorithm. Must be called once per function that wants to use it.
 */
#ifndef KAHAN_SUM_INIT
	#define KAHAN_SUM_INIT() double __kahan_double_final_sum, __kahan_double_sum_error, __kahan_double_temporary, __kahan_double_error_compensation;
#else
	#warning "Constant KAHAN_SUM_INIT already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** @def KAHAN_SUM_START
 *
 * @brief Prepares variables to start a new Kahan Summation
 */
#ifndef KAHAN_SUM_START
	#define KAHAN_SUM_START() { __kahan_double_final_sum = 0; __kahan_double_sum_error = 0; }
#else
	#warning "Macro KAHAN_SUM_START already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** @def KAHAN_SUM
 *
 * @brief Adds the input to the summation accumulator variable
 */
#ifndef KAHAN_SUM
	#define KAHAN_SUM(INPUT_TO_SUM) {\
	__kahan_double_error_compensation = (INPUT_TO_SUM) - __kahan_double_sum_error;\
	__kahan_double_temporary = __kahan_double_final_sum + __kahan_double_error_compensation;\
	__kahan_double_sum_error = (__kahan_double_temporary - __kahan_double_final_sum) - __kahan_double_error_compensation;\
	__kahan_double_final_sum = __kahan_double_temporary;\
	}
#else
	#warning "Macro KAHAN_SUM already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** @def KAHAN_SUM_FINISH
 * 
 * @brief Ends Kahan Summation by adding the accumulated sum to the variable received
 */
#ifndef KAHAN_SUM_FINISH
	#define KAHAN_SUM_FINISH(VAR_TO_SUM_RESULT) { VAR_TO_SUM_RESULT += __kahan_double_final_sum; }
#else
	#warning "Macro KAHAN_SUM_FINISH already defined. Errors may occur."
#endif



//##########################################################
// FLOAT OPERATIONS DEFINITIONS
//##########################################################

//------------------------------------------------------------------
/**
 * @brief Zero definition for float-type comparations
 */
#ifndef DBL_ZERO
	#define DBL_ZERO 0.0000000001
#else
	#warning "Constant DBL_ZERO already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Returns float X's sign bit
 */
#ifndef DBL_SIGN
	#define DBL_SIGN(X) ((double_union_t) X).sign
#else
	#warning "Macro DBL_SIGN already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Returns float X's exponent (8 bits)
 */
#ifndef DBL_EXPONENT
	#define DBL_EXPONENT(X) ((double_union_t) X).exponent
#else
	#warning "Macro DBL_EXPONENT already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Returns float X's mantissa (23 bits)
 */
#ifndef DBL_MANTISSA
	#define DBL_MANTISSA(X) ((double_union_t) X).mantissa
#else
	#warning "Macro DBL_MANTISSA already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Returns absolute value for X
 */
#ifndef DBL_ABS
	#define DBL_ABS(X) ((X) < 0 ? -(X) : (X))
#else
	#warning "Macro DBL_ABS already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Compares if float X is equal to zero
 */
#ifndef DBL_EQZERO
	#define DBL_EQZERO(X) ((X) == 0 || DBL_ABS(X) <= DBL_EPSILON)
#else
	#warning "Macro DBL_EQZERO already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Compares if float X is lesser than zero
 */
#ifndef DBL_LTZERO
	#define DBL_LTZERO(X) ( !DBL_EQZERO(X) && (X) < 0 )
#else
	#warning "Macro DBL_LTZERO already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/**
 * @brief Compares if float X is lesser or equal to zero
 */
#ifndef DBL_LEZERO
	#define DBL_LEZERO(X) (DBL_LTZERO(X) || DBL_EQZERO(X))
#else
	#warning "Macro DBL_LEZERO already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** 
 * @brief Compares if float X is greater than zero
 */
#ifndef DBL_GTZERO
	#define DBL_GTZERO(X) (!DBL_EQZERO(X) && (X) > 0)
#else
	#warning "Macro DBL_GTZERO already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** 
 * @brief Compares if float X is greater or equal to zero
 */
#ifndef DBL_GEZERO
	#define DBL_GEZERO(X) (DBL_GTZERO(X) || DBL_EQZERO(X))
#else
	#warning "Macro DBL_GEZERO already defined. Errors may occur."
#endif


//------------------------------------------------------------------
/* Already exists in float.h 
 * @brief Returns the maximum value between floats X and Y
 */
// #ifndef DBL_MAX
// 	#define DBL_MAX(X, Y) (DBL_ABS(X) >= DBL_ABS(Y) ? X : Y)
// #else
// 	#warning "Macro DBL_MAX already defined. Errors may occur."
// #endif

//------------------------------------------------------------------
/**
 *	@brief Maximum error margin for float comparison
 */
#ifndef DBL_OP_ERROR_MARGIN
	#define DBL_OP_ERROR_MARGIN 0.0000001
#else
	#warning "Constant DBL_SUM_ERROR_MARGIN already defined. Errors may occur."
#endif

//------------------------------------------------------------------
/** 
 *	@brief Returns equality of two floats X and Y received
 */
#ifndef DBL_EQUAL
	#define DBL_EQUAL(X, Y) \
	( X == Y ? 1 : \
		( !DBL_EXPONENT(X) ? ( !DBL_EXPONENT(Y) ? 0 : 0 ) : \
			( !DBL_EXPONENT(Y) ? 0 : \
				( DBL_ABS((X - Y)/DBL_MAX(X, Y)) < DBL_OP_ERROR_MARGIN ? 1 : 0 ) ) ) )
#else
	#warning "Macro DBL_RELATIVE_ERROR already defined. Errors may occur."
#endif



//##########################################################
// FLOAT OPERATIONS STRUCTS AND UNIONS
//##########################################################

// Union to facilitate float-type operations
/**
 * @brief Union that breaks down a double-type variable in it's components
 */
typedef union double_union_t {
  double d; /**< The variable */
  struct {
    unsigned long mantissa:52; /**< The variable's mantissa bits */
    unsigned int exponent:11; /**< The variable's exponent bits */
    unsigned int sign:1; /**< The variable's sign bit */
  }; /**< Unnamed struct */
} double_union_t;

#endif
