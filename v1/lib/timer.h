#ifndef __TIMESTAMPS__
#define __TIMESTAMPS__

#include <sys/time.h>
#include <time.h>

/**
 * @file timer.h
 * 
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 * 
 * @brief Helper file for using the timestamp() function in other files aside from invmat.c
 * 
 * @see lu_decomposition()
 * @see refine()
 *
 */

long timestamp();

#endif