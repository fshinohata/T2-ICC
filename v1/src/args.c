#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"
#include "args.h"

/**
 * @file args.c
 *
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 *
 * @brief File contains the functions that deal with <i>argc</i> and <i>argv</i> and processes their options as needed by invmat.c
 *
 * Arguments:
 *   * -i @<number@> -- Sets the number of refinement iterations as @<number@>. This is <b>obligatory</b>.
 *   * -i @<input_file@> -- Reads the file @<input_file@> instead of <i>stdin</i>. This is <b>optional</b>.
 *   * -r @<number@> -- Generates a random matrix of size @<number@>², instead of reading it from <i>stdin</i>. This is <b>optional</b>.
 *   * -o @<output_file@> -- Writes all results in the file @<output_file@>, instead of <i>stdout</i>. This is <b>optional</b>.
 *
 * @see invmat.c
 */

/**
 * @brief      Checks if string is a number
 *
 * @param      str   The string
 *
 * @return     1 (true) if string is a number, 0 (false) otherwise
 */
int str_is_number(char *str)
{
	if(!str) return 0;
	if(*str == '-' || *str == '+') str++;
	for(;*str;str++)
		if(*str < 48 || *str > 57)
			return 0;
	return 1;
}

/**
 * @brief      Gets the arguments.
 *
 * @param      args  The arguments struct
 * @param[in]  argc  The argc
 * @param      argv  The argv
 */
void get_args(arguments_t *args, int argc, char **argv)
{
	args->input = stdin;
	args->output = stdout;
	args->randomize_size = 0;
	args->iterations = -1;

	for(int i = 1; i < argc-1; i++)
	{
		if(!strcmp(argv[i], "-i"))
		{
			if(str_is_number(argv[i+1]))
			{
				long check = atol(argv[i+1]);
				if(check >= 0)
				{
					args->iterations = (unsigned long) check;
				}
				else
				{
					fprintf(stderr, "Error: refinement iteration count must be >= 0.\n");
					exit(__ARGS_ERROR_INVALID_ITERATION_COUNT);
				}
			}
			else
			{
				fprintf(stderr, "Error: invalid value for option -i (must be a number).");
			}
		}
		else if(!strcmp(argv[i], "-o"))
		{
			args->output = fopen(argv[i+1], "w");
			if(!args->output)
			{
				perror("Error while opening output file");
				exit(__ARGS_ERROR_OPENING_OUTPUT_FILE);
			}
		}
		else if(!strcmp(argv[i], "-r"))
		{
			if(str_is_number(argv[i+1]))
			{
				long check = atol(argv[i+1]);
				if(check > 1 && check < 32768)
					args->randomize_size = check;
				else
				{
					fprintf(stderr, "Error: Random matrix size must be inside the interval 1 < n < 32768\n");
					exit(__ARGS_ERROR_INVALID_RANDOM);
				}
			}
			else
			{
				fprintf(stderr, "Error: Parameter \'-r\' must be followed by a number\n");
				exit(__ARGS_ERROR_INVALID_RANDOM);
			}
		}
		else if(!strcmp(argv[i], "-e"))
		{
			args->input = fopen(argv[i+1], "r");
			if(!args->input)
			{
				perror("Error while opening input file");
				exit(__ARGS_ERROR_OPENING_INPUT_FILE);
			}
		}
	}
	if(args->iterations < 0)
	{
		fprintf(stderr, "Error: You must specify the number of refinement iterations to run!\nUsage: invmat [OPTIONS] -i <number_of_iterations> [OPTIONS]\n");
		exit(__ARGS_ERROR_ITERATIONS_NOT_SPECIFIED);
	}
}

/**
 * @brief      Processes arguments after execution of get_args()
 *
 * @param[in]  args  The arguments
 *
 * @see invmat.c
 * @see get_args()
 * 
 * @return     Allocated and prepared matrix_t pointer, with actual matrix ready with data from stdin/input file
 */
matrix_t *process_args(arguments_t args)
{
	int size;
	matrix_t *matrix;

	if(args.randomize_size)
	{
		matrix = square_matrix_init(args.randomize_size);
		
		if(!matrix)
		{
			fprintf(stderr, "ERROR: in process_args(): could not allocate memory for random matrix. Aborting...\n");
			return NULL;
		}

		free(matrix->elem);
		matrix->elem = square_matrix_generate(args.randomize_size);
		matrix->size = args.randomize_size;


	}
	else
	{
		if(fscanf(args.input, "%d\n", &size) == EOF)
		{
			fprintf(stderr, "ERROR: in process_args(): could not read matrix size. Aborting...\n");
			return NULL;
		}

		matrix = square_matrix_init(size);

		if(!matrix)
		{
			fprintf(stderr, "ERROR: in process_args(): could not allocated memory for matrix_t. Aborting...\n");
			return NULL;
		}

		for(int i = 0; i < matrix->size; i++)
		{
			for(int j = 0; j < matrix->size; j++)
				if(fscanf(args.input, "%lf", &(MATRIX_ELEM(matrix, i, j))) == EOF)
				{
					fprintf(stderr, "ERROR: in process_args(): unexpected EOF in input file. Aborting...\n");
					free(matrix->elem);
					free(matrix);
					return NULL;
				}
		}
	}

	return matrix;
}
