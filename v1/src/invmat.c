#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "timer.h"
#include "matrix.h"
#include "args.h"
#include "double_ops.h"

#include <likwid.h>

/**
 * @file invmat.c
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 *
 * @brief File contains optimized functions and macros to deal with inversion of double-type matrixes
 *
 * This file contains optimized functions to invert matrixes (allocated as vectors) through:
 * 1. LU Decomposition using Gauss Elimination (with partial pivoting);\n
 * 2. Solving the linear systems Ly = I and Ux = y through Gauss-Seidel;
 * 3. Solution refinement using residues.
 */


/**
 * @brief      Inverts received lu_matrix_t matrix <b>factorized by LU
 *             decomposition with swapped lines</b>!
 *
 *             <b>WARNING:</b> Received matrix must be a resulting matrix from
 *             LU decomposition, executed by lu_decomposition()!
 *             
 *             The return will be the inverse matrix of the original matrix, with unswapped lines from LU decomposition.
 *
 * @param      matrix  The lu_matrix_t pointer
 *
 * @see        lu_decomposition()
 *
 * @return     Inverse matrix of original matrix
 */
matrix_t *invert_matrix(lu_matrix_t *matrix)
{
	// Variables for solely decreasing coding length
	int *matline = matrix->actual_line_pos, n = matrix->size;

	// Inverse matrix column-organization
	int *invcols = matline;

	// Inverse matrix
	matrix_t *inv = square_matrix_init(n);

	if(!inv)
	{
		fprintf(stderr, "ERROR: in invert_matrix(): could not allocate memory for inverse matrix. Aborting...\n");
		free(invcols);
		return NULL;
	}

	// Y array to solve both systems (Ly = b) and (Ux = y) (y is also used as x)
	double *y = (double *) malloc (n * sizeof(double));

	if(!y)
	{
		fprintf(stderr, "ERROR: in invert_matrix(): could not allocate memory for residue matrix. Aborting...\n");
		free(invcols);
		free(inv);
		return NULL;
	}

	int line;

	// Calculation of first solution for LU A⁻¹ = I
	for(int identity = 0; identity < n; identity++, invcols++)
	{
		LIKWID_MARKER_START("op1");
		
		//---------------------------------------
		// Ly = b
		//---------------------------------------

		// Preparing solution y
		// Note that we ignore the zeros in the interval [0..identity-1], because we know they are zeroes
		for(line = identity+1, y[line-1] = 1.0; line < n; line++)
		{
			y[line] = 0.0;

			for(int column = identity; column < line; column++)
			{
				y[line] -= y[column] * LOWER_ELEM(matrix, line, column);
			}
		}






		//---------------------------------------
		// Ux = y
		//---------------------------------------
		// Solution y is ready here
		// Note that y is reused as solution vector x here
		// We run through the interval [identity..(n-1)].
		// Before that, we have "trash" that must be treated as zeroes later on
		for(line--; line >= identity; line--)
		{
			INVERSE_ELEM(inv, line, *invcols) = y[line];

			for(int column = n-1; column > line; column--)
			{
				INVERSE_ELEM(inv, line, *invcols) -= INVERSE_ELEM(inv, column, *invcols) * UPPER_ELEM(matrix, line, column);
			}

			INVERSE_ELEM(inv, line, *invcols) /= UPPER_ELEM(matrix, line, line);
		}

		// Here, we consider that everything in the interval [0..identity-1] is a zero
		for(; line >= 0; line--)
		{
			INVERSE_ELEM(inv, line, *invcols) = 0;

			for(int column = n-1; column > line; column--)
			{
				INVERSE_ELEM(inv, line, *invcols) -= INVERSE_ELEM(inv, column, *invcols) * UPPER_ELEM(matrix, line, column);
			}

			INVERSE_ELEM(inv, line, *invcols) /= UPPER_ELEM(matrix, line, line);
		}

		LIKWID_MARKER_STOP("op1");
	}

	return inv;
}





/**
 * @brief      Refines first solution returned from function invert_matrix()
 *
 * @param      inv                 The inverse matrix_t pointer
 * @param      matrix              The Original matrix_t A pointer
 * @param      lu                  LU matrix for solving the linear system
 * @param[in]  iter_n              Number of iterations to run
 * @param      mean_residue_timer  The mean residue timer pointer
 * @param      mean_iter_timer     The mean iterator timer pointer
 *
 * @return     Inverse matrix with refined solution
 */
matrix_t* refine (matrix_t *inv, matrix_t *matrix, lu_matrix_t *lu, int iter_n, long *mean_residue_timer, long *mean_iter_timer, FILE *output)
{	
	// If there are no iterations to run, just stop here
 	if(iter_n == 0) return NULL;

 	// Initializes Kahan Summations variables
 	KAHAN_SUM_INIT();

 	// Time spent accumulators
 	*mean_iter_timer = *mean_residue_timer = 0.0;

 	// Timers for calculating the spent time for a single iteration
 	double residue_timer, iter_timer;

 	// Main dudes
	double *residue = (double *) malloc (matrix->size*sizeof(double)); // Residue array
	double *x = residue; // Coherence var
	double *y = x; // Coherence var

	// Helpers
	int i,j, iter, col, line_pos;
	double norm_sum;

	// Code decreasing matters
	int size = matrix->size;

	 for (iter=1; iter <= iter_n; iter++) {

		 norm_sum = 0;
		 for (col = 0; col < size; col++) { //col desl

		 	//---------------------------------------
		 	// Calculation of residue on column col
		 	//---------------------------------------

		 	residue_timer = timestamp();
		 	LIKWID_MARKER_START("op2");
		 	for(i = 0, line_pos = lu->actual_line_pos[0]; i < col; i++, line_pos = lu->actual_line_pos[i])
		 	{
		 		residue[line_pos] = 0.0;
				 
				KAHAN_SUM_START();
				for (j = 0; j < size; j++) { // A:col, INV:lin
				 	KAHAN_SUM(-(MATRIX_ELEM(matrix, i, j) * INVERSE_ELEM(inv, j, col)));
				}
				KAHAN_SUM_FINISH(residue[line_pos]);

				( !DBL_EQZERO(residue[line_pos]) ? 1 : (residue[line_pos] = 0.0) );
		 	}

		 	residue[line_pos] = 1.0;
				 
			KAHAN_SUM_START();
			for (j = 0; j < size; j++) { // A:col, INV:lin
			 	KAHAN_SUM(-(MATRIX_ELEM(matrix, i, j) * INVERSE_ELEM(inv, j, col)));
			}
			KAHAN_SUM_FINISH(residue[line_pos]);

			( !DBL_EQZERO(residue[line_pos]) ? 1 : (residue[line_pos] = 0.0) );

			for(i++, line_pos = lu->actual_line_pos[i]; i < size; i++, line_pos = lu->actual_line_pos[i])
		 	{
		 		residue[line_pos] = 0.0;
				 
				KAHAN_SUM_START();
				for (j = 0; j < size; j++) { // A:col, INV:lin
				 	KAHAN_SUM(-(MATRIX_ELEM(matrix, i, j) * INVERSE_ELEM(inv, j, col)));
				}
				KAHAN_SUM_FINISH(residue[line_pos]);

				( !DBL_EQZERO(residue[line_pos]) ? 1 : (residue[line_pos] = 0.0) );
		 	}
			*mean_residue_timer += (timestamp() - residue_timer);



			//---------------------------------------
			// Calculation of norm sum for the column
			//---------------------------------------

			KAHAN_SUM_START();
			for (i = 0; i < size; i++) { 
				KAHAN_SUM(residue[i] * residue[i]);
			}
			KAHAN_SUM_FINISH(norm_sum);
			LIKWID_MARKER_STOP("op2");

			


			// LU - Tridiagonal Linear Systems

			//---------------------------------------
			// Linear System Solving
			//---------------------------------------
			//L * y = R
			 
			iter_timer = timestamp();
			LIKWID_MARKER_START("op1");
			for (i = 1; i < size; i++) {
				KAHAN_SUM_START();
				for (j = 0; j != i;  j++) {						
					KAHAN_SUM( -(y[j]*LOWER_ELEM(lu, i, j)) );
				}
				KAHAN_SUM_FINISH(y[i]);
			}
			 
			 // U * x = Y
			 x[size-1] /= UPPER_ELEM(lu, (size-1), (size-1));

			 for (i = size-2; i >=0 ; i--) {
				 KAHAN_SUM_START();
				 for (j = i+1; j < size; j++) {
						KAHAN_SUM( -(UPPER_ELEM(lu, i, j) * x[j]) );
				 }
				 KAHAN_SUM_FINISH(x[i]);
				 x[i] /= UPPER_ELEM(lu, i, i);
			 }
			 *mean_iter_timer += timestamp() - iter_timer;





			 //---------------------------------------
			 // Sum of new result back to inverse matrix
			 //---------------------------------------

			 // INV[col] += x[col]
			 for (i = 0; i < size; i++) { 
				INVERSE_ELEM(inv, i, col) += x[i];
			 }
			 LIKWID_MARKER_STOP("op1");
		 } //for(col) end

 		fprintf(output, "# iter %d: <%.17g>\n", iter, sqrt(norm_sum));
	 } // for(iter) end

	 *mean_residue_timer /= (long) iter_n;
	 *mean_iter_timer /= (long) iter_n;

	 return inv;
}




/**
 * @brief      Processes arguments and inverses the received matrix
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     Main function. Always returns 0.
 */
int main(int argc, char **argv)
{
	LIKWID_MARKER_INIT;

	// Status to return
	int status;

	// Timers (as they must be printed in the end)
	long lu_timer = 0, mean_iter_timer = 0, mean_residue_timer = 0;

	// Matrixes
	matrix_t *matrix, *inverse;
	lu_matrix_t *lu;

	// Sets random seed as specified
	srand(20172);

	// Arguments struct
	arguments_t args;

	// Reads command-line (actually argv)
	get_args(&args, argc, argv);

	// Processes the arguments and try to allocate the matrix with necessary values
	matrix = process_args(args);

	// If couldn't allocate memory, abort
	if(!matrix) return -1;

	// Factorizes matrix through LU decomposition. If matrix isn't inversible or another error occurred, abort.
	if((status = lu_decomposition(matrix, &lu, &lu_timer))) return status;

	// Actual inversion of matrix (1 calculation over LU)
	inverse = invert_matrix(lu);

	// Refines solution
	fprintf(args.output, "#\n");
	refine(inverse, matrix, lu, args.iterations, &mean_residue_timer, &mean_iter_timer, args.output); // Actual refining
	fprintf(args.output, "# Tempo LU: %.3gs\n", (double) lu_timer/1e3); // Time spent in LU decomposition
	fprintf(args.output, "# Tempo iter: %.3gs\n", (double) mean_iter_timer/1e3); // Mean time spent solving linear system during refinement
	fprintf(args.output, "# Tempo Residuo: %.3gs\n#\n", (double) mean_residue_timer/1e3); // Mean time spent calculating residues

	if(!inverse) return -1;

	// Final printing of matrix
	// print_inverse(inverse, args.output);

	// Memory free
	free_matrix(inverse);
	free_matrix(matrix);
	free_lu_matrix(lu);
	
	LIKWID_MARKER_CLOSE;

	// Return no-errors-occurred
	return 0;
}
