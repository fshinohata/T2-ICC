#include "timer.h"

/**
 * @file timer.c
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 *
 * @brief File contains the timestamp() function
 */

/**
 * @brief      Get actual clock time (in ms)
 *
 * @return     Actual clock time in long-type variable
 */
long timestamp(void){
    struct timespec tp;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp);
    return((long)(tp.tv_sec*1.0e3 + tp.tv_nsec/1.0e6));
}