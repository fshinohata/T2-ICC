#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timer.h"
#include "double_ops.h"
#include "matrix.h"
#include "logic.h"

/**
 * @file matrix.c
 * @author Fernando Aoyagui Shinohata - GRR20165388
 * @author Lucas Sampaio Franco - GRR20166836
 *
 * @brief File contains structures and generic functions and macros to deal with double-type matrixes
 *
 * This file contains generic functions and macros to access optimized matrixes (allocated as one-dimension arrays),
 * as well as functions to factorize'em through LU decomposition.\n
 * To access the matrixes, you should use the macro MATRIX_ELEM().\n
 * <b>Note: The matrixes L and U are allocated in a single matrix of size n*n, with n being the original matrix size.</b>
 */

/**
 * @brief      Factorizes matrix A intro matrixes L (Lower) and U (Upper)
 *             through Gauss Elimination
 *
 *             <b>BEWARE:</b> Matrixes U and L aren't "fully related" to the
 *             original matrix A.\n They are actual results of swapping lines of
 *             A during the execution of the Gauss Elimination with Partial
 *             Pivoting process.\n To obtain the original A matrix, it's
 *             necessary to multiply L and U and reorder the new matrix's lines
 *             to their 'original' positions.\n Matrix A is also 'lost' in the
 *             process (it's space is used to store matrixes L and U).
 *
 * @param      matrix  matrix_t pointer to matrix to decompose (square matrix)
 * @param      LU      lu_matrix_t pointer to hold sparse matrixes L and U
 *                     inside structure (matrix A is <b>changed</b>!)
 * @param      timer   The timer pointer (will hold spent time by Gauss Elimination)
 *
 * @return     Error values (0 for successfull)
 */
int lu_decomposition(matrix_t *matrix, lu_matrix_t **LU, long *timer)
{
	int return_status = 0;

	//---------------------------
		#ifdef DEBUG
			int i, j;
			fprintf(__DEBUG_OUTPUT,"Original matrix:\n\n");
			print_matrix(matrix, __DEBUG_OUTPUT);
		#endif
	//---------------------------
	
	// Copies original matrix as it's needed for refining the solution later on
	matrix_t *A = matrix_copy(matrix);

	// Variables
	*LU = lu_matrix_init(A->size);

	if(!(*LU))
	{
		fprintf(stderr, "ERROR: in lu_decomposition(): could not allocate memory for matrixes L and U. Aborting...\n");
		return -1;
	}
	
	int *matline = (*LU)->actual_line_pos, // Matrix 'runner'
		
		n = A->size, // Marks matrix A size
		
		*nptr = matline + n, // Marks end of array matline
		
		*pivot; // Matrix 'runner' too, for partial pivoting

	*timer = timestamp();
	// Line runner
	for(int line = 0; line < n; line++, matline++)
	{
		// Takes actual coefficient (i,j) as pivot
		pivot = matline;

		// Search for an actual pivot
		for(int *pivot_search = matline + 1; pivot_search < nptr; pivot_search++)
		{
			if(DBL_ABS(MATRIX_ELEM(A, *pivot, line)) < DBL_ABS(MATRIX_ELEM(A, *pivot_search, line)))
				pivot = pivot_search;
		}

		// If pivot-element is zero, then the matrix is much likely singular
		if(DBL_EQZERO(MATRIX_ELEM(A, *pivot, line)))
		{
			fprintf(stderr, "WARNING: lu_decomposition(): Received matrix is not inversible.\n");
			return_status = __MATRIX_ERROR_NOT_INVERSIBLE;
		}

		// If pivot was found, swap matrix A lines and update matrix L
		if(pivot != matline)
		{
			//---------------------------
				#ifdef DEBUG
					fprintf(__DEBUG_OUTPUT,"Swapping lines %d with %d\n", *pivot, *matline);
				#endif
			//---------------------------
			
			// Matrix L update
			for(int column = 0; column < line; column++)
			{
				//---------------------------
					#ifdef DEBUG
						fprintf(__DEBUG_OUTPUT,"Swapping L[%d][%d] = %.5f with L[%d][%d] = %.5f\n", line, column, LOWER_ELEM(*LU, line, column), *pivot, column, LOWER_ELEM(*LU, *pivot, column));
					#endif
				//---------------------------
				
				VALUE_SWAP( LOWER_ELEM(*LU, line, column), LOWER_ELEM(*LU, *pivot, column) );
			}
			
			VALUE_SWAP( (*matline), (*pivot) );

			//---------------------------
				#ifdef DEBUG
					fprintf(__DEBUG_OUTPUT,"Matrix lines:\n");
					for(i = 0; i < n; i++)
						fprintf(__DEBUG_OUTPUT,"matline[%d] = %d\n", i, (*LU)->actual_line_pos[i]);
				#endif
			//---------------------------
		}

		// Gauss Elimination for actual line
		for(int *line_to_erase = matline + 1; line_to_erase < nptr; line_to_erase++)
		{
			//---------------------------
				#ifdef DEBUG
					fprintf(__DEBUG_OUTPUT,"Wandering on line %d, column %d\n", *line_to_erase, line);
				#endif
			//---------------------------

			// Multiplicator for erasing
			LOWER_ELEM(*LU, *line_to_erase, line) = (MATRIX_ELEM(A, *line_to_erase, line) / MATRIX_ELEM(A, *matline, line));
		
			//---------------------------
				#ifdef DEBUG
					fprintf(__DEBUG_OUTPUT,"Calculated multiplier L[%d][%d] =  %.5f / %.5f = %.5f\n", *line_to_erase, line, MATRIX_ELEM(A, *line_to_erase, line), MATRIX_ELEM(A, *matline, line), LOWER_ELEM(*LU,*line_to_erase, line));
				#endif
			//---------------------------

			for(int column = line + 1; column < n; column++)
			{
				//---------------------------
					#ifdef DEBUG
						fprintf(__DEBUG_OUTPUT,"Executing operation: m[%d][%d] = %.5f - %.5f * %.5f", *line_to_erase, column, MATRIX_ELEM(A, *line_to_erase, column), MATRIX_ELEM(A, *matline, column), LOWER_ELEM(*LU,*line_to_erase, line));
					#endif
				//---------------------------
				
				MATRIX_ELEM(A, *line_to_erase, column) -= MATRIX_ELEM(A, *matline, column) * LOWER_ELEM(*LU, *line_to_erase, line);

				//---------------------------
					#ifdef DEBUG
						fprintf(__DEBUG_OUTPUT," = %.5f\n", MATRIX_ELEM(A, *line_to_erase, column));
					#endif
				//---------------------------
				
			}

		}
	}

	for(int i = 0, *matline = (*LU)->actual_line_pos; i < n; i++, matline++)
	{
		for(int j = i; j < n; j++)
			UPPER_ELEM(*LU, i, j) = MATRIX_ELEM(A, *matline, j);
	}
	*timer = timestamp() - *timer;

	//-----------------------------------
		#ifdef DEBUG
			fprintf(__DEBUG_OUTPUT,"Resulting L matrix:\n\n");
			for(i = 0; i < n; i++)
			{
				for(j = 0; j <= i; j++)
					fprintf(__DEBUG_OUTPUT,"%.5f\t", LOWER_ELEM(*LU, i, j));
				for(; j < n; j++)
					fprintf(__DEBUG_OUTPUT,"0\t");
				fprintf(__DEBUG_OUTPUT,"\n");
			}
			fprintf(__DEBUG_OUTPUT,"\n\n");

			fprintf(__DEBUG_OUTPUT,"Resulting U matrix:\n\n");
			for(i = 0; i < n; i++)
			{
				for(j = 0; j < i; j++)
					fprintf(__DEBUG_OUTPUT,"0\t");
				for(; j < n; j++)
					fprintf(__DEBUG_OUTPUT,"%.5f\t", UPPER_ELEM(*LU, i, j));
				fprintf(__DEBUG_OUTPUT,"\n");
			}
			fprintf(__DEBUG_OUTPUT,"\n\n");
		#endif
	//-----------------------------------

	//---------------------------
		#ifdef DEBUG
			fprintf(__DEBUG_OUTPUT,"Freeing copy-matrix...\n");
		#endif
	//---------------------------

	free_matrix(A);

	return return_status;
}

/**
 * @brief      Generates a NxN matrix of double-type elements, optimized in an array-form
 *
 * @param[in]  n     Size of the matrix to be generated (must be > 1)
 *
 * @return     Double-type vector of size N*N, containing the random-matrix
 */
double *square_matrix_generate(long n)
{
	if(n <= 1)
	{
		fprintf(stderr, "Error in square_matrix_generate(): size parameter must be higher than 1. Returning NULL...\n");
		return NULL;
	}

	double *mat = NULL;

	/* return NULL if memory allocation fails */
	if ( ! (mat = (double *) malloc(n*n*sizeof(double))) )
	{
		fprintf(stderr, "ERROR: in square_matrix_generate(): could not allocated memory for matrix elements. Aborting...\n");
		return (NULL);
	}

	/* generate a randomly initialized matrix in row-major order */
	double *ptr = mat;
	double *end = mat + n*n;

	double invRandMax = 1.0/(double)RAND_MAX;

	while( ptr != end ) {
	  *ptr++ = (double)rand() * invRandMax;
	}

	return (mat);
}

/**
 * @brief      Prints received matrix_t matrix
 *
 * @param      matrix  The matrix_t pointer
 * @param      output  The output FILE *
 */
void print_matrix(matrix_t *matrix, FILE *output)
{
	fprintf(output, "%d\n", matrix->size);
	
	for(int i = 0; i < matrix->size; i++)
	{
		for(int j = 0; j < matrix->size; j++)
			fprintf(output, "%.17g ", MATRIX_ELEM(matrix, i, j));
		fprintf(output, "\n");
	}
}

/**
 * @brief      Prints received inverse matrix_t matrix
 *
 * @param      inv     The inv
 * @param      output  The output
 */
void print_inverse(matrix_t *inv, FILE *output)
{
	fprintf(output, "%d\n", inv->size);
	
	for(int i = 0; i < inv->size; i++)
	{
		for(int j = 0; j < inv->size; j++)
			fprintf(output, "%.17g ", INVERSE_ELEM(inv, i, j));
		fprintf(output, "\n");
	}
}

/**
 * @brief      Initializes a matrix_t pointer by preparing a struct and returning it's pointer
 *
 * @param[in]  size  The size of one side of the matrix (number of rows/columns, not number of elements)
 *
 * @return     A struct matrix_t pointer properly initialized
 */
matrix_t *square_matrix_init(int size)
{
	matrix_t *m = (matrix_t *) malloc (sizeof(matrix_t));

	if(!m) return m;

	m->size = size;
	m->memory = size * size * sizeof(double);
	m->elem = (double *) malloc (m->memory);

	if(!m->elem)
	{
		free(m);
		return NULL;
	}

	m->actual_line_pos = (int *) malloc (size * sizeof(int));

	if(!m->actual_line_pos)
	{
		free(m->elem);
		free(m);
		return NULL;
	}

	int *ptr = m->actual_line_pos;
	for(int i = 0; i < size; i++, ptr++)
		*ptr = i;

	return m;
}

/**
 * @brief      Initializes a lu_matrix_t pointer by preparing a struct and returning it's pointer
 *
 * @param[in]  size  The size of one side of the matrix (number of rows/columns, not number of elements)
 *
 * @return     A struct lu_matrix_t pointer properly initialized
 */
lu_matrix_t *lu_matrix_init(int size)
{
	lu_matrix_t *m = (lu_matrix_t *) malloc (sizeof(lu_matrix_t));

	if(!m) return m;

	m->size = size;

	m->L = (double *) malloc (size * (size + 1) * sizeof(double));

	if(!m->L)
	{
		free(m);
		return NULL;
	}

	for(int i = 0; i < size; i++)
		LOWER_ELEM(m, i, i) = 1.0;

	m->U = m->L + ( (size * (size + 1))/2 );

	m->actual_line_pos = (int *) malloc (size * sizeof(int));

	if(!m->actual_line_pos)
	{
		free(m->L);
		free(m);
		return NULL;
	}

	for(int i = 0; i < size; i++)
		m->actual_line_pos[i] = i;

	return m;
}

/**
 * @brief      Frees a matrix_t pointer properly
 *
 * @param      matrix  The matrix_t pointer
 */
void free_matrix(matrix_t *matrix)
{
	if(!matrix) return;
	if(matrix->elem) free(matrix->elem);
	if(matrix->actual_line_pos) free(matrix->actual_line_pos);
	free(matrix);
	return;
}

/**
 * @brief      Frees a lu_matrix_t pointer properly
 *
 * @param      matrix  The lu_matrix_t pointer
 */
void free_lu_matrix(lu_matrix_t *matrix)
{
	if(!matrix) return;
	if(matrix->L) free(matrix->L);
	free(matrix);
	return;
}

/**
 * @brief      Makes a copy of received matrix
 *
 * @param      matrix  The matrix
 *
 * @return     Pointer to copied matrix struct
 */
matrix_t *matrix_copy(matrix_t *matrix)
{
	if(!matrix || !matrix->elem || !matrix->actual_line_pos) return NULL;

	matrix_t *copy = square_matrix_init(matrix->size);

	memcpy(copy->elem, matrix->elem, matrix->memory);
	memcpy(copy->actual_line_pos, matrix->actual_line_pos, matrix->size);

	return copy;
}