# Dados Gerais {#mainpage}

## Autores

Fernando Aoyagui Shinohata - GRR20165388 - fas16  \n
Lucas Sampaio Franco - GRR20166836 - lsf16  

## Estrutura dos arquivos

Pastas:  
  - *src/* -- Contém todos os arquivos-fonte  
  - *obj/* -- Usada para armazenar os arquivos-objeto  
  - *lib/* -- Contém todos os arquivos de cabeçalho (.h)  
  - *doc/* -- Contém o arquivo de configuração e compilação do doxygen, além de ser usada para armazenar seu output.
  - *samples/* -- Contém algumas matrizes de teste. Os nomes estão no seguinte formato:
    * NxN -- Matriz quadrada de tamanho N;
    * (N-1)xN -- Matriz quadrada de tamanho N, com uma das linhas sendo linearmente dependentes;
    * I_NxN -- Matriz identidade de tamanho N;
   
## Makefile

Opções:
  - `make`: Compila o executável *invmat* normalmente.
  - `make optimized`: Compila o executável *invmat* com a opção -O3, do gcc.
  - `make debug`: Compila o executável *invmat* em "modo de depuração". Neste modo, todas as operações executadas são enviadas para stdout
  - `make clean`: Remove todos os arquivos gerados pelo *Makefile*, que inclui os arquivos compilados pelo *Doxygen*. Executa as regras `clean_files` e `clean_doxygen`.
  - `make clean_files`: Remove todos os arquivos gerados pelo *Makefile*, sem incluir os arquivos compilados pelo *Doxygen*.
  - `make clean_doxygen`: Remove os arquivos compilados pelo *Doxygen*.
  - `make doc`: Executa o *Doxygen* para compilar a documentação do código, criando também um arquivo *Documentation.html* no diretório principal para facilitar o acesso.

## Principais estruturas de dados

**Nota: O número de acessos a memória é calculado considerando um compilador bem ruim, que não reutiliza registradores para acessar vetores/matrizes.**

  - **Matrizes Genéricas:** Estão contidas na *struct matrix\_t*. Sua macro de acesso é MATRIX\_ELEM(). Os elementos são mapeados **linha-a-linha**. São alocadas como um vetor, em uma única chamada de malloc().  
    * **Prós:** Como um vetor é alocado, ganha-se desempenho pelo uso mais eficiente da cache (se baseada na Lei da Localidade).  
    * **Contras:** Aumenta uma operação de multiplicação, além de cada acesso exigir um total, em pior caso, de **sete** acessos a memória (por ser uma *struct*), sendo que o modo "comum" exige cinco acessos.
  - **Matrizes LU:** Estão contidas na *struct lu\_matrix\_t*. As macros de acesso LOWER\_ELEM() e UPPER\_ELEM() foram elaboradas a partir da solução de somatórios provenientes da análise da composição das matrizes L e U. Para mais informações, consulte a documentação de ambas as macros.  
    * **Prós:** Economiza-se espaço, uma vez que cada uma das matrizes L e U são representadas de maneira esparsa, em um vetor com ((N+1)\*N)/2 entradas cada. Além disso, são alocadas com uma única chamada de malloc(), aproveitando-se a Lei da Localidade.  
    * **Contras:** Cada acesso exige uma multiplicação, uma divisão, uma soma e seis acessos a memória em pior caso para a matriz L. No caso da matriz U, cada acesso exige duas multiplicações, uma divisão, três somas e **dez** acessos a memória em pior caso (Considerando um compilador muito ruim).  
  - **Matriz Inversa:** É representada pela *struct matrix\_t*, assim como as demais matrizes genéricas. Seu diferencial é a macro de acesso INVERSE\_ELEM(), que desta vez mapeia os elementos **coluna-a-coluna**, em vista da necessidade de resolver sistemas lineares coluna-a-coluna para se obter a matriz inversa.
    * **Prós:** O mapeamento coluna-a-coluna, no caso deste trabalho, aproveita a Lei da Localidade. Agrega os prós das matrizes genéricas.  
    * **Contras:** Idem aos contras das matrizes genéricas.  

## Nota

O resto da documentação está em inglês...