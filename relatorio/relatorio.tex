\documentclass[10pt]{article}

\usepackage{color}

\usepackage{listings}

\usepackage{multirow}

\usepackage{amsmath}

\usepackage{sbc}

\usepackage{graphicx,url}

\usepackage[brazil]{babel}

\usepackage[utf8]{inputenc}

\usepackage[colorlinks=false]{hyperref}

% Configurações de tabelas
\usepackage[table]{xcolor}
\setlength{\tabcolsep}{10pt}
\renewcommand{\arraystretch}{1.4}
\usefont{T1}{times}{m}{n}

\sloppy

\title{Relatório -- Otimização e Comparação do Inversor de Matrizes}

\author{Fernando Aoyagui Shinohata\inst{1} -- GRR20165388\\Lucas Sampaio Franco\inst{1} -- GRR20166836}

\address{Departamento de Informática -- Universidade Federal do Paraná (UFPR)\\
    Curitiba -- PR -- Brasil
  \email{\{fas16, lsf16\}@inf.ufpr.br}
}

\begin{document}

\maketitle

\begin{abstract}
This article-report presents comparative data between two versions of a matrix inversion program: an initial version and one with basic optimizations seem in class. The tests, ran in an i7-4770 with Haswell architecture, have shown an approximately 7.2 times improvement on it's performance. The main optimizations include loop unrolling and the use of AVX Intrinsics functions. With the graphic analysis, some interesting facts/problems were identified.
\end{abstract}

\begin{resumo}
Este artigo-relatório apresenta dados comparativos entre duas versões de um programa inversor de matrizes: uma versão inicial e outra com otimizações básicas vistas em aula. Os testes, executados em um i7-4770 de arquitetura Haswell, apresentaram uma melhora de aproximadamente 7.2 vezes no desempenho. As principais otimizações englobam o \textit{loop unrolling} e o uso de funções \textit{AVX Intrinsics}. Com a análise dos gráficos, alguns fatos/problemas interessantes foram identificados.
\end{resumo}

\section{Introdução}
\label{sec-intro}

Este relatório contém dados referentes a otimizações feitas sobre o programa de Inversão de Matrizes. As otimizações implementadas englobam, principalmente, \textit{loop unrolling} e o uso de funções \textit{intrinsics} em AVX.

O processador utilizado nos testes foi um \textit{Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz}, com \textit{Cache} L1 de 32kB, com linhas de 64 \textit{bytes}. Os dados detalhados podem ser vistos na Tabela~\ref{tab-processor}. Especificamente, a máquina de testes foi a I15, do Laboratório 4 do Departamento de Informática.

\begin{table}[ht]
    \centering
    \caption{Dados do processador utilizado nos testes.}
    \label{tab-processor}
    \small{
    \begin{tabular}{c|c}
        \textbf{Processador} & Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz \\
        \hline
        \textbf{Arquitetura} & Intel Core Haswell Processor \\
        \hline
        \textbf{Tam. Cache L1} & 32kB \\
        \hline
        \textbf{Tam. Linha} & 64B \\
        \hline
        \textbf{Assoc. Cache L1} & 8-way
    \end{tabular}
    }
\end{table}

Os testes foram executados no \textit{core} 0, e apresentaram uma melhora na velocidade de aproximadamente 7.2 vezes. Houve também uma melhora na precisão da ordem de $10^{2}$, na maioria dos casos.

A próxima seção apresenta comparações em gráficos entre as duas versões, separadas nas subseções consequentes. As análises tentam não abordar aspectos em muito baixo nível. Os detalhes das otimizações estão descritas na seção~\ref{sec-optimizations}.



\section{Análise em gráficos}
\label{sec-graphics}

Executando os testes para as matrizes de tamanho \{32, 33, 64, 65, 128, 129, 256, 257, 512, 1000, 2000\}, analisamos o tempo médio de cálculo de duas operações seguido dos seguintes \textit{Performance Monitors} do \textit{LIKWID}: FLOPS\_AVX, L2CACHE e L3.

Os gráficos utilizam a seguinte terminologia para as legendas: \textit{v1} e \textit{v2} se referem, respectivamente, à versão original e a versão otimizada; \textit{op1} representa o tempo de resolução dos sistemas lineares $Ly=b$ e $Ux=y$ \textbf{no refinamento e na inversão da matriz}; \textit{op2} representa o tempo de cálculo do resíduo no refinamento.

Nas subseções abaixo seguem análises dos gráficos obtidos.

\subsection{Tempo médio de cálculo}
\label{sec-graphics--calc-time}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=.55]{figures/time2.png}
    \caption{Gráfico de comparação de tempos médios de cálculo das versões 1 e 2.}
    \label{fig-time-graphic}
\end{figure}


Note, na Figura~\ref{fig-time-graphic}, que o cálculo do resíduo (denotado por \textit{v1\_op2}) na versão 1 mantém um comportamento linear. Este comportamento é o esperado, dado o fato de que a multiplicação das matrizes $A$ e $A^{-1}$ utiliza cálculos ``estáveis'', sem muitas oscilações por conta do uso da memória \textit{cache}: para todo \lstinline{R[i][j]}, uma linha ou coluna de uma das matrizes é alterado, que implica sua inexistência na \textit{cache} e, portanto, um crescimento ``estável'' de \textit{cache misses}.

Veja abaixo uma simplificação da implementação utilizada. As matrizes $R$ e $A$ seguem o mapeamento padrão \textit{row major}, enquanto a inversa é mapeada no padrão \textit{column major}:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(i = 0; i < N; i++)
  for(j = 0; j < N; j++)
    for(k = 0; k < N; k++)
      R[i][j] -= A[i][k] * Inv[k][j];
\end{lstlisting}

Ainda analisando a versão 1, no caso da resolução dos sistemas lineares (denotado por \textit{v1\_op1}), é possível observar oscilações entre \textit{op1} e \textit{op2} nos tempos médios de cálculo para os tamanhos 32, 64 e 128. Os dados coletados apresentam um aumento não linear no \textit{miss ratio} da \textit{cache} L2. Esses dados podem ser observados na Tabela~\ref{tab-v1-data}.

\begin{table}[ht]
    \centering
    \caption{Dados comparativos do tempo médio de cálculo das operações 1 e 2 na versão 1. A coluna $N$ contém os tamanhos das matrizes.}
    \label{tab-v1-data}
    \small{
    \begin{tabular}{c|c}
        \textbf{N} & \textbf{L2 \textit{miss ratio}} \\
        \hline
        32 & 0.1427 \\
        \hline
        33 & 0.1391 \\
        \hline
        64 & 0.1860 \\
        \hline
        65 & 0.2033 \\
        \hline
        128 & 0.3088 \\
        \hline
        129 & 0.3016 \\
    \end{tabular}
    }
\end{table}

Abaixo, colocamos um trecho de código simplificado da resolução do sistema $Ux=y$. A variável $x$ é um vetor, e $U$ é uma matriz:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(i = n-1; i >= 0; i--)
{   
  for(j = i+1; j < N; j++)
    x[i] -= x[j] * U[i][j];
  x[i] /= U[i][i];
}
\end{lstlisting}

O vetor $y$, no trecho acima, é a própria variável $x$, que é reaproveitado. O sistema $Ly=b$ é calculado de maneira análoga.

Com base no código acima e no fato de que o tamanho da \textit{cache} L1 tem tamanho de 32kB, acreditamos que o aumento no \textit{miss ratio} vem do mapeamento de alguns elementos \lstinline{x[j]} e \lstinline{U[i][j]} para uma mesma linha da \textit{cache} L1, resultando em ``perdas periódicas'' de elementos \lstinline{x[j]}.

Em outras palavras, para os tamanhos 32, 64 e 128, os dados do vetor \lstinline{x[]} e da linha \lstinline{U[i]} cabem, em parte, na \textit{cache} L1. Ao chegar ao final do laço, alguns dos primeiros elementos são perdidos devido a mapeamentos para a mesma linha, resultando em um pequeno aumento no \textit{miss ratio}. 

Para os tamanhos 256 em diante, os dados do laço não cabem mais na \textit{cache} L1, resultando no comportamento linear visto na Figura~\ref{fig-time-graphic}. Essa explicação vale, analogamente, para o laço de resolução do sistema $Ly=b$.


\subsection{MFLOPs}
\label{sec-graphics--mflops}

Analisando a contagem de MFLOPS por segundo nas duas versões (veja Figura~\ref{fig-mflops-graphic}), é possível observar a estabilidade da versão 2 em relação ao aumento da matriz para os tamanhos testados. No caso da versão 1, a taxa decresce de maneira linear desde os primeiros testes.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=.55]{figures/mflops.png}
    \caption{Gráfico de comparação de MFLOPS/s nas versões 1 e 2.}
    \label{fig-mflops-graphic}
\end{figure}

Este fato ocorre porque a versão 1 realiza todas as operações de maneira linear, em laços singulares:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(int i = 0; i < N; i++)
  A[i] += B[i] * C[i];
\end{lstlisting}

Dessa forma, aumentar o tamanho da matriz significa aumentar o número de iterações necessárias para realizar os cálculos. No caso da versão 2, alguns aspectos dos laços são explorados: O \textit{loop unrolling} de tamanho 4 permite a utilização eficiente dos registradores AVX, além de diminuir o número de iterações para $25\%$:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(i = 0; i < N-4; i += 4)
  fused_multiply_add(A[i..i+3], B[i..i+3], C[i..i+3]);
// Remainder
\end{lstlisting}

Com isso, para os tamanhos de matriz testados, a execução se mantém estável, mas espera-se um comportamento semelhante à versão 1 a partir de algum tamanho $N$.

\subsection{Cache miss ratio L2}
\label{sec-graphics--miss-ratio}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=.55]{figures/l2_cache.png}
    \caption{Gráfico de comparação do \textit{cache L2 miss ratio} das versões 1 e 2.}
    \label{fig-miss-ratio-graphic}
\end{figure}

Analisando o gráfico da Figura~\ref{fig-miss-ratio-graphic}, é possível observar as mesmas anomalias citadas na subseção~\ref{sec-graphics--calc-time}. Para $N < 512$, o cálculo do resíduo da versão 1 mantém o mesmo comportamento linear, excetuando o caso onde $N=64$. 

Nossa suposição para este caso é de que $N=64$ é um ponto de máximo local para a operação de multiplicação de matrizes para o processador, uma vez que o mesmo cálculo para a versão 2 obtém um comportamento similar, seguido de um \textit{miss ratio} extremamente próximo para $N=65$ (observe \textit{v1\_op2} e \textit{v2\_op2}).

Como a \textit{cache} L1 tem tamanho de 32kB (em 8-way, que implica 4k linhas), acreditamos que $N=64$ cause mapeamentos de linhas/colunas de ambas matrizes $A$ e $A^{-1}$ para as mesmas linhas na \textit{cache}, de maneira regular e uniforme. Dessa forma, o \textit{miss ratio} aumenta em uma mesma proporção para todas as iterações do laço, resultando em um \textit{miss ratio} inevitavelmente maior.

Seguindo a análise, para $N \geq 512$, o comportamento das duas versões deixa de ser linear e passa a convergir: a versão 1 converge para $N \geq 1000$, enquanto a versão dois aparenta iniciar sua convergência para $N \geq 2000$.

Note que, apesar de ser mais rápida, a versão 2 se estabiliza em um \textit{cache miss ratio} mais elevado (aprox. 0.37) do que a versão 1 (aprox. 0.25). Nossa suposição para este caso é de que o \textit{loop unrolling}, em conjunto com o uso de \textit{AVX Intrinsics}, gera uma carga pontual de memória mais elevada, aumentando o \textit{miss ratio} de dados em troca da diminuição do número de iterações por laço.


\subsection{Banda de Memória -- Cache L3}
\label{sec-graphics--mem}

\begin{figure}[ht]
    \centering
    \includegraphics[scale=.55]{figures/mem.png}
    \caption{Gráfico de comparação da banda de memória da \textit{cache} L3 das versões 1 e 2.}
    \label{fig-mem-graphic}
\end{figure}

Analisando a banda de memória da \textit{cache} L3 na Figura~\ref{fig-mem-graphic}, pode-se observar uma ``perda de estabilidade'' na versão 2, se comparada à versão 1. Apesar de obtermos uma banda de memória muito maior, há oscilações significativas na versão 2, principalmente durante a resolução dos sistemas lineares (denotada no gráfico como \textit{v2\_op1}).

Esse problema se dá, em nossa análise, por um motivo principal: O uso não alinhado das operações em AVX. Esse fato pode ser observado para todos os valores $N$ que são potências de 2, seguido de seus vizinhos.

O que ocorre é que, mesmo com a alocação estando alinhada na \textit{cache}, as operações na resolução do sistema $Ux=y$, em específico, não utilizam deste fator corretamente. Observe o seguinte trecho de código simplificado da resolução do sistema:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
x[n-1] /= UPPER(M, (n-1), (n-1));
for (i = n-2; i >= 4 ; i -= 4) { // (1)
    double *restrict U0 = &(UPPER(M, i-3, 0));
    double *restrict U1 = &(UPPER(M, i-2, 0));
    double *restrict U2 = &(UPPER(M, i-1, 0));
    double *restrict U3 = &(UPPER(M, i, 0));

    __m256d tmp0 = _mm256_set1_pd(0.0);
    __m256d tmp1 = _mm256_set1_pd(0.0);
    __m256d tmp2 = _mm256_set1_pd(0.0);
    __m256d tmp3 = _mm256_set1_pd(0.0);

    for(j = i+1; j < n-4; j += 4) // (2)
    {
        // fmadd(a, b, c) = a + b * c
        tmp0 = fmadd(tmp0, &x[j], &U0[j]);
        tmp1 = fmadd(tmp1, &x[j], &U1[j]);
        tmp2 = fmadd(tmp2, &x[j], &U2[j]);
        tmp3 = fmadd(tmp3, &x[j], &U3[j]);
    }

    x[i-3] = x[i-3] - tmp0[0] - tmp0[1] - tmp0[2] - tmp0[3];
    x[i-2] = x[i-2] - tmp1[0] - tmp1[1] - tmp1[2] - tmp1[3];
    x[i-1] = x[i-1] - tmp2[0] - tmp2[1] - tmp2[2] - tmp2[3];
    x[i] = x[i] - tmp3[0] - tmp3[1] - tmp3[2] - tmp3[3];

    // Remainders
}
\end{lstlisting}

Nesse código, as variáveis \lstinline{U0}, \lstinline{U1}, \lstinline{U2} e \lstinline{U3} são \textit{aliases} que apontam para o início das linhas \lstinline{i-3}, \lstinline{i-2}, \lstinline{i-1} e \lstinline{i} da matriz $U$, respectivamente.

Note que a variável \lstinline{i} é inicializada em \lstinline{n-2}, e decrementada em 4 a cada iteração. A inicialização em \lstinline{n-2} foi decidida na primeira versão, pois o vetor \lstinline{x[]} é um \textit{alias} para o vetor \lstinline{y[]}. Logo, para \lstinline{x[n-1]} (última linha do sistema da matriz $U$), a resolução é simplesmente \lstinline{x[n-1] = y[n-1] / U[n-1][n-1] = x[n-1] / U[n-1][n-1]}.

Entretanto, após os testes percebemos que o uso de instruções AVX com essa inicialização, em teoria, gera uma carga de memória 1.5 vezes maior: A cada iteração, o laço ``(2)'' tenta carregar quatro \textit{doubles} a partir dos endereços \lstinline{x + j} e \lstinline{U + j}. O problema é que, a cada duas iterações, um dos quatro \textit{doubles} está em \textbf{outra linha da \textit{cache} L1} (veja Figura~\ref{fig-ux-system-mem-align-problem}).

\begin{figure}[ht]
    \centering
    \includegraphics[scale=.6]{figures/ux-system-mem-align-problem.png}
    \caption{Como os \textit{loads} do vetor \lstinline{x[]} ocorrem. Cada retângulo em laranja representa um \textit{load} de um registrador AVX.}
    \label{fig-ux-system-mem-align-problem}
\end{figure}

Logo, apesar de todos os vetores serem alocados em múltiplos de 64 (\textit{i.e.} alinhados na \textit{cache}), esse alinhamento não é bem aproveitado na implementação. Como nota de consideração, decidimos manter este problema por ser um tema interessante para este relatório.



\section{Otimizações}
\label{sec-optimizations}

Quanto a otimizações aplicadas, as principais envolvem: simplificações de acesso às matrizes; \textit{loop unrolling} na resolução dos sistemas lineares e no cálculo do resíduo; uso de funções \textit{intrinsics} AVX; alocação alinhada na memória \textit{cache}.

Algumas outras otimizações mínimas também foram aplicadas. Por exemplo, a primeira versão utilizava uma rotina que implementava a ``soma de Kahan'', que foi retirada na segunda versão. Mais detalhes dos procedimentos estão nas subseções abaixo.


\subsection{\textit{Pointer Aliasing}}
\label{sec-optimizations--access}

Como as matrizes são alocadas em memória contígua, seu acesso é feito via \textit{macros} para facilitar a leitura do código. Entretanto, recalcular as posições dos elementos na matriz se provou ineficaz. Para simplificar o acesso, os cálculos são feitos de antemão, fato que também simplifica a leitura do código (veja o exemplo abaixo).

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
double *restrict L = &(LOWER(M, l, 0));
int i = some_index();

y[i] = 0.0;
for(int j = 0; j < N; j++)
{
    y[i] -= y[j] * L[j];
}
\end{lstlisting}

No trecho de código acima, a variável \lstinline{L[]} se torna um \textit{alias} que aponta para o início da linha $l$ da matriz \lstinline{L}. Logo, apenas o deslocamento \lstinline{j} é necessário para realizar as operações em linha.


\subsection{\textit{Loop Unrolling} na Resolução dos Sistemas Lineares e no Cálculo do Resíduo}
\label{sec-optimizations--loop-unrolling}

Na resolução dos sistemas $Ly=b$ e $Ux=y$, temos uma dependência de dados linear, onde $x_i$ depende de $x_{[0..i-1]}$. Entretanto, é possível aplicar um \textit{loop unrolling} nesses sistemas.

No sistema $Ly=b$, para um \textit{loop unroll} de tamanho 4, temos que, para um dado $y_i$, $y_{[0..i-1]}$ já estão calculados. Logo, podemos calcular simultaneamente $y_i$, $y_{i+1}$, $y_{i+2}$ e $y_{i+3}$ para os elementos $y_{[0..i-1]}$, e depois tratar o restante separadamente. A implementação simplificada pode ser vista no trecho de código abaixo.

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(int i = 0; i < N-4; i += 4)
{
    double *restrict L0 = &(LOWER(M, i, 0));
    double *restrict L1 = &(LOWER(M, i+1, 0));
    double *restrict L2 = &(LOWER(M, i+2, 0));
    double *restrict L3 = &(LOWER(M, i+3, 0));

    y[i] = r[i];
    y[i+1] = r[i+1];
    y[i+2] = r[i+2];
    y[i+3] = r[i+3];

    for(int j = 0; j < i; j++)
    {
        y[i] -= y[j] * L0[j];
        y[i+1] -= y[j] * L1[j];
        y[i+2] -= y[j] * L2[j];
        y[i+3] -= y[j] * L3[j];
    }

    // Calculos finais para y[i+1..i+3]
}
// Remainder
\end{lstlisting}

No sistema $Ux=y$, de forma análoga, para um \textit{loop unroll} de tamanho 4, temos que, para um dado $x_i$, $x_{[i+1..n-1]}$ já estão calculados. Logo, podemos calcular simultaneamente, de maneira inversa, os valores $x_{[i-3..i]}$, utilizando os valores já calculados $x_{[i+1..n-1]}$. A implementação simplificada pode ser vista no trecho de código abaixo.

\begin{lstlisting}[language=C,frame=single,basicstyle=\small]
for(int i = N-1; i >= 0; i -= 4)
{
    double *restrict U0 = &(UPPER(M, i-3, 0));
    double *restrict U1 = &(UPPER(M, i-2, 0));
    double *restrict U2 = &(UPPER(M, i-1, 0));
    double *restrict U3 = &(UPPER(M, i, 0));

    x[i-3] = y[i-3];
    x[i-2] = y[i-2];
    x[i-1] = y[i-1];
    x[i]   = y[i];

    for(int j = i+1; j < N; j++)
    {
        x[i-3] -= x[j] * U0[j];
        x[i-2] -= x[j] * U1[j];
        x[i-1] -= x[j] * U2[j];
        x[i]   -= x[j] * U3[j];
    }

    // Calculos finais
}
// Remainder
\end{lstlisting}

No cálculo do resíduo, como as operações são independentes, o \textit{loop unrolling} passa a ser trivial, e portanto não será exemplificado neste artigo. Quanto ao aumento no desempenho, testes preliminares demonstraram um aumento de cerca de 2.5 vezes com estas otimizações.


\subsection{Uso de \textit{AVX Intrinsics} com alinhamento na \textit{cache}}
\label{sec-optimizations--avx}

Por conta dos problemas de alinhamento apresentados na subseção~\ref{sec-graphics--mem}, decidimos utilizar a instrução de carregamento \lstinline{_mm256_loadu_pd()}, que permite realizar \textit{loads} em endereços de memória não alinhados em múltiplos de 32. Por padrão, utilizamos esta instrução em todo o código, ``encobrindo'' as áreas onde o alinhamento não é respeitado.

O alinhamento do endereço inicial dos vetores na \textit{cache} foi feito pela troca da função \lstinline{malloc()} pela função \lstinline{posix_memalign()}. No caso do \textit{padding}, na prática, seu cálculo é ${\left(\left\lceil \dfrac{n}{8} \right\rceil * 8\right)}$, onde 8 é ``o número de \textit{doubles} que cabem em uma linha da \textit{cache}''.

Para as instruções AVX, não utilizamos explicitamente a instrução de \textit{fused multiply add}. Entretanto, na compilação, ela é utilizada. Abaixo, colocamos dois trechos de código do cálculo do resíduo que utilizam \textit{AVX Intrinsics}, seguidos de seus resultantes na compilação com o \lstinline{GCC}. Esses dados podem ser confirmados nos arquivos enviados junto ao e-mail.

\begin{lstlisting}[language=C,frame=single,basicstyle=\small,breaklines=true]
>> Loop original
r[i] = 0;
for(j = 0; j < n-4; j += 4)
{
    r[i] -= A[j] * I[j];
    r[i] -= A[j+1] * I[j+1];
    r[i] -= A[j+2] * I[j+2];
    r[i] -= A[j+3] * I[j+3];
}

// [Transformacao do Loop para AVX]
>> Pasta v2/src/
>> Arquivo invmat.c, linhas 282--289
r[i] = 0;
__m256d tmp = _mm256_set1_pd(0);
for(j = 0; j < n-4; j += 4)
{
    __asm__ volatile("# [START] (residue) AVX CHECK");
    tmp = _mm256_add_pd(tmp, _mm256_mul_pd(_mm256_loadu_pd(A + j), _mm256_loadu_pd(I + j)));
    __asm__ volatile("# [END] (residue) AVX CHECK");
}
r[i] = r[i] - tmp[0] - tmp[1] - tmp[2] - tmp[3];

>> Pasta data/compiled/
>> Arquivo v2_AVX_invmat_Intel(R)_Core(TM)_i7-4770_CPU_@_3.40GHz.s, linhas 718--727
# [START] (residue) AVX CHECK
    vmovupd (%rax), %ymm7
    vfmadd231pd (%rdx), %ymm7, %ymm2
# [END] (residue) AVX CHECK
\end{lstlisting}

O loop acima foi retirado do cálculo do \textit{remainder} do resíduo, sob a variável \lstinline{i}. Logo, como a execução é de no máximo três iterações, o \textit{loop unrolling} só pôde ser aplicado na variável \lstinline{j}.

Note que o compilador substituíu as operações das instruções \lstinline{_mm256_mul_pd()} e \lstinline{_mm256_add_pd()} pela instrução referente ao \textit{fused multiply add}. Dessa forma, são executadas duas instruções ao invés de três, a cada iteração do laço.

Em complemento, o trecho de código abaixo mostra a implementação do \textit{loop unrolling} no laço principal de cálculo do resíduo, onde dois laços são desenrolados, possibilitando o uso de quatro instruções \textit{AVX Intrinsics} ao mesmo tempo:

\begin{lstlisting}[language=C,frame=single,basicstyle=\small,breaklines=true]
>> Loop original
for(i = 0; i < n-4; i += 4)
{
    A = &(ORIGINAL(M, M->swap[i], 0)); // Linha i de A
    B = &(ORIGINAL(M, M->swap[i+1], 0)); // Linha i+1 de A
    C = &(ORIGINAL(M, M->swap[i+2], 0)); // Linha i+2 de A
    D = &(ORIGINAL(M, M->swap[i+3], 0)); // Linha i+3 de A

    r[i] = 0;
    r[i+1] = 0;
    r[i+2] = 0;
    r[i+3] = 0;

    for(j = 0; j < n-4; j += 4)
    {
        r[i] -= A[j] * I[j];
        r[i] -= A[j+1] * I[j+1];
        r[i] -= A[j+2] * I[j+2];
        r[i] -= A[j+3] * I[j+3];
        r[i+1] -= B[j] * I[j];
        r[i+1] -= B[j+1] * I[j+1];
        r[i+1] -= B[j+2] * I[j+2];
        r[i+1] -= B[j+3] * I[j+3];
        r[i+2] -= C[j] * I[j];
        r[i+2] -= C[j+1] * I[j+1];
        r[i+2] -= C[j+2] * I[j+2];
        r[i+2] -= C[j+3] * I[j+3];
        r[i+3] -= D[j] * I[j];
        r[i+3] -= D[j+1] * I[j+1];
        r[i+3] -= D[j+2] * I[j+2];
        r[i+3] -= D[j+3] * I[j+3];
    }
    // Remainders
}

// [Transformacao do Loop para AVX]
>> Pasta v2/src/
>> Arquivo invmat.c, linhas 228--255
for(i = 0; i < n-4; i += 4)
{
    __m256d tmp0 = _mm256_set1_pd(0.0);
    __m256d tmp1 = _mm256_set1_pd(0.0);
    __m256d tmp2 = _mm256_set1_pd(0.0);
    __m256d tmp3 = _mm256_set1_pd(0.0);

    A = &(ORIGINAL(M, M->swap[i], 0));
    B = &(ORIGINAL(M, M->swap[i+1], 0));
    C = &(ORIGINAL(M, M->swap[i+2], 0));
    D = &(ORIGINAL(M, M->swap[i+3], 0));

    for(j = 0; j < n-4; j += 4)
    {
        // tmpX[j..j+3] += Y[j..j+3] * I[j..j+3], X = [0..3] , Y = [A..D]

        __asm__ volatile("# [START] (residue) AVX CHECK");
        tmp0 = _mm256_add_pd(tmp0, _mm256_mul_pd(_mm256_loadu_pd(A + j), _mm256_loadu_pd(I + j)));
        tmp1 = _mm256_add_pd(tmp1, _mm256_mul_pd(_mm256_loadu_pd(B + j), _mm256_loadu_pd(I + j)));
        tmp2 = _mm256_add_pd(tmp2, _mm256_mul_pd(_mm256_loadu_pd(C + j), _mm256_loadu_pd(I + j)));
        tmp3 = _mm256_add_pd(tmp3, _mm256_mul_pd(_mm256_loadu_pd(D + j), _mm256_loadu_pd(I + j)));
        __asm__ volatile("# [END] (residue) AVX CHECK");
    }
    residue[i]   = residue[i]   - tmp0[0] - tmp0[1] - tmp0[2] - tmp0[3];
    residue[i+1] = residue[i+1] - tmp1[0] - tmp1[1] - tmp1[2] - tmp1[3];
    residue[i+2] = residue[i+2] - tmp2[0] - tmp2[1] - tmp2[2] - tmp2[3];
    residue[i+3] = residue[i+3] - tmp3[0] - tmp3[1] - tmp3[2] - tmp3[3];
    // Remainders
}

>> Pasta data/compiled/
>> Arquivo v2_AVX_invmat_Intel(R)_Core(TM)_i7-4770_CPU_@_3.40GHz.s, linhas 576--589
# [START] (residue) AVX CHECK
    vmovupd (%r15,%rdx), %ymm4
    vfmadd231pd (%r10,%rdx), %ymm4, %ymm0
    vfmadd231pd (%r9,%rdx), %ymm4, %ymm8
    vfmadd231pd (%r8,%rdx), %ymm4, %ymm7
    vfmadd231pd (%rdi,%rdx), %ymm4, %ymm6
# [END] (residue) AVX CHECK
\end{lstlisting}

Note que o compilador aloca as variáveis \lstinline{tmp[]} na pilha, sendo que outros registradores \lstinline{ymm} poderiam ser utilizados para armazená-las. Entretanto, o \lstinline{GCC} percebeu que os elementos \lstinline{I[j..j+3]} são reutilizados no código, e realiza o carregamento da memória apenas uma vez.

Testes mostraram um aumento de aproximadamente 2 vezes no desempenho, com a aplicação de \textit{AVX Intrinsics} em todos os principais laços do código.



\section{Considerações Finais}

Neste artigo-relatório, comentamos e defendemos algumas otimizações básicas realizadas no programa inversor de matrizes. Após executar uma bateria de testes, verificamos que o \textit{loop unrolling} ``na mão'' de laços complexos e o uso explícito de \textit{AVX Intrinsics} foram os principais fatores que favoreceram no aumento do desempenho do programa.

Apesar do cuidado, os gráficos mostraram alguns problemas que não foram identificados pelos programadores antes da elaboração deste relatório, mas que contribuíram como fatos interessantes para melhorar o entendimento do tema. Também mostramos algumas otimizações ``extras'' que poderiam ser feitas pelo compilador, como o uso total de registradores AVX para calcular os resultados dos laços.

Como conclusão, acreditamos que a versão 2 do programa se mostrou muito melhor que a anterior, nos aspectos de desempenho e precisão. No que rege o tema de otimizações, percebemos apenas pequenas alterações que poderiam ser feitas, mas não sabemos o quanto isso poderia melhorar no desempenho. Achamos que, como um problema \textit{memory bound}, o programa inversor de matrizes obteve uma melhora satisfatória.

\end{document}
